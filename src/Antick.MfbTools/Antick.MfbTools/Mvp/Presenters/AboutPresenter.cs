﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.MfbTools.Infractructure.Mvp.Presenters;
using Antick.MfbTools.Mvp.Models;
using Antick.MfbTools.Mvp.Views;
using Castle.MicroKernel;

namespace Antick.MfbTools.Mvp.Presenters
{
    public class AboutPresenter : Presenter<IAboutView,AboutModel>
    {
        public AboutPresenter(IKernel kernel, IAboutView view, AboutModel model) : base(kernel, view, model)
        {
        }
    }
}
