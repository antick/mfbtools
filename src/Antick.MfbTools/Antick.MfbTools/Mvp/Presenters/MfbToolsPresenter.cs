﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Antick.MfbTools.Components;
using Antick.MfbTools.Components.Data;
using Antick.MfbTools.Components.Settings;
using Antick.MfbTools.Domain.Enums;
using Antick.MfbTools.Infractructure.Mvp.Presenters;
using Antick.MfbTools.Mvp.Args;
using Antick.MfbTools.Mvp.Models;
using Antick.MfbTools.Mvp.Views;
using Castle.Core.Logging;
using Castle.MicroKernel;

namespace Antick.MfbTools.Mvp.Presenters
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class MfbToolsPresenter : Presenter<IMfbToolsView,MfbToolsModel>
    {
        private readonly ILogger m_Logger;
        private readonly SettingsRootPresenter m_SettingsViewPresenter;
        private readonly ISettingsService m_SettingsService;
        private readonly IAboutPresenterFactory m_AboutPresenterFactory;
        private readonly AppConfig m_AppConfig;

        public MfbToolsPresenter(IKernel kernel, IMfbToolsView view, MfbToolsModel model, ILogger logger, SettingsRootPresenter settingsViewPresenter, ISettingsService settingsService, IAboutPresenterFactory aboutPresenterFactory, AppConfig appConfig) : base(kernel, view, model)
        {
            m_Logger = logger;

            m_SettingsViewPresenter = settingsViewPresenter;
            m_SettingsService = settingsService;
            m_AboutPresenterFactory = aboutPresenterFactory;
            m_AppConfig = appConfig;
            m_SettingsViewPresenter.ChangesApplied += settingsViewPresenter_ChangesApplied;

            View.InstrumentChanging += View_InstrumentChanging;
            View.TimeFrameChanging += View_TimeFrameChanging;
            View.SourceDataChanging += View_SourceDataChanging;
            View.CountDataChanging += View_CountDataChanging;
            View.IndicatorChanging += View_IndicatorChanging;
            View.RefreshStarting += View_RefreshStarting;
            View.ViewStarted += View_ViewStarted;
            View.RenderStarting += View_RenderStarting;
            View.DataProjectionTypeChanged += View_DataProjectionTypeChanged;
            View.DataProjectionValueChanged += View_DataProjectionValueChanged;
            View.ExitingApplication += View_ExitingApplication;
            View.ShowingAbout += View_ShowingAbout;
            View.ShowingSettings += View_SettingsRaised;
            View.CandleProjectionTypeChanging += View_CandleProjectionTypeChanging;
            View.CandleProjectionValueChanging += View_CandleProjectionValueChanging;
        }

        private void View_ExitingApplication(object sender, EventArgs e)
        {
            View.Close();
        }

        public override void Run()
        {
            var settings = m_SettingsService.Get();
            changeLanuage(settings.Language);
            View.LoadInitDatas(Model.GetInitData());
            View.Show();
        }


        private void render()
        {
            Task.Factory.StartNew(() =>
            {
                if (Model.ViewDataLots.Any() && Model.ViewDataPositions.Any())
                {

                    var renderdata = Model.GenerateRenderData();
                    View.Render(renderdata);
                }
                else
                {
                    try
                    {
                        downloadData();
                    }
                    catch (Exception ex)
                    {
                        View.RenderError();
                        return;
                    }
                }

                View.UpdateCurrentMode(Model.GenerateMode());
            });
        }

        private void refresh()
        {
            Task.Factory.StartNew(() =>
            {
                try
                {
                    downloadData();
                }
                catch (Exception ex)
                {
                    View.RenderError();
                    return;
                }

                render();
            });
        }

        private void downloadData()
        {
            Model.DownloadData();

            if (Model.DownlaodDataLots.Last().Candle.Time > m_AppConfig.ExpirationTime)
                View.ShowTrialMessageExpired();
        }

        private void View_RefreshStarting(object sender, EventArgs e)
        {
            refresh();
        }

        private void View_IndicatorChanging(object sender, IndicatorChangingEventArgs e)
        {
            Model.Indicator = e.Indicator;

            render();
        }

        private void View_CountDataChanging(object sender, CountDataEventArgs e)
        {
            Model.CountData = e.CountData;

            render();
        }

        private void View_SourceDataChanging(object sender, SourceDataChangingEventArgs e)
        {
            Model.SourceData = e.SourceData;

            render();
        }

        private void View_TimeFrameChanging(object sender, TimeFrameChangingEventArgs e)
        {
            Model.TimeFrame = e.TimeFrame;

            refresh();
        }

        private void View_InstrumentChanging(object sender, InstrumentChangingEventArgs e)
        {
            Model.Instrument = e.Instrument;

            refresh();
        }

        private void View_CandleProjectionTypeChanging(object sender, CandleProjectionTypeChangingEventArgs e)
        {
            Model.CandleProjectionType = e.Type;
            render();
        }

        private void View_CandleProjectionValueChanging(object sender, CandleProjectionValueChangedEventArgs e)
        {
            Model.CandleProjectionValue = e.Value;
            render();
        }

        private void View_ViewStarted(object sender, EventArgs e)
        {
            try
            {
                downloadData();
            }
            catch (Exception ex)
            {
                View.RenderError();
                return;
            }

            View.LoadComboBoxes(Model.AvalibleDataProjectionTypes, Model.AvalibleCandleProjectionTypes);

            render();
        }

        private void View_RenderStarting(object sender, EventArgs e)
        {
            Model.UpdateViewData();
            render();
        }

        private void View_DataProjectionTypeChanged(object sender, SourceDataProjectionTypeChangedEventArgs e)
        {
            Model.ProjectionType = e.ProjectionType;
        }

        private void View_DataProjectionValueChanged(object sender, SourceDataProjectionValueChangedEventArgs e)
        {
            Model.ProjectionValue = e.ProjectionValue;
        }
        
        private void View_SettingsRaised(object sender, EventArgs e)
        {
            m_SettingsViewPresenter.Run();
        }

        private void settingsViewPresenter_ChangesApplied(object sender, SettingChangesAppliedEventArgs e)
        {
            changeLanuage(e.Settings.Language);
            m_SettingsViewPresenter.ApplyLocalization();
            View.ApplyLocalization();
            View.LoadComboBoxes(Model.AvalibleDataProjectionTypes, Model.AvalibleCandleProjectionTypes);
            View.UpdateCurrentMode(Model.GenerateMode());
        }

        private void View_ShowingAbout(object sender, EventArgs e)
        {
            var aboutPresenter = m_AboutPresenterFactory.Create();
            aboutPresenter.Run();
        }

        private void changeLanuage(LanguageType language)
        {
            var culture = language == LanguageType.Russian
                ? CultureInfo.GetCultureInfo("ru-ru")
                : CultureInfo.GetCultureInfo("en-US");

            Application.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;
        }

        
    }
}
