﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.MfbTools.Extensions;
using Antick.MfbTools.Infractructure.Mvp.Presenters;
using Antick.MfbTools.Mvp.Args;
using Castle.MicroKernel;

namespace Antick.MfbTools.Mvp.Presenters
{
    public class SettingsRootPresenter : RootPresenter
    {
        public event EventHandler<SettingChangesAppliedEventArgs> ChangesApplied;

        private readonly ISettingsPresenterFactory m_FactoryPresenter;

        private SettingsPresenter m_SettingsPresenter;

        public SettingsRootPresenter(IKernel kernel, ISettingsPresenterFactory factoryPresenter) : base(kernel)
        {
            m_FactoryPresenter = factoryPresenter;
        }

        public override void Run()
        {
            m_SettingsPresenter = m_FactoryPresenter.Create();
            m_SettingsPresenter.ChangesApplied += presenter_ChangesApplied;
            m_SettingsPresenter.Run();
        }

        private void presenter_ChangesApplied(object sender, SettingChangesAppliedEventArgs e)
        {
            ChangesApplied.Raise(this, e);
        }

        public void ApplyLocalization()
        {
            m_SettingsPresenter.ApplyLocalization();
        }
    }
}
