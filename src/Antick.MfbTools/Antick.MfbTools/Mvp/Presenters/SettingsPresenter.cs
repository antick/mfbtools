﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.MfbTools.Components;
using Antick.MfbTools.Components.Settings;
using Antick.MfbTools.Extensions;
using Antick.MfbTools.Infractructure.Mvp.Models;
using Antick.MfbTools.Infractructure.Mvp.Presenters;
using Antick.MfbTools.Mvp.Args;
using Antick.MfbTools.Mvp.Models;
using Antick.MfbTools.Mvp.Views;
using Castle.MicroKernel;

namespace Antick.MfbTools.Mvp.Presenters
{
    public class SettingsPresenter : Presenter<ISettingsView, SettingsModel>
    {
        public event EventHandler<SettingChangesAppliedEventArgs> ChangesApplied;

        private readonly ISettingsService m_SettingsService;

        public SettingsPresenter(IKernel kernel, ISettingsView view, SettingsModel model, ISettingsService settingsService) : base(kernel, view, model)
        {
            m_SettingsService = settingsService;
            View.LanguageChanging += View_LanguageChanging;
            View.Loaded += View_Loaded;
            View.Applying += view_Applying;
            View.Saving += View_Saving;
        }

        private void View_Loaded(object sender, EventArgs e)
        {
            var settings = m_SettingsService.Get();
            Model.Settings = settings;
            View.Render(Model.Settings);
        }

        private void view_Applying(object sender, EventArgs e)
        {
            save();
        }

        private void View_LanguageChanging(object sender, LanguageChangingEventArgs e)
        {
            Model.Settings.Language = e.LanguageType;
        }

        private void View_Saving(object sender, EventArgs e)
        {
            save();
            View.Close();
        }

        private void save()
        {
            m_SettingsService.Save(Model.Settings);
            ChangesApplied.Raise(this, new SettingChangesAppliedEventArgs { Settings = Model.Settings });
        }

        public void ApplyLocalization()
        {
            View.ApplyChanges();
        }
    }
}
