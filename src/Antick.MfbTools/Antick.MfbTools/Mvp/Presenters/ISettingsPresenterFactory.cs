﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Antick.MfbTools.Mvp.Presenters
{
    public interface ISettingsPresenterFactory
    {
        SettingsPresenter Create();
    }
}
