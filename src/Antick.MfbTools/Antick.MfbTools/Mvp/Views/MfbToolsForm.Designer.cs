﻿using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Antick.MfbTools.Mvp.Views
{
    partial class MfbToolsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series7 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series8 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series9 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.buttonTfD1Set = new System.Windows.Forms.Button();
            this.buttonTfH4Set = new System.Windows.Forms.Button();
            this.buttonTfH1Set = new System.Windows.Forms.Button();
            this.buttonTfM30Set = new System.Windows.Forms.Button();
            this.buttonTfM5Set = new System.Windows.Forms.Button();
            this.buttonTfM15Set = new System.Windows.Forms.Button();
            this.buttonInstrumentEurUsdSelect = new System.Windows.Forms.Button();
            this.buttonInstrumentGbpUsdSelect = new System.Windows.Forms.Button();
            this.buttonInstrumentAudUsdSelect = new System.Windows.Forms.Button();
            this.buttonInstrumentNzdUsdSelect = new System.Windows.Forms.Button();
            this.buttonInstrumentUsdCadSelect = new System.Windows.Forms.Button();
            this.buttonInstrumentUsdChfSelect = new System.Windows.Forms.Button();
            this.buttonInstrumentUsdJpySelect = new System.Windows.Forms.Button();
            this.buttonInstrumentXauUsdSelect = new System.Windows.Forms.Button();
            this.buttonInstrumentXagusdSelect = new System.Windows.Forms.Button();
            this.buttonInstrumentEurGpbSelect = new System.Windows.Forms.Button();
            this.buttonTfM1Set = new System.Windows.Forms.Button();
            this.radioButtonIndicatorLongShort = new System.Windows.Forms.RadioButton();
            this.radioButtonIndicatorSumGrowDelta = new System.Windows.Forms.RadioButton();
            this.radioButtonIndicatorOpenInterest = new System.Windows.Forms.RadioButton();
            this.radioButtonIndicatorGrowDelta = new System.Windows.Forms.RadioButton();
            this.buttonRefresh = new System.Windows.Forms.Button();
            this.groupBoxSourceData = new System.Windows.Forms.GroupBox();
            this.radioButtonSourceDataPositions = new System.Windows.Forms.RadioButton();
            this.radioButtonSourceDataLots = new System.Windows.Forms.RadioButton();
            this.groupBoxCountData = new System.Windows.Forms.GroupBox();
            this.buttonCountData500 = new System.Windows.Forms.Button();
            this.buttonCountData200 = new System.Windows.Forms.Button();
            this.buttonCountData100 = new System.Windows.Forms.Button();
            this.buttonCountDataAll = new System.Windows.Forms.Button();
            this.labelCurrentTime = new System.Windows.Forms.Label();
            this.labelCandleData = new System.Windows.Forms.Label();
            this.labelIndicatorData = new System.Windows.Forms.Label();
            this.textBoxDataProjectionValue = new System.Windows.Forms.TextBox();
            this.groupBoxDataProjection = new System.Windows.Forms.GroupBox();
            this.labelDataProjAveragePeriod = new System.Windows.Forms.Label();
            this.comboBoxDataProjectionType = new System.Windows.Forms.ComboBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SettingstoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBoxTimeFrame = new System.Windows.Forms.GroupBox();
            this.groupBoxIndicators = new System.Windows.Forms.GroupBox();
            this.checkBoxIndicatorVisible = new System.Windows.Forms.CheckBox();
            this.labelCurrentMode = new System.Windows.Forms.Label();
            this.groupBoxCandlesProjection = new System.Windows.Forms.GroupBox();
            this.labelCandlesProjAveragePerio = new System.Windows.Forms.Label();
            this.comboBoxCandleProjection = new System.Windows.Forms.ComboBox();
            this.textBoxCandleProjectionData = new System.Windows.Forms.TextBox();
            this.panelChartCounters = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.groupBoxSourceData.SuspendLayout();
            this.groupBoxCountData.SuspendLayout();
            this.groupBoxDataProjection.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.groupBoxTimeFrame.SuspendLayout();
            this.groupBoxIndicators.SuspendLayout();
            this.groupBoxCandlesProjection.SuspendLayout();
            this.panelChartCounters.SuspendLayout();
            this.SuspendLayout();
            // 
            // chart1
            // 
            chartArea1.CursorX.LineColor = System.Drawing.Color.Black;
            chartArea1.CursorX.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea1.CursorX.LineWidth = 2;
            chartArea1.Name = "MainArea";
            chartArea2.AlignWithChartArea = "MainArea";
            chartArea2.CursorX.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            chartArea2.CursorX.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea2.CursorX.LineWidth = 2;
            chartArea2.Name = "AreaThree";
            this.chart1.ChartAreas.Add(chartArea1);
            this.chart1.ChartAreas.Add(chartArea2);
            this.chart1.Location = new System.Drawing.Point(227, 217);
            this.chart1.Name = "chart1";
            series1.ChartArea = "MainArea";
            series1.Color = System.Drawing.Color.Transparent;
            series1.IsXValueIndexed = true;
            series1.Name = "BarsStub";
            series1.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series2.ChartArea = "MainArea";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Candlestick;
            series2.CustomProperties = "PriceDownColor=Red, PointWidth=1, PriceUpColor=Green";
            series2.IsXValueIndexed = true;
            series2.Name = "Bars";
            series2.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series2.YValuesPerPoint = 4;
            series3.BorderWidth = 2;
            series3.ChartArea = "AreaThree";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series3.Color = System.Drawing.Color.Blue;
            series3.IsXValueIndexed = true;
            series3.Name = "Buys";
            series3.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series4.BorderWidth = 2;
            series4.ChartArea = "AreaThree";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series4.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            series4.IsXValueIndexed = true;
            series4.Name = "Sells";
            series4.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series5.ChartArea = "AreaThree";
            series5.Color = System.Drawing.Color.Blue;
            series5.IsXValueIndexed = true;
            series5.Name = "Delta";
            series5.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series6.ChartArea = "AreaThree";
            series6.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StackedColumn;
            series6.Color = System.Drawing.Color.Red;
            series6.IsXValueIndexed = true;
            series6.Name = "Delta2";
            series6.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series7.ChartArea = "AreaThree";
            series7.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.SplineArea;
            series7.Color = System.Drawing.Color.Blue;
            series7.CustomProperties = "EmptyPointValue=Zero, LineTension=1";
            series7.IsXValueIndexed = true;
            series7.Name = "BuysArea";
            series7.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series8.ChartArea = "AreaThree";
            series8.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.SplineArea;
            series8.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            series8.CustomProperties = "EmptyPointValue=Zero, LineTension=1";
            series8.IsXValueIndexed = true;
            series8.Name = "SellsArea";
            series8.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            series9.BorderWidth = 2;
            series9.ChartArea = "AreaThree";
            series9.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series9.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            series9.IsXValueIndexed = true;
            series9.Name = "Sells2";
            series9.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
            this.chart1.Series.Add(series1);
            this.chart1.Series.Add(series2);
            this.chart1.Series.Add(series3);
            this.chart1.Series.Add(series4);
            this.chart1.Series.Add(series5);
            this.chart1.Series.Add(series6);
            this.chart1.Series.Add(series7);
            this.chart1.Series.Add(series8);
            this.chart1.Series.Add(series9);
            this.chart1.Size = new System.Drawing.Size(1109, 437);
            this.chart1.TabIndex = 2;
            this.chart1.Text = "chart1";
            this.chart1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.chart1_MouseMove);
            // 
            // buttonTfD1Set
            // 
            this.buttonTfD1Set.Location = new System.Drawing.Point(404, 29);
            this.buttonTfD1Set.Name = "buttonTfD1Set";
            this.buttonTfD1Set.Size = new System.Drawing.Size(56, 39);
            this.buttonTfD1Set.TabIndex = 61;
            this.buttonTfD1Set.Text = "D1";
            this.buttonTfD1Set.UseVisualStyleBackColor = true;
            this.buttonTfD1Set.Click += new System.EventHandler(this.buttonTfD1Set_Click);
            // 
            // buttonTfH4Set
            // 
            this.buttonTfH4Set.Location = new System.Drawing.Point(342, 29);
            this.buttonTfH4Set.Name = "buttonTfH4Set";
            this.buttonTfH4Set.Size = new System.Drawing.Size(56, 39);
            this.buttonTfH4Set.TabIndex = 62;
            this.buttonTfH4Set.Text = "H4";
            this.buttonTfH4Set.UseVisualStyleBackColor = true;
            this.buttonTfH4Set.Click += new System.EventHandler(this.buttonTfH4Set_Click);
            // 
            // buttonTfH1Set
            // 
            this.buttonTfH1Set.BackColor = System.Drawing.Color.Transparent;
            this.buttonTfH1Set.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonTfH1Set.Location = new System.Drawing.Point(280, 29);
            this.buttonTfH1Set.Name = "buttonTfH1Set";
            this.buttonTfH1Set.Size = new System.Drawing.Size(56, 39);
            this.buttonTfH1Set.TabIndex = 63;
            this.buttonTfH1Set.Text = "H1";
            this.buttonTfH1Set.UseVisualStyleBackColor = false;
            this.buttonTfH1Set.Click += new System.EventHandler(this.buttonTfH1Set_Click);
            // 
            // buttonTfM30Set
            // 
            this.buttonTfM30Set.Location = new System.Drawing.Point(218, 29);
            this.buttonTfM30Set.Name = "buttonTfM30Set";
            this.buttonTfM30Set.Size = new System.Drawing.Size(56, 39);
            this.buttonTfM30Set.TabIndex = 64;
            this.buttonTfM30Set.Text = "M30";
            this.buttonTfM30Set.UseVisualStyleBackColor = true;
            this.buttonTfM30Set.Click += new System.EventHandler(this.buttonTfM30Set_Click);
            // 
            // buttonTfM5Set
            // 
            this.buttonTfM5Set.Location = new System.Drawing.Point(94, 29);
            this.buttonTfM5Set.Name = "buttonTfM5Set";
            this.buttonTfM5Set.Size = new System.Drawing.Size(56, 39);
            this.buttonTfM5Set.TabIndex = 65;
            this.buttonTfM5Set.Text = "M5";
            this.buttonTfM5Set.UseVisualStyleBackColor = true;
            this.buttonTfM5Set.Click += new System.EventHandler(this.buttonTfM5Set_Click);
            // 
            // buttonTfM15Set
            // 
            this.buttonTfM15Set.Location = new System.Drawing.Point(156, 29);
            this.buttonTfM15Set.Name = "buttonTfM15Set";
            this.buttonTfM15Set.Size = new System.Drawing.Size(56, 39);
            this.buttonTfM15Set.TabIndex = 66;
            this.buttonTfM15Set.Text = "M15";
            this.buttonTfM15Set.UseVisualStyleBackColor = true;
            this.buttonTfM15Set.Click += new System.EventHandler(this.buttonTfM15Set_Click);
            // 
            // buttonInstrumentEurUsdSelect
            // 
            this.buttonInstrumentEurUsdSelect.Location = new System.Drawing.Point(316, 118);
            this.buttonInstrumentEurUsdSelect.Name = "buttonInstrumentEurUsdSelect";
            this.buttonInstrumentEurUsdSelect.Size = new System.Drawing.Size(83, 46);
            this.buttonInstrumentEurUsdSelect.TabIndex = 67;
            this.buttonInstrumentEurUsdSelect.Text = "EURUSD";
            this.buttonInstrumentEurUsdSelect.UseVisualStyleBackColor = true;
            this.buttonInstrumentEurUsdSelect.Click += new System.EventHandler(this.buttonInstrumentEurUsdSelect_Click);
            // 
            // buttonInstrumentGbpUsdSelect
            // 
            this.buttonInstrumentGbpUsdSelect.Location = new System.Drawing.Point(227, 118);
            this.buttonInstrumentGbpUsdSelect.Name = "buttonInstrumentGbpUsdSelect";
            this.buttonInstrumentGbpUsdSelect.Size = new System.Drawing.Size(83, 46);
            this.buttonInstrumentGbpUsdSelect.TabIndex = 67;
            this.buttonInstrumentGbpUsdSelect.Text = "GBPUSD";
            this.buttonInstrumentGbpUsdSelect.UseVisualStyleBackColor = true;
            this.buttonInstrumentGbpUsdSelect.Click += new System.EventHandler(this.buttonInstrumentGbpUsdSelect_Click);
            // 
            // buttonInstrumentAudUsdSelect
            // 
            this.buttonInstrumentAudUsdSelect.Location = new System.Drawing.Point(405, 118);
            this.buttonInstrumentAudUsdSelect.Name = "buttonInstrumentAudUsdSelect";
            this.buttonInstrumentAudUsdSelect.Size = new System.Drawing.Size(83, 46);
            this.buttonInstrumentAudUsdSelect.TabIndex = 67;
            this.buttonInstrumentAudUsdSelect.Text = "AUDUSD";
            this.buttonInstrumentAudUsdSelect.UseVisualStyleBackColor = true;
            this.buttonInstrumentAudUsdSelect.Click += new System.EventHandler(this.buttonInstrumentAudUsdSelect_Click);
            // 
            // buttonInstrumentNzdUsdSelect
            // 
            this.buttonInstrumentNzdUsdSelect.Location = new System.Drawing.Point(494, 118);
            this.buttonInstrumentNzdUsdSelect.Name = "buttonInstrumentNzdUsdSelect";
            this.buttonInstrumentNzdUsdSelect.Size = new System.Drawing.Size(83, 46);
            this.buttonInstrumentNzdUsdSelect.TabIndex = 67;
            this.buttonInstrumentNzdUsdSelect.Text = "NZDUSD";
            this.buttonInstrumentNzdUsdSelect.UseVisualStyleBackColor = true;
            this.buttonInstrumentNzdUsdSelect.Click += new System.EventHandler(this.buttonInstrumentNzdUsdSelect_Click);
            // 
            // buttonInstrumentUsdCadSelect
            // 
            this.buttonInstrumentUsdCadSelect.Location = new System.Drawing.Point(672, 118);
            this.buttonInstrumentUsdCadSelect.Name = "buttonInstrumentUsdCadSelect";
            this.buttonInstrumentUsdCadSelect.Size = new System.Drawing.Size(83, 46);
            this.buttonInstrumentUsdCadSelect.TabIndex = 67;
            this.buttonInstrumentUsdCadSelect.Text = "USDCAD";
            this.buttonInstrumentUsdCadSelect.UseVisualStyleBackColor = true;
            this.buttonInstrumentUsdCadSelect.Click += new System.EventHandler(this.buttonInstrumentUsdCadSelect_Click);
            // 
            // buttonInstrumentUsdChfSelect
            // 
            this.buttonInstrumentUsdChfSelect.Location = new System.Drawing.Point(761, 118);
            this.buttonInstrumentUsdChfSelect.Name = "buttonInstrumentUsdChfSelect";
            this.buttonInstrumentUsdChfSelect.Size = new System.Drawing.Size(83, 46);
            this.buttonInstrumentUsdChfSelect.TabIndex = 67;
            this.buttonInstrumentUsdChfSelect.Text = "USDCHF";
            this.buttonInstrumentUsdChfSelect.UseVisualStyleBackColor = true;
            this.buttonInstrumentUsdChfSelect.Click += new System.EventHandler(this.buttonInstrumentUsdChfSelect_Click);
            // 
            // buttonInstrumentUsdJpySelect
            // 
            this.buttonInstrumentUsdJpySelect.Location = new System.Drawing.Point(850, 118);
            this.buttonInstrumentUsdJpySelect.Name = "buttonInstrumentUsdJpySelect";
            this.buttonInstrumentUsdJpySelect.Size = new System.Drawing.Size(83, 46);
            this.buttonInstrumentUsdJpySelect.TabIndex = 67;
            this.buttonInstrumentUsdJpySelect.Text = "USDJPY";
            this.buttonInstrumentUsdJpySelect.UseVisualStyleBackColor = true;
            this.buttonInstrumentUsdJpySelect.Click += new System.EventHandler(this.buttonInstrumentUsdJpySelect_Click);
            // 
            // buttonInstrumentXauUsdSelect
            // 
            this.buttonInstrumentXauUsdSelect.Location = new System.Drawing.Point(583, 118);
            this.buttonInstrumentXauUsdSelect.Name = "buttonInstrumentXauUsdSelect";
            this.buttonInstrumentXauUsdSelect.Size = new System.Drawing.Size(83, 46);
            this.buttonInstrumentXauUsdSelect.TabIndex = 67;
            this.buttonInstrumentXauUsdSelect.Text = "XAUUSD";
            this.buttonInstrumentXauUsdSelect.UseVisualStyleBackColor = true;
            this.buttonInstrumentXauUsdSelect.Click += new System.EventHandler(this.buttonInstrumentXauUsdSelect_Click);
            // 
            // buttonInstrumentXagusdSelect
            // 
            this.buttonInstrumentXagusdSelect.Location = new System.Drawing.Point(1028, 118);
            this.buttonInstrumentXagusdSelect.Name = "buttonInstrumentXagusdSelect";
            this.buttonInstrumentXagusdSelect.Size = new System.Drawing.Size(83, 46);
            this.buttonInstrumentXagusdSelect.TabIndex = 67;
            this.buttonInstrumentXagusdSelect.Text = "XAGUSD";
            this.buttonInstrumentXagusdSelect.UseVisualStyleBackColor = true;
            this.buttonInstrumentXagusdSelect.Click += new System.EventHandler(this.buttonInstrumentXagusdSelect_Click);
            // 
            // buttonInstrumentEurGpbSelect
            // 
            this.buttonInstrumentEurGpbSelect.Location = new System.Drawing.Point(939, 118);
            this.buttonInstrumentEurGpbSelect.Name = "buttonInstrumentEurGpbSelect";
            this.buttonInstrumentEurGpbSelect.Size = new System.Drawing.Size(83, 46);
            this.buttonInstrumentEurGpbSelect.TabIndex = 67;
            this.buttonInstrumentEurGpbSelect.Text = "EURGPB";
            this.buttonInstrumentEurGpbSelect.UseVisualStyleBackColor = true;
            this.buttonInstrumentEurGpbSelect.Click += new System.EventHandler(this.buttonInstrumentEurGpbSelect_Click);
            // 
            // buttonTfM1Set
            // 
            this.buttonTfM1Set.Location = new System.Drawing.Point(32, 29);
            this.buttonTfM1Set.Name = "buttonTfM1Set";
            this.buttonTfM1Set.Size = new System.Drawing.Size(56, 39);
            this.buttonTfM1Set.TabIndex = 65;
            this.buttonTfM1Set.Text = "M1";
            this.buttonTfM1Set.UseVisualStyleBackColor = true;
            this.buttonTfM1Set.Click += new System.EventHandler(this.buttonTfM1Set_Click);
            // 
            // radioButtonIndicatorLongShort
            // 
            this.radioButtonIndicatorLongShort.AutoSize = true;
            this.radioButtonIndicatorLongShort.Checked = true;
            this.radioButtonIndicatorLongShort.Location = new System.Drawing.Point(20, 57);
            this.radioButtonIndicatorLongShort.Name = "radioButtonIndicatorLongShort";
            this.radioButtonIndicatorLongShort.Size = new System.Drawing.Size(99, 21);
            this.radioButtonIndicatorLongShort.TabIndex = 68;
            this.radioButtonIndicatorLongShort.TabStop = true;
            this.radioButtonIndicatorLongShort.Text = "Long/Short";
            this.radioButtonIndicatorLongShort.UseVisualStyleBackColor = true;
            this.radioButtonIndicatorLongShort.CheckedChanged += new System.EventHandler(this.radioButtonIndicatorLongShort_CheckedChanged);
            // 
            // radioButtonIndicatorSumGrowDelta
            // 
            this.radioButtonIndicatorSumGrowDelta.AutoSize = true;
            this.radioButtonIndicatorSumGrowDelta.Location = new System.Drawing.Point(19, 132);
            this.radioButtonIndicatorSumGrowDelta.Name = "radioButtonIndicatorSumGrowDelta";
            this.radioButtonIndicatorSumGrowDelta.Size = new System.Drawing.Size(131, 21);
            this.radioButtonIndicatorSumGrowDelta.TabIndex = 68;
            this.radioButtonIndicatorSumGrowDelta.Text = "Sum Grow Delta";
            this.radioButtonIndicatorSumGrowDelta.UseVisualStyleBackColor = true;
            this.radioButtonIndicatorSumGrowDelta.CheckedChanged += new System.EventHandler(this.radioButtonIndicatorSumGrowDelta_CheckedChanged);
            // 
            // radioButtonIndicatorOpenInterest
            // 
            this.radioButtonIndicatorOpenInterest.AutoSize = true;
            this.radioButtonIndicatorOpenInterest.Location = new System.Drawing.Point(20, 171);
            this.radioButtonIndicatorOpenInterest.Name = "radioButtonIndicatorOpenInterest";
            this.radioButtonIndicatorOpenInterest.Size = new System.Drawing.Size(115, 21);
            this.radioButtonIndicatorOpenInterest.TabIndex = 68;
            this.radioButtonIndicatorOpenInterest.Text = "Open interest";
            this.radioButtonIndicatorOpenInterest.UseVisualStyleBackColor = true;
            this.radioButtonIndicatorOpenInterest.CheckedChanged += new System.EventHandler(this.radioButtonIndicatorOpenInterest_CheckedChanged);
            // 
            // radioButtonIndicatorGrowDelta
            // 
            this.radioButtonIndicatorGrowDelta.AutoSize = true;
            this.radioButtonIndicatorGrowDelta.Location = new System.Drawing.Point(20, 95);
            this.radioButtonIndicatorGrowDelta.Name = "radioButtonIndicatorGrowDelta";
            this.radioButtonIndicatorGrowDelta.Size = new System.Drawing.Size(99, 21);
            this.radioButtonIndicatorGrowDelta.TabIndex = 68;
            this.radioButtonIndicatorGrowDelta.Text = "Grow Delta";
            this.radioButtonIndicatorGrowDelta.UseVisualStyleBackColor = true;
            this.radioButtonIndicatorGrowDelta.CheckedChanged += new System.EventHandler(this.radioButtonIndicatorGrowDelta_CheckedChanged);
            // 
            // buttonRefresh
            // 
            this.buttonRefresh.Location = new System.Drawing.Point(12, 42);
            this.buttonRefresh.Name = "buttonRefresh";
            this.buttonRefresh.Size = new System.Drawing.Size(209, 64);
            this.buttonRefresh.TabIndex = 69;
            this.buttonRefresh.Text = "Refresh";
            this.buttonRefresh.UseVisualStyleBackColor = true;
            this.buttonRefresh.Click += new System.EventHandler(this.buttonRefresh_Click);
            // 
            // groupBoxSourceData
            // 
            this.groupBoxSourceData.Controls.Add(this.radioButtonSourceDataPositions);
            this.groupBoxSourceData.Controls.Add(this.radioButtonSourceDataLots);
            this.groupBoxSourceData.Location = new System.Drawing.Point(953, 42);
            this.groupBoxSourceData.Name = "groupBoxSourceData";
            this.groupBoxSourceData.Size = new System.Drawing.Size(296, 67);
            this.groupBoxSourceData.TabIndex = 70;
            this.groupBoxSourceData.TabStop = false;
            this.groupBoxSourceData.Text = "Source data";
            // 
            // radioButtonSourceDataPositions
            // 
            this.radioButtonSourceDataPositions.AutoSize = true;
            this.radioButtonSourceDataPositions.Location = new System.Drawing.Point(149, 32);
            this.radioButtonSourceDataPositions.Name = "radioButtonSourceDataPositions";
            this.radioButtonSourceDataPositions.Size = new System.Drawing.Size(86, 21);
            this.radioButtonSourceDataPositions.TabIndex = 0;
            this.radioButtonSourceDataPositions.Text = "Positions";
            this.radioButtonSourceDataPositions.UseVisualStyleBackColor = true;
            this.radioButtonSourceDataPositions.Click += new System.EventHandler(this.radioButtonSourceDataPositions_Click);
            // 
            // radioButtonSourceDataLots
            // 
            this.radioButtonSourceDataLots.AutoSize = true;
            this.radioButtonSourceDataLots.Checked = true;
            this.radioButtonSourceDataLots.Location = new System.Drawing.Point(39, 29);
            this.radioButtonSourceDataLots.Name = "radioButtonSourceDataLots";
            this.radioButtonSourceDataLots.Size = new System.Drawing.Size(56, 21);
            this.radioButtonSourceDataLots.TabIndex = 0;
            this.radioButtonSourceDataLots.TabStop = true;
            this.radioButtonSourceDataLots.Text = "Lots";
            this.radioButtonSourceDataLots.UseVisualStyleBackColor = true;
            this.radioButtonSourceDataLots.Click += new System.EventHandler(this.radioButtonSourceDataLots_Click);
            // 
            // groupBoxCountData
            // 
            this.groupBoxCountData.Controls.Add(this.buttonCountData500);
            this.groupBoxCountData.Controls.Add(this.buttonCountData200);
            this.groupBoxCountData.Controls.Add(this.buttonCountData100);
            this.groupBoxCountData.Controls.Add(this.buttonCountDataAll);
            this.groupBoxCountData.Location = new System.Drawing.Point(705, 38);
            this.groupBoxCountData.Name = "groupBoxCountData";
            this.groupBoxCountData.Size = new System.Drawing.Size(242, 76);
            this.groupBoxCountData.TabIndex = 71;
            this.groupBoxCountData.TabStop = false;
            this.groupBoxCountData.Text = "CountData";
            // 
            // buttonCountData500
            // 
            this.buttonCountData500.Location = new System.Drawing.Point(186, 22);
            this.buttonCountData500.Name = "buttonCountData500";
            this.buttonCountData500.Size = new System.Drawing.Size(50, 48);
            this.buttonCountData500.TabIndex = 0;
            this.buttonCountData500.Text = "500";
            this.buttonCountData500.UseVisualStyleBackColor = true;
            this.buttonCountData500.Click += new System.EventHandler(this.buttonCountData500_Click);
            // 
            // buttonCountData200
            // 
            this.buttonCountData200.Location = new System.Drawing.Point(116, 21);
            this.buttonCountData200.Name = "buttonCountData200";
            this.buttonCountData200.Size = new System.Drawing.Size(64, 48);
            this.buttonCountData200.TabIndex = 0;
            this.buttonCountData200.Text = "200";
            this.buttonCountData200.UseVisualStyleBackColor = true;
            this.buttonCountData200.Click += new System.EventHandler(this.buttonCountData200_Click);
            // 
            // buttonCountData100
            // 
            this.buttonCountData100.Location = new System.Drawing.Point(59, 21);
            this.buttonCountData100.Name = "buttonCountData100";
            this.buttonCountData100.Size = new System.Drawing.Size(51, 48);
            this.buttonCountData100.TabIndex = 0;
            this.buttonCountData100.Text = "100";
            this.buttonCountData100.UseVisualStyleBackColor = true;
            this.buttonCountData100.Click += new System.EventHandler(this.buttonCountData100_Click);
            // 
            // buttonCountDataAll
            // 
            this.buttonCountDataAll.Location = new System.Drawing.Point(11, 21);
            this.buttonCountDataAll.Name = "buttonCountDataAll";
            this.buttonCountDataAll.Size = new System.Drawing.Size(45, 48);
            this.buttonCountDataAll.TabIndex = 0;
            this.buttonCountDataAll.Text = "ALL";
            this.buttonCountDataAll.UseVisualStyleBackColor = true;
            this.buttonCountDataAll.Click += new System.EventHandler(this.buttonCountDataAll_Click);
            // 
            // labelCurrentTime
            // 
            this.labelCurrentTime.AutoSize = true;
            this.labelCurrentTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelCurrentTime.Location = new System.Drawing.Point(10, 13);
            this.labelCurrentTime.Name = "labelCurrentTime";
            this.labelCurrentTime.Size = new System.Drawing.Size(89, 20);
            this.labelCurrentTime.TabIndex = 72;
            this.labelCurrentTime.Text = "10.10.2016";
            // 
            // labelCandleData
            // 
            this.labelCandleData.AutoSize = true;
            this.labelCandleData.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelCandleData.Location = new System.Drawing.Point(232, 13);
            this.labelCandleData.Name = "labelCandleData";
            this.labelCandleData.Size = new System.Drawing.Size(132, 20);
            this.labelCandleData.TabIndex = 73;
            this.labelCandleData.Text = "labelCandleData";
            // 
            // labelIndicatorData
            // 
            this.labelIndicatorData.AutoSize = true;
            this.labelIndicatorData.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelIndicatorData.Location = new System.Drawing.Point(546, 13);
            this.labelIndicatorData.Name = "labelIndicatorData";
            this.labelIndicatorData.Size = new System.Drawing.Size(53, 20);
            this.labelIndicatorData.TabIndex = 74;
            this.labelIndicatorData.Text = "label1";
            // 
            // textBoxDataProjectionValue
            // 
            this.textBoxDataProjectionValue.Location = new System.Drawing.Point(13, 80);
            this.textBoxDataProjectionValue.Name = "textBoxDataProjectionValue";
            this.textBoxDataProjectionValue.Size = new System.Drawing.Size(127, 22);
            this.textBoxDataProjectionValue.TabIndex = 77;
            this.textBoxDataProjectionValue.TextChanged += new System.EventHandler(this.textBoxDataProjectionValue_TextChanged);
            this.textBoxDataProjectionValue.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxLim_KeyDown);
            // 
            // groupBoxDataProjection
            // 
            this.groupBoxDataProjection.Controls.Add(this.labelDataProjAveragePeriod);
            this.groupBoxDataProjection.Controls.Add(this.comboBoxDataProjectionType);
            this.groupBoxDataProjection.Controls.Add(this.textBoxDataProjectionValue);
            this.groupBoxDataProjection.Location = new System.Drawing.Point(6, 198);
            this.groupBoxDataProjection.Name = "groupBoxDataProjection";
            this.groupBoxDataProjection.Size = new System.Drawing.Size(174, 113);
            this.groupBoxDataProjection.TabIndex = 78;
            this.groupBoxDataProjection.TabStop = false;
            this.groupBoxDataProjection.Text = "Data projection";
            // 
            // labelDataProjAveragePeriod
            // 
            this.labelDataProjAveragePeriod.AutoSize = true;
            this.labelDataProjAveragePeriod.Location = new System.Drawing.Point(11, 60);
            this.labelDataProjAveragePeriod.Name = "labelDataProjAveragePeriod";
            this.labelDataProjAveragePeriod.Size = new System.Drawing.Size(140, 17);
            this.labelDataProjAveragePeriod.TabIndex = 78;
            this.labelDataProjAveragePeriod.Text = "Период усреднения";
            // 
            // comboBoxDataProjectionType
            // 
            this.comboBoxDataProjectionType.FormattingEnabled = true;
            this.comboBoxDataProjectionType.Location = new System.Drawing.Point(14, 21);
            this.comboBoxDataProjectionType.Name = "comboBoxDataProjectionType";
            this.comboBoxDataProjectionType.Size = new System.Drawing.Size(126, 24);
            this.comboBoxDataProjectionType.TabIndex = 0;
            this.comboBoxDataProjectionType.SelectedIndexChanged += new System.EventHandler(this.comboBoxDataProjectionType_SelectedIndexChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem,
            this.SettingstoolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1348, 28);
            this.menuStrip1.TabIndex = 79;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(45, 24);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // SettingstoolStripMenuItem
            // 
            this.SettingstoolStripMenuItem.Name = "SettingstoolStripMenuItem";
            this.SettingstoolStripMenuItem.Size = new System.Drawing.Size(74, 24);
            this.SettingstoolStripMenuItem.Text = "Settings";
            this.SettingstoolStripMenuItem.Click += new System.EventHandler(this.settingstoolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(62, 24);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // groupBoxTimeFrame
            // 
            this.groupBoxTimeFrame.Controls.Add(this.buttonTfM30Set);
            this.groupBoxTimeFrame.Controls.Add(this.buttonTfM5Set);
            this.groupBoxTimeFrame.Controls.Add(this.buttonTfM1Set);
            this.groupBoxTimeFrame.Controls.Add(this.buttonTfH1Set);
            this.groupBoxTimeFrame.Controls.Add(this.buttonTfH4Set);
            this.groupBoxTimeFrame.Controls.Add(this.buttonTfD1Set);
            this.groupBoxTimeFrame.Controls.Add(this.buttonTfM15Set);
            this.groupBoxTimeFrame.Location = new System.Drawing.Point(227, 38);
            this.groupBoxTimeFrame.Name = "groupBoxTimeFrame";
            this.groupBoxTimeFrame.Size = new System.Drawing.Size(472, 74);
            this.groupBoxTimeFrame.TabIndex = 80;
            this.groupBoxTimeFrame.TabStop = false;
            this.groupBoxTimeFrame.Text = "TimeFrame";
            // 
            // groupBoxIndicators
            // 
            this.groupBoxIndicators.Controls.Add(this.checkBoxIndicatorVisible);
            this.groupBoxIndicators.Controls.Add(this.radioButtonIndicatorLongShort);
            this.groupBoxIndicators.Controls.Add(this.radioButtonIndicatorSumGrowDelta);
            this.groupBoxIndicators.Controls.Add(this.radioButtonIndicatorGrowDelta);
            this.groupBoxIndicators.Controls.Add(this.groupBoxDataProjection);
            this.groupBoxIndicators.Controls.Add(this.radioButtonIndicatorOpenInterest);
            this.groupBoxIndicators.Location = new System.Drawing.Point(21, 111);
            this.groupBoxIndicators.Name = "groupBoxIndicators";
            this.groupBoxIndicators.Size = new System.Drawing.Size(200, 319);
            this.groupBoxIndicators.TabIndex = 81;
            this.groupBoxIndicators.TabStop = false;
            this.groupBoxIndicators.Text = "Indicators";
            // 
            // checkBoxIndicatorVisible
            // 
            this.checkBoxIndicatorVisible.AutoSize = true;
            this.checkBoxIndicatorVisible.Checked = true;
            this.checkBoxIndicatorVisible.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxIndicatorVisible.Location = new System.Drawing.Point(20, 21);
            this.checkBoxIndicatorVisible.Name = "checkBoxIndicatorVisible";
            this.checkBoxIndicatorVisible.Size = new System.Drawing.Size(71, 21);
            this.checkBoxIndicatorVisible.TabIndex = 79;
            this.checkBoxIndicatorVisible.Text = "Visible";
            this.checkBoxIndicatorVisible.UseVisualStyleBackColor = true;
            this.checkBoxIndicatorVisible.CheckedChanged += new System.EventHandler(this.checkBoxIndicatorVisible_CheckedChanged);
            // 
            // labelCurrentMode
            // 
            this.labelCurrentMode.AutoSize = true;
            this.labelCurrentMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelCurrentMode.Location = new System.Drawing.Point(604, 180);
            this.labelCurrentMode.Name = "labelCurrentMode";
            this.labelCurrentMode.Size = new System.Drawing.Size(311, 29);
            this.labelCurrentMode.TabIndex = 82;
            this.labelCurrentMode.Text = "EURUSD - H1 - 200Candles";
            // 
            // groupBoxCandlesProjection
            // 
            this.groupBoxCandlesProjection.Controls.Add(this.labelCandlesProjAveragePerio);
            this.groupBoxCandlesProjection.Controls.Add(this.comboBoxCandleProjection);
            this.groupBoxCandlesProjection.Controls.Add(this.textBoxCandleProjectionData);
            this.groupBoxCandlesProjection.Location = new System.Drawing.Point(21, 436);
            this.groupBoxCandlesProjection.Name = "groupBoxCandlesProjection";
            this.groupBoxCandlesProjection.Size = new System.Drawing.Size(200, 114);
            this.groupBoxCandlesProjection.TabIndex = 78;
            this.groupBoxCandlesProjection.TabStop = false;
            this.groupBoxCandlesProjection.Text = "Candles projection";
            // 
            // labelCandlesProjAveragePerio
            // 
            this.labelCandlesProjAveragePerio.AutoSize = true;
            this.labelCandlesProjAveragePerio.Location = new System.Drawing.Point(17, 59);
            this.labelCandlesProjAveragePerio.Name = "labelCandlesProjAveragePerio";
            this.labelCandlesProjAveragePerio.Size = new System.Drawing.Size(140, 17);
            this.labelCandlesProjAveragePerio.TabIndex = 78;
            this.labelCandlesProjAveragePerio.Text = "Период усреднения";
            // 
            // comboBoxCandleProjection
            // 
            this.comboBoxCandleProjection.FormattingEnabled = true;
            this.comboBoxCandleProjection.Location = new System.Drawing.Point(19, 21);
            this.comboBoxCandleProjection.Name = "comboBoxCandleProjection";
            this.comboBoxCandleProjection.Size = new System.Drawing.Size(169, 24);
            this.comboBoxCandleProjection.TabIndex = 0;
            this.comboBoxCandleProjection.SelectedIndexChanged += new System.EventHandler(this.comboBoxCandleProjection_SelectedIndexChanged);
            // 
            // textBoxCandleProjectionData
            // 
            this.textBoxCandleProjectionData.Location = new System.Drawing.Point(19, 79);
            this.textBoxCandleProjectionData.Name = "textBoxCandleProjectionData";
            this.textBoxCandleProjectionData.Size = new System.Drawing.Size(127, 22);
            this.textBoxCandleProjectionData.TabIndex = 77;
            this.textBoxCandleProjectionData.TextChanged += new System.EventHandler(this.textBoxCandleProjectionData_TextChanged);
            this.textBoxCandleProjectionData.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxLim_KeyDown);
            // 
            // panelChartCounters
            // 
            this.panelChartCounters.Controls.Add(this.labelCurrentTime);
            this.panelChartCounters.Controls.Add(this.labelCandleData);
            this.panelChartCounters.Controls.Add(this.labelIndicatorData);
            this.panelChartCounters.Location = new System.Drawing.Point(227, 660);
            this.panelChartCounters.Name = "panelChartCounters";
            this.panelChartCounters.Size = new System.Drawing.Size(1109, 58);
            this.panelChartCounters.TabIndex = 83;
            // 
            // MfbToolsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1348, 721);
            this.Controls.Add(this.panelChartCounters);
            this.Controls.Add(this.labelCurrentMode);
            this.Controls.Add(this.groupBoxIndicators);
            this.Controls.Add(this.groupBoxTimeFrame);
            this.Controls.Add(this.groupBoxCandlesProjection);
            this.Controls.Add(this.groupBoxCountData);
            this.Controls.Add(this.groupBoxSourceData);
            this.Controls.Add(this.buttonRefresh);
            this.Controls.Add(this.buttonInstrumentEurGpbSelect);
            this.Controls.Add(this.buttonInstrumentXagusdSelect);
            this.Controls.Add(this.buttonInstrumentXauUsdSelect);
            this.Controls.Add(this.buttonInstrumentUsdJpySelect);
            this.Controls.Add(this.buttonInstrumentUsdChfSelect);
            this.Controls.Add(this.buttonInstrumentUsdCadSelect);
            this.Controls.Add(this.buttonInstrumentGbpUsdSelect);
            this.Controls.Add(this.buttonInstrumentNzdUsdSelect);
            this.Controls.Add(this.buttonInstrumentAudUsdSelect);
            this.Controls.Add(this.buttonInstrumentEurUsdSelect);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MfbToolsForm";
            this.Text = "MfbTools v. 1.0.3.5";
            this.Load += new System.EventHandler(this.mfbToolsForm_Load);
            this.SizeChanged += new System.EventHandler(this.MfbToolsForm_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.groupBoxSourceData.ResumeLayout(false);
            this.groupBoxSourceData.PerformLayout();
            this.groupBoxCountData.ResumeLayout(false);
            this.groupBoxDataProjection.ResumeLayout(false);
            this.groupBoxDataProjection.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBoxTimeFrame.ResumeLayout(false);
            this.groupBoxIndicators.ResumeLayout(false);
            this.groupBoxIndicators.PerformLayout();
            this.groupBoxCandlesProjection.ResumeLayout(false);
            this.groupBoxCandlesProjection.PerformLayout();
            this.panelChartCounters.ResumeLayout(false);
            this.panelChartCounters.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Chart chart1;
        private Button buttonTfD1Set;
        private Button buttonTfH4Set;
        private Button buttonTfH1Set;
        private Button buttonTfM30Set;
        private Button buttonTfM5Set;
        private Button buttonTfM15Set;
        private Button buttonInstrumentEurUsdSelect;
        private Button buttonInstrumentGbpUsdSelect;
        private Button buttonInstrumentAudUsdSelect;
        private Button buttonInstrumentNzdUsdSelect;
        private Button buttonInstrumentUsdCadSelect;
        private Button buttonInstrumentUsdChfSelect;
        private Button buttonInstrumentUsdJpySelect;
        private Button buttonInstrumentXauUsdSelect;
        private Button buttonInstrumentXagusdSelect;
        private Button buttonInstrumentEurGpbSelect;
        private Button buttonTfM1Set;
        private RadioButton radioButtonIndicatorLongShort;
        private RadioButton radioButtonIndicatorSumGrowDelta;
        private RadioButton radioButtonIndicatorOpenInterest;
        private RadioButton radioButtonIndicatorGrowDelta;
        private Button buttonRefresh;
        private GroupBox groupBoxSourceData;
        private RadioButton radioButtonSourceDataPositions;
        private RadioButton radioButtonSourceDataLots;
        private GroupBox groupBoxCountData;
        private Button buttonCountData500;
        private Button buttonCountData200;
        private Button buttonCountData100;
        private Button buttonCountDataAll;
        private Label labelCurrentTime;
        private Label labelCandleData;
        private Label labelIndicatorData;
        private TextBox textBoxDataProjectionValue;
        private GroupBox groupBoxDataProjection;
        private ComboBox comboBoxDataProjectionType;
        private MenuStrip menuStrip1;
        private ToolStripMenuItem exitToolStripMenuItem;
        private ToolStripMenuItem SettingstoolStripMenuItem;
        private ToolStripMenuItem aboutToolStripMenuItem;
        private GroupBox groupBoxTimeFrame;
        private GroupBox groupBoxIndicators;
        private Label labelCurrentMode;
        private GroupBox groupBoxCandlesProjection;
        private ComboBox comboBoxCandleProjection;
        private TextBox textBoxCandleProjectionData;
        private CheckBox checkBoxIndicatorVisible;
        private Panel panelChartCounters;
        private Label labelDataProjAveragePeriod;
        private Label labelCandlesProjAveragePerio;
    }
}