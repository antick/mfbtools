﻿namespace Antick.MfbTools.Mvp.Views
{
    partial class AboutForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelTitle = new System.Windows.Forms.Label();
            this.labelSharkFxMention = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labelContracts = new System.Windows.Forms.Label();
            this.linkLabelMailtoAuthor = new System.Windows.Forms.LinkLabel();
            this.linkLabelSharkFx = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelTitle.Location = new System.Drawing.Point(114, 9);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(132, 32);
            this.labelTitle.TabIndex = 0;
            this.labelTitle.Text = "MfbTools";
            // 
            // labelSharkFxMention
            // 
            this.labelSharkFxMention.AutoSize = true;
            this.labelSharkFxMention.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelSharkFxMention.Location = new System.Drawing.Point(84, 75);
            this.labelSharkFxMention.Name = "labelSharkFxMention";
            this.labelSharkFxMention.Size = new System.Drawing.Size(171, 20);
            this.labelSharkFxMention.TabIndex = 1;
            this.labelSharkFxMention.Text = "Developed special for";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(238, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(132, 32);
            this.label2.TabIndex = 0;
            this.label2.Text = "v. 1.0.4.0";
            // 
            // labelContracts
            // 
            this.labelContracts.AutoSize = true;
            this.labelContracts.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelContracts.Location = new System.Drawing.Point(84, 115);
            this.labelContracts.Name = "labelContracts";
            this.labelContracts.Size = new System.Drawing.Size(159, 20);
            this.labelContracts.TabIndex = 2;
            this.labelContracts.Text = "Contact with author:";
            // 
            // linkLabelMailtoAuthor
            // 
            this.linkLabelMailtoAuthor.AutoSize = true;
            this.linkLabelMailtoAuthor.Location = new System.Drawing.Point(261, 118);
            this.linkLabelMailtoAuthor.Name = "linkLabelMailtoAuthor";
            this.linkLabelMailtoAuthor.Size = new System.Drawing.Size(158, 17);
            this.linkLabelMailtoAuthor.TabIndex = 3;
            this.linkLabelMailtoAuthor.TabStop = true;
            this.linkLabelMailtoAuthor.Text = "mailto:ithkul@yandex.ru";
            this.linkLabelMailtoAuthor.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // linkLabelSharkFx
            // 
            this.linkLabelSharkFx.AutoSize = true;
            this.linkLabelSharkFx.Location = new System.Drawing.Point(261, 78);
            this.linkLabelSharkFx.Name = "linkLabelSharkFx";
            this.linkLabelSharkFx.Size = new System.Drawing.Size(76, 17);
            this.linkLabelSharkFx.TabIndex = 4;
            this.linkLabelSharkFx.TabStop = true;
            this.linkLabelSharkFx.Text = "SharkFx.ru";
            this.linkLabelSharkFx.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel2_LinkClicked);
            // 
            // AboutForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(521, 165);
            this.Controls.Add(this.linkLabelSharkFx);
            this.Controls.Add(this.linkLabelMailtoAuthor);
            this.Controls.Add(this.labelContracts);
            this.Controls.Add(this.labelSharkFxMention);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.labelTitle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AboutForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "About";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Label labelSharkFxMention;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelContracts;
        private System.Windows.Forms.LinkLabel linkLabelMailtoAuthor;
        private System.Windows.Forms.LinkLabel linkLabelSharkFx;
    }
}