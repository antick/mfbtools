﻿using System;
using Antick.MfbTools.Domain;
using Antick.MfbTools.Domain.Enums;
using Antick.MfbTools.Domain.Render;
using Antick.MfbTools.Infractructure.Mvp.Views;
using Antick.MfbTools.Mvp.Args;

namespace Antick.MfbTools.Mvp.Views
{
    public interface IMfbToolsView : IView
    {
        /// <summary>
        /// Генерируется после первой отрисовки приложения
        /// </summary>
        event EventHandler<EventArgs> ViewStarted; 

        /// <summary>
        /// Изменяется TimeFrame
        /// </summary>
        event EventHandler<TimeFrameChangingEventArgs> TimeFrameChanging;

        /// <summary>
        /// Изменяеться Instrument
        /// </summary>
        event EventHandler<InstrumentChangingEventArgs> InstrumentChanging;

        /// <summary>
        /// Изменется Indicator
        /// </summary>
        event EventHandler<IndicatorChangingEventArgs> IndicatorChanging;

        /// <summary>
        /// Старт полной отрисовки с получением данных
        /// </summary>
        event EventHandler<EventArgs> RefreshStarting;

        /// <summary>
        /// Изменяется SourceData
        /// </summary>
        event EventHandler<SourceDataChangingEventArgs> SourceDataChanging;

        /// <summary>
        /// Изменяется CountData
        /// </summary>
        event EventHandler<CountDataEventArgs> CountDataChanging;

        /// <summary>
        /// Изменяется DataProjectionType
        /// </summary>
        event EventHandler<SourceDataProjectionTypeChangedEventArgs> DataProjectionTypeChanged;

        /// <summary>
        /// Изменяется DataProjectionValue
        /// </summary>
        event EventHandler<SourceDataProjectionValueChangedEventArgs> DataProjectionValueChanged;

        /// <summary>
        /// Запуск нового цикла рендеринга
        /// </summary>
        event EventHandler<EventArgs> RenderStarting;
        
        /// <summary>
        /// Запуск окна настроек
        /// </summary>
        event EventHandler<EventArgs> ShowingSettings;

        /// <summary>
        /// Нажатие кнопки выхода в меню
        /// </summary>
        event EventHandler<EventArgs> ExitingApplication;

        /// <summary>
        /// Запуск окна "О программе"
        /// </summary>
        event EventHandler<EventArgs> ShowingAbout;

        event EventHandler<CandleProjectionTypeChangingEventArgs> CandleProjectionTypeChanging;
        event EventHandler<CandleProjectionValueChangedEventArgs> CandleProjectionValueChanging;

        void LoadInitDatas(InitData initData);

        void LoadComboBoxes(SourceDataProjectionType[] comboBoxDataProjectionTypeDataSource, CandleProjectionType[] comboBoxCandleProjectionDataSource);

        /// <summary>
        /// Отрисовка
        /// </summary>
        /// <param name="renderData"></param>
        void Render(RenderData renderData);

        void UpdateCurrentMode(CurrentInstrumentView instrumentView);

        /// <summary>
        /// Применеие Локализации
        /// </summary>
        void ApplyLocalization();

        void ShowTrialMessageExpired();

        void RenderError();

    }
}
