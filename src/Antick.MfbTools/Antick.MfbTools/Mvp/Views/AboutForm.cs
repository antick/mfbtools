﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Antick.MfbTools.Extensions;
using Antick.MfbTools.Resources;

namespace Antick.MfbTools.Mvp.Views
{
    public partial class AboutForm : Form, IAboutView
    {
        public AboutForm()
        {
            InitializeComponent();
            ApplyLocalization();
        }

        public new void Show()
        {
            ShowDialog();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("mailto:ithkul@yandex.ru");
        }

        public void ApplyLocalization()
        {
            this.InvokeAsync(c =>
            {
                this.Text = Localization.About;
                labelSharkFxMention.Text = Localization.SharkFxMention;
                labelContracts.Text = Localization.ContractAuthor + ":";
            });

            this.InvokeAsync(c =>
            {
                var rightMention = labelSharkFxMention.Left + labelSharkFxMention.Width;
                var rightContracts = labelContracts.Left + labelContracts.Width;

                var max = new[] {rightMention, rightContracts}.Max();

                linkLabelSharkFx.Left = max;
                linkLabelMailtoAuthor.Left = max;
            });
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("http://sharkfx.ru");
        }
    }
}
