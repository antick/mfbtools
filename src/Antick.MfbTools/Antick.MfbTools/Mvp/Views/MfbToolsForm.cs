﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using Antick.Contracts;
using Antick.MfbTools.Domain;
using Antick.MfbTools.Domain.Enums;
using Antick.MfbTools.Domain.Render;
using Antick.MfbTools.Extensions;
using Antick.MfbTools.Mvp.Args;
using Antick.MfbTools.Resources;
using Antick.MfbTools.Utils;

namespace Antick.MfbTools.Mvp.Views
{
    public partial class MfbToolsForm : Form, IMfbToolsView
    {
        private readonly ApplicationContext m_Context;

        public event EventHandler<EventArgs> ViewStarted;
        public event EventHandler<TimeFrameChangingEventArgs> TimeFrameChanging;
        public event EventHandler<InstrumentChangingEventArgs> InstrumentChanging;
        public event EventHandler<IndicatorChangingEventArgs> IndicatorChanging;
        public event EventHandler<EventArgs> RefreshStarting;
        public event EventHandler<SourceDataChangingEventArgs> SourceDataChanging;
        public event EventHandler<CountDataEventArgs> CountDataChanging;
        public event EventHandler<SourceDataProjectionTypeChangedEventArgs> DataProjectionTypeChanged;
        public event EventHandler<SourceDataProjectionValueChangedEventArgs> DataProjectionValueChanged;
        public event EventHandler<EventArgs> RenderStarting;
        public event EventHandler<EventArgs> ShowingSettings;
        public event EventHandler<EventArgs> ExitingApplication;
        public event EventHandler<EventArgs> ShowingAbout;
        public event EventHandler<CandleProjectionTypeChangingEventArgs> CandleProjectionTypeChanging;
        public event EventHandler<CandleProjectionValueChangedEventArgs> CandleProjectionValueChanging;

        private int lastPointIndex;
        private RenderData LastrenderData;

        private InitData InitData;

        public MfbToolsForm(ApplicationContext context)
        {
            m_Context = context;
            InitializeComponent();

          
        }

        public new void Show()
        {
            m_Context.MainForm = this;
            Application.Run(m_Context);
            Application.ThreadException += Application_ThreadException;
        }

        #region Combobox and textbox convertation

        private int DataProjectionValue
        {
            get
            {
                try
                {
                    return Convert.ToInt32(textBoxDataProjectionValue.Text);
                }
                catch
                {
                    return 0;
                }
            }
        }

        private SourceDataProjectionType DataProjectionType
        {
            get
            {
                try
                {
                    return ((KeyValuePair<SourceDataProjectionType,string>) comboBoxDataProjectionType.SelectedItem).Key;
                }
                catch (Exception e)
                {
                    return SourceDataProjectionType.None;
                }
            }
        }
        
        private CandleProjectionType CandleProjectionType
        {
            get
            {
                try
                {
                    return ((KeyValuePair<CandleProjectionType, string>)comboBoxCandleProjection.SelectedItem).Key;
                }
                catch (Exception e)
                {
                    return CandleProjectionType.None;
                }
            }
        }

        private int CandleProjectionValue
        {
            get
            {
                try
                {
                    return Convert.ToInt32(textBoxCandleProjectionData.Text);
                }
                catch
                {
                    return 0;
                }
            }
        }

#endregion

        #region Change Instrument and TimeFrame buttons 

        private void buttonTfM1Set_Click(object sender, EventArgs e)
        {
            TimeFrameChanging.Raise(this, new TimeFrameChangingEventArgs {TimeFrame = TimeFrame.M1});
        }

        private void buttonTfM5Set_Click(object sender, EventArgs e)
        {
            TimeFrameChanging.Raise(this, new TimeFrameChangingEventArgs {TimeFrame = TimeFrame.M5});
        }

        private void buttonTfM15Set_Click(object sender, EventArgs e)
        {
            TimeFrameChanging.Raise(this, new TimeFrameChangingEventArgs {TimeFrame = TimeFrame.M15});
        }

        private void buttonTfM30Set_Click(object sender, EventArgs e)
        {
            TimeFrameChanging.Raise(this, new TimeFrameChangingEventArgs {TimeFrame = TimeFrame.M30});
        }

        private void buttonTfH1Set_Click(object sender, EventArgs e)
        {
            TimeFrameChanging.Raise(this, new TimeFrameChangingEventArgs {TimeFrame = TimeFrame.H1});
        }

        private void buttonTfH4Set_Click(object sender, EventArgs e)
        {
            TimeFrameChanging.Raise(this, new TimeFrameChangingEventArgs {TimeFrame = TimeFrame.H4});
        }

        private void buttonTfD1Set_Click(object sender, EventArgs e)
        {
            TimeFrameChanging.Raise(this, new TimeFrameChangingEventArgs {TimeFrame = TimeFrame.D1});
        }

        private void buttonInstrumentGbpUsdSelect_Click(object sender, EventArgs e)
        {
            InstrumentChanging.Raise(this, new InstrumentChangingEventArgs {Instrument = Instrument.GBP_USD});
        }

        private void buttonInstrumentEurUsdSelect_Click(object sender, EventArgs e)
        {
            InstrumentChanging.Raise(this, new InstrumentChangingEventArgs {Instrument = Instrument.EUR_USD});
        }

        private void buttonInstrumentAudUsdSelect_Click(object sender, EventArgs e)
        {
            InstrumentChanging.Raise(this, new InstrumentChangingEventArgs {Instrument = Instrument.AUD_USD});
        }

        private void buttonInstrumentNzdUsdSelect_Click(object sender, EventArgs e)
        {
            InstrumentChanging.Raise(this, new InstrumentChangingEventArgs {Instrument = Instrument.NZD_USD});
        }

        private void buttonInstrumentXauUsdSelect_Click(object sender, EventArgs e)
        {
            InstrumentChanging.Raise(this, new InstrumentChangingEventArgs {Instrument = Instrument.XAU_USD});
        }

        private void buttonInstrumentUsdCadSelect_Click(object sender, EventArgs e)
        {
            InstrumentChanging.Raise(this, new InstrumentChangingEventArgs {Instrument = Instrument.USD_CAD});
        }

        private void buttonInstrumentUsdChfSelect_Click(object sender, EventArgs e)
        {
            InstrumentChanging.Raise(this, new InstrumentChangingEventArgs {Instrument = Instrument.USD_CHF});
        }

        private void buttonInstrumentUsdJpySelect_Click(object sender, EventArgs e)
        {
            InstrumentChanging.Raise(this, new InstrumentChangingEventArgs {Instrument = Instrument.USD_JPY});
        }

        private void buttonInstrumentEurGpbSelect_Click(object sender, EventArgs e)
        {
            InstrumentChanging.Raise(this, new InstrumentChangingEventArgs {Instrument = Instrument.EUR_GBP});
        }

        private void buttonInstrumentXagusdSelect_Click(object sender, EventArgs e)
        {
            InstrumentChanging.Raise(this, new InstrumentChangingEventArgs {Instrument = Instrument.XAG_USD});
        }

        #endregion

        #region Change Indicator Type

        private void radioButtonIndicatorLongShort_CheckedChanged(object sender, EventArgs e)
        {
            IndicatorChanging.Raise(this, new IndicatorChangingEventArgs {Indicator = IndicatorType.LongShort});
        }

        private void radioButtonIndicatorSumGrowDelta_CheckedChanged(object sender, EventArgs e)
        {
            IndicatorChanging.Raise(this, new IndicatorChangingEventArgs {Indicator = IndicatorType.SumGrowDelta});
        }

        private void radioButtonIndicatorGrowDelta_CheckedChanged(object sender, EventArgs e)
        {
            IndicatorChanging.Raise(this, new IndicatorChangingEventArgs {Indicator = IndicatorType.GrowDelta});
        }

        private void radioButtonIndicatorOpenInterest_CheckedChanged(object sender, EventArgs e)
        {
            IndicatorChanging.Raise(this, new IndicatorChangingEventArgs {Indicator = IndicatorType.OpenInterest});
        }

        private void radioButtonIndicatorLongDeltaShortDelta_Click(object sender, EventArgs e)
        {
            IndicatorChanging.Raise(this,
                new IndicatorChangingEventArgs {Indicator = IndicatorType.LongDeltaShortDelta});
        }

        #endregion

        #region Change Source data radio buttons

        private void radioButtonSourceDataPositions_Click(object sender, EventArgs e)
        {
            SourceDataChanging.Raise(this, new SourceDataChangingEventArgs {SourceData = SourceData.Positions});
        }

        private void radioButtonSourceDataLots_Click(object sender, EventArgs e)
        {
            SourceDataChanging.Raise(this, new SourceDataChangingEventArgs {SourceData = SourceData.Lots});
        }

        #endregion

        #region Change CountData buttons

        private void buttonCountDataAll_Click(object sender, EventArgs e)
        {
            CountDataChanging.Raise(this, new CountDataEventArgs {CountData = CountData.All});
        }

        private void buttonCountData100_Click(object sender, EventArgs e)
        {
            CountDataChanging.Raise(this, new CountDataEventArgs {CountData = CountData.C100});
        }

        private void buttonCountData200_Click(object sender, EventArgs e)
        {
            CountDataChanging.Raise(this, new CountDataEventArgs {CountData = CountData.C200});
        }

        private void buttonCountData500_Click(object sender, EventArgs e)
        {
            CountDataChanging.Raise(this, new CountDataEventArgs {CountData = CountData.C500});
        }

        #endregion

        #region toolStripMenu Items 

        private void settingstoolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowingSettings.Raise(this, new EventArgs());
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ExitingApplication.Raise(this, new EventArgs());
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowingAbout.Raise(this, new EventArgs());
        }

        #endregion

        #region misc EventHandlers

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            RefreshStarting.Raise(this, new EventArgs());
        }

        private void textBoxLim_KeyDown(object sender, KeyEventArgs e)
        {
        }

        private void comboBoxDataProjectionType_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataProjectionTypeChanged.Raise(this,
                new SourceDataProjectionTypeChangedEventArgs {ProjectionType = DataProjectionType});
            RenderStarting.Raise(this, new EventArgs());
        }

        private void textBoxDataProjectionValue_TextChanged(object sender, EventArgs e)
        {
            DataProjectionValueChanged.Raise(this,
                new SourceDataProjectionValueChangedEventArgs {ProjectionValue = DataProjectionValue});

            RenderStarting.Raise(this,new EventArgs());
        }

        private void chart1_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                var hitTestResult = chart1.HitTest(e.Location.X, e.Location.Y, ChartElementType.DataPoint);
                if (hitTestResult != null && hitTestResult.ChartElementType != ChartElementType.Nothing)
                {
                    var point = hitTestResult.PointIndex;
                    lastPointIndex = point;
                    onCursorDataCatching(point);
                }

            }
            catch (ArgumentException ex)
            {
            }
        }

        private void textBoxCandleProjectionData_TextChanged(object sender, EventArgs e)
        {
            CandleProjectionValueChanging.Raise(this,
                new CandleProjectionValueChangedEventArgs {Value = CandleProjectionValue});
        }

        private void comboBoxCandleProjection_SelectedIndexChanged(object sender, EventArgs e)
        {
            CandleProjectionTypeChanging.Raise(this,
                new CandleProjectionTypeChangingEventArgs {Type = CandleProjectionType});
        }

        private void mfbToolsForm_Load(object sender, EventArgs e)
        {
            ApplyLocalization();
            ViewStarted.Raise(this, new EventArgs());
        }

        #endregion

        public void RenderInner(RenderData renderData)
        {
           

            LastrenderData = renderData;

            foreach (var s in chart1.Series)
            {
                s.Points.Clear();
            }

            var count = renderData.Candles.Count;

            chart1.ChartAreas[0].AxisX.ScaleView.Size = count + 1;
            chart1.ChartAreas[1].AxisX.ScaleView.Size = count + 1;
            chart1.ChartAreas[0].AxisX.ScaleView.Position = 0;
            chart1.ChartAreas[0].AxisY.LabelStyle.Format = renderData.Spec.Format;

            #region Draw Bars

            if (renderData.CandleProjectionType == CandleProjectionType.None)
            {
                for (int index = 0; index < renderData.Candles.Count; index++)
                {
                    var data = renderData.Candles[index];

                    chart1.Series["BarsStub"].Points.Add(new DataPoint(data.Time.ToOADate(), 1000));
                    chart1.Series["Bars"].Points.Add(new DataPoint(data.Time.ToOADate(),
                        new[] {data.High, data.Low, data.Open, data.Close}));
                }
            }
            else if (new[]
            {
                CandleProjectionType.Grow,
                CandleProjectionType.RectangeGrow
            }.Contains(renderData.CandleProjectionType))
            {
                for (int index = 0; index < renderData.Candles.Count; index++)
                {
                    var data = renderData.Candles[index];

                    chart1.Series["BarsStub"].Points.Add(new DataPoint(data.Time.ToOADate(), 1000));

                    var values = data.Open < data.Close
                        ? new[] {data.High, data.Low, data.Low, data.High}
                        : new[] {data.High, data.Low, data.High, data.Low};

                    var color = "Gray";
                    if (renderData.CandleDirections[index] == CandleDirectionType.Buy)
                        color = "Blue";
                    if (renderData.CandleDirections[index] == CandleDirectionType.Sell)
                        color = "Red";

                    chart1.Series["Bars"].Points.Add(new DataPoint(data.Time.ToOADate(), values)
                    {
                        CustomProperties = string.Format("PriceDownColor={0}, PriceUpColor={0}", color)
                    });
                }
            }
            else if (renderData.CandleProjectionType == CandleProjectionType.Grow2Type || renderData.CandleProjectionType == CandleProjectionType.RestangeGrow2Type)
            {
                for (int index = 0; index < renderData.Candles.Count; index++)
                {
                    var data = renderData.Candles[index];

                    chart1.Series["BarsStub"].Points.Add(new DataPoint(data.Time.ToOADate(), 1000));

                    var values = data.Open < data.Close
                        ? new[] { data.High, data.Low, data.Low, data.High }
                        : new[] { data.High, data.Low, data.High, data.Low };

                    var color = "Gray";
                    if (renderData.CandleGrowTypes[index] == CandleGrowType.BuyUpperAvr)
                        color = "DarkBlue";
                    if (renderData.CandleGrowTypes[index] == CandleGrowType.BuyLowerAvr)
                        color = Color.DeepSkyBlue.Name;
                    if (renderData.CandleGrowTypes[index] == CandleGrowType.SellUpperAvr)
                        color = "DarkRed";
                    if (renderData.CandleGrowTypes[index] == CandleGrowType.SellLowerAvr)
                        color = Color.Orange.Name;

                    chart1.Series["Bars"].Points.Add(new DataPoint(data.Time.ToOADate(), values)
                    {
                        CustomProperties = string.Format("PriceDownColor={0}, PriceUpColor={0}", color)
                    });
                }
            }



            #endregion


            var minIndicatorArea = 0.0;
            var maxIndidcatorArea = 0.0;

            for (var index = 0; index < renderData.IndicatorsData.Count; index++)
            {
                var indicatorData = renderData.IndicatorsData[index];

                switch (renderData.IndicatorType)
                {
                    case RenderIndicatorType.Line:
                        chart1.Series["Buys"].Points
                            .Add(new DataPoint(renderData.Candles[index].Time.ToOADate(), indicatorData.Buy));
                        break;

                    case RenderIndicatorType.Line2:
                        chart1.Series["Buys"].Points
                            .Add(new DataPoint(renderData.Candles[index].Time.ToOADate(), indicatorData.Buy));
                        chart1.Series["Sells"].Points
                            .Add(new DataPoint(renderData.Candles[index].Time.ToOADate(), indicatorData.Sell));
                        break;

                    case RenderIndicatorType.Delta:

                        var dateItem = new DataPoint(renderData.Candles[index].Time.ToOADate(), indicatorData.Buy);
                        dateItem.Color = indicatorData.Buy < 0 ? Color.Red : Color.Blue;
                        chart1.Series["Delta"].Points.Add(dateItem);
                        break;

                    case RenderIndicatorType.Delta2:
                        chart1.Series["Delta"].Points
                            .Add(new DataPoint(renderData.Candles[index].Time.ToOADate(), indicatorData.Buy));
                        chart1.Series["Delta2"].Points
                            .Add(new DataPoint(renderData.Candles[index].Time.ToOADate(), indicatorData.Sell));
                        break;
                }
            }

            switch (renderData.IndicatorType)
            {
                case RenderIndicatorType.Line:
                    minIndicatorArea = renderData.IndicatorsData.Min(p => p.Buy);
                    maxIndidcatorArea = renderData.IndicatorsData.Max(p => p.Buy);
                    break;

                case RenderIndicatorType.Delta:
                    var averageBuys = renderData.IndicatorsData.Where(p => p.Buy > 0).Average(p => p.Buy);
                    var averageSells = renderData.IndicatorsData.Where(p => p.Buy < 0).Average(p => p.Buy);

                    var maxAverage = new[] {averageBuys, Math.Abs(averageSells)}.Max();

                    minIndicatorArea = -maxAverage * 1.5;
                    maxIndidcatorArea = maxAverage * 1.5;

                    break;

                case RenderIndicatorType.Line2:
                case RenderIndicatorType.Delta2:
                    minIndicatorArea = new[]
                            {renderData.IndicatorsData.Min(p => p.Buy), renderData.IndicatorsData.Min(p => p.Sell)}
                        .Min(p => p);
                    maxIndidcatorArea = new[]
                            {renderData.IndicatorsData.Max(p => p.Buy), renderData.IndicatorsData.Max(p => p.Sell)}
                        .Max(p => p);
                    break;
            }

            chart1.ChartAreas[1].AxisY.LabelStyle.Format = renderData.IndicatorDataFormat;


            var max = renderData.Candles.Max(p => p.High);
            var min = renderData.Candles.Min(p => p.Low);
            setMinMaxForArea(0, min, max);
            setMinMaxForArea(1, minIndicatorArea, maxIndidcatorArea);


            chart1.ChartAreas[1].AxisY.StripLines.Clear();
            if (renderData.IndicatorType == RenderIndicatorType.Line ||
                renderData.IndicatorType == RenderIndicatorType.Line2)
            {
                chart1.ChartAreas[1].AxisY.StripLines.Add(new StripLine
                {
                    IntervalOffset = 0,
                    BorderColor = Color.Green,
                    BorderDashStyle = ChartDashStyle.Dash,
                    BorderWidth = 2
                });
            }


            onCursorDataCatching(0);
        }

        public void LoadComboBoxes(SourceDataProjectionType[] comboBoxDataProjectionTypeDataSource, CandleProjectionType[] comboBoxCandleProjectionDataSource)
        {
            this.InvokeAsync(c =>
            {
                comboBoxDataProjectionType.DataSource =
                    comboBoxDataProjectionTypeDataSource.Select(
                        x => new KeyValuePair<SourceDataProjectionType, string>(x,
                            LocalicationAttributeExtensions.GetLocalizationEnum(x))).ToList();
                comboBoxDataProjectionType.ValueMember = "Key";
                comboBoxDataProjectionType.DisplayMember = "Value";
            
                comboBoxCandleProjection.DataSource =
                    comboBoxCandleProjectionDataSource.Select(
                        x => new KeyValuePair<CandleProjectionType, string>(x,
                            LocalicationAttributeExtensions.GetLocalizationEnum(x))).ToList();
                comboBoxCandleProjection.ValueMember = "Key";
                comboBoxCandleProjection.DisplayMember = "Value";
            });
        }

        public void Render(RenderData renderData)
        {
            this.InvokeAsync(c =>
            {
                RenderInner(renderData);
            });
        }

        private double calcRangeItem(RangeType type, double value, InstrumentStep step)
        {
            var resultValue = 0.0;

            var minNormalPips = Math.Pow(10, step.MajorDigits);
            var pips = step.Pips;

            int tmp = (int)Math.Round(value * minNormalPips);
            var t2 = ((int)(tmp / pips)) * pips;
            var t3 = t2 / minNormalPips;

            resultValue = t3;

            if (type == RangeType.Min)
            {
                if (resultValue > value)
                    resultValue -= pips / minNormalPips;
            }
            else
            {
                if (resultValue < value)
                    resultValue += pips / minNormalPips;
            }

            return resultValue;
        }


        private void setMinMaxForArea(int areaIndex, double min, double max)
        {
            var instrumentStep = InstrumentStepHelper.Get(LastrenderData.Instrument, LastrenderData.TimeFrame);

            var minSet = min == 0 ? double.NaN : calcRangeItem(RangeType.Min, min, instrumentStep);
            var maxSet = max == 0 ? double.NaN : calcRangeItem(RangeType.Max, max, instrumentStep);

            chart1.ChartAreas[areaIndex].AxisY.Minimum = minSet;
            chart1.ChartAreas[areaIndex].AxisY.Maximum = maxSet;
            chart1.ChartAreas[areaIndex].AxisY.Interval =
                1 / Math.Pow(10, instrumentStep.MajorDigits) * instrumentStep.Pips;
        }

        private void onCursorDataCatching(int pointIndex)
        {
            this.InvokeAsync(c =>
            {
                try
                {
                    chart1.ChartAreas[0].CursorX.Position = pointIndex + 1;

                    var candle = LastrenderData.Candles[pointIndex];
                    var format = LastrenderData.Spec.Format;

                    labelCurrentTime.Text = candle.Time.ToString();
                    labelCandleData.Text = string.Format("O:{0},C:{1},H:{2},L:{3}",
                        candle.Open.ToString(format),
                        candle.Close.ToString(format),
                        candle.High.ToString(format),
                        candle.Low.ToString(format));


                    var indicatorData = LastrenderData.IndicatorsData[pointIndex];

                    if (LastrenderData.IndicatorType == RenderIndicatorType.Delta2 ||
                        LastrenderData.IndicatorType == RenderIndicatorType.Line2)
                    {
                        labelIndicatorData.Text =
                            $@"{Localization.Buy}:{indicatorData.Buy.ToString(LastrenderData.IndicatorDataFormat)}, {Localization.Sell}: {indicatorData.Sell.ToString(LastrenderData.IndicatorDataFormat)}";
                    }
                    else
                    {
                        labelIndicatorData.Text = $@"{Localization.Value}:{indicatorData.Buy.ToString(LastrenderData.IndicatorDataFormat)}";
                    }

                }
                catch
                {
                }
            });
        }

        public void ApplyLocalization()
        {
            this.InvokeAsync(c =>
            {
                this.Text = $@"{InitData.Title} {InitData.Version} ({Localization.ValidUntil} {InitData.ExpireTime.ToString("dd.MM.yyyy")})";
                exitToolStripMenuItem.Text = Localization.Exit;
                SettingstoolStripMenuItem.Text = Localization.Settings;
                aboutToolStripMenuItem.Text = Localization.About;
                groupBoxCountData.Text = Localization.CountData;
                buttonCountDataAll.Text = Localization.All;
                groupBoxSourceData.Text = Localization.SourceData;
                radioButtonSourceDataLots.Text = Localization.Lots;
                radioButtonSourceDataPositions.Text = Localization.Positions;
                radioButtonIndicatorLongShort.Text = Localization.IndicatorLongShort;
                radioButtonIndicatorSumGrowDelta.Text = Localization.IndicatorGrowCumulativeDelta;
                radioButtonIndicatorGrowDelta.Text = Localization.IndicatorGrowDelta;
                radioButtonIndicatorOpenInterest.Text = Localization.IndicatorOpenInterest;
                groupBoxIndicators.Text = Localization.Indicators;
                buttonRefresh.Text = Localization.Refresh;
                groupBoxDataProjection.Text = Localization.DataProjection;
                groupBoxCandlesProjection.Text = Localization.CandlesProjection;
                labelDataProjAveragePeriod.Text = Localization.AveragePeriod;
                labelCandlesProjAveragePerio.Text = Localization.AveragePeriod;
                checkBoxIndicatorVisible.Text = Localization.IndicatorVisible;
            });
        }

        public void ShowTrialMessageExpired()
        {
            this.InvokeAsync(c =>
            {
                MessageBox.Show(this, Localization.TrialExpired);
                Application.Exit();
            });
        }

        public void UpdateCurrentMode(CurrentInstrumentView mode)
        {
            this.InvokeAsync(c =>
            {
                labelCurrentMode.Text = string.Format("{0} - {1} - {2}", mode.Instrument, mode.TimeFrame,
                    mode.CountCandles == null ? Localization.All : mode.CountCandles + " " + Localization.Candles);
            });
        }

        public void RenderError()
        {
            this.InvokeAsync(c =>
            {
                MessageBox.Show(this, Localization.ErrorConnection);
            });
        }

        private void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            this.InvokeAsync(c =>
            {
                MessageBox.Show(this, Localization.ErrorApplication);
            });
        }

        private void checkBoxIndicatorVisible_CheckedChanged(object sender, EventArgs e)
        {
            chart1.ChartAreas[1].Visible = checkBoxIndicatorVisible.Checked;
        }

        private void MfbToolsForm_SizeChanged(object sender, EventArgs e)
        {
            var panelChartCountersTop = this.Size.Height - 2 * panelChartCounters.Size.Height;
            var panelChartCountersWidth = this.Size.Width - panelChartCounters.Left;
            var chart1Size = new Size(this.Size.Width - chart1.Left,
                this.Size.Height - chart1.Top - 2 * panelChartCounters.Size.Height);

            if (panelChartCountersTop < 0 || panelChartCountersWidth < 0 || chart1Size.Height < 0 ||
                chart1Size.Width < 0)
                return;

            panelChartCounters.Top = panelChartCountersTop;
            panelChartCounters.Width = panelChartCountersWidth;
            chart1.Size= chart1Size;

        
        }

        public void LoadInitDatas(InitData initData)
        {
            InitData = initData;
        }
    }
}
