﻿using System;
using System.Windows.Forms;
using Antick.MfbTools.Domain;
using Antick.MfbTools.Domain.Enums;
using Antick.MfbTools.Extensions;
using Antick.MfbTools.Mvp.Args;
using Antick.MfbTools.Resources;

namespace Antick.MfbTools.Mvp.Views
{
    public partial class SettingsView : Form, ISettingsView
    {
        public event EventHandler<EventArgs> Loaded;
        public event EventHandler<LanguageChangingEventArgs> LanguageChanging;
        public event EventHandler<EventArgs> Applying;
        public event EventHandler<EventArgs> Saving;
        
        public SettingsView()
        {
            InitializeComponent();

            comboBoxLanguages.DataSource = Enum.GetValues(typeof(LanguageType));
        }

        public new void Show()
        {
            ShowDialog();
        }

        private LanguageType Language
        {
            get
            {
                try
                {
                    return (LanguageType)comboBoxLanguages.SelectedItem;
                }
                catch (Exception e)
                {
                    return LanguageType.English;
                }
            }
        }

        private void comboBoxLanguages_SelectedIndexChanged(object sender, EventArgs e)
        {
            LanguageChanging.Raise(this, new LanguageChangingEventArgs {LanguageType = Language});
        }

        private void SettingsView_FormClosed(object sender, FormClosedEventArgs e)
        {
        }

        private void SettingsView_Load(object sender, EventArgs e)
        {
            localize();
            Loaded.Raise(this, new EventArgs());
        }

        public void Render(Settings settings)
        {
            this.InvokeAsync(c =>
            {
                comboBoxLanguages.SelectedItem = settings.Language;
            });
        }

        public void ApplyChanges()
        {
            localize();
        }

        private void buttonApply_Click(object sender, EventArgs e)
        {
            Applying.Raise(this, new EventArgs());
        }

        private void Save_Click(object sender, EventArgs e)
        {
            Saving.Raise(this, new EventArgs());
        }

        private void localize()
        {
            this.InvokeAsync(c =>
            {
                buttonApply.Text = Localization.Apply;
                buttonSave.Text = Localization.Save;
                labelLanguage.Text = Localization.Language;
                Text = Localization.Settings;
            });
        }
    }
}
