﻿using System;
using Antick.MfbTools.Domain;
using Antick.MfbTools.Infractructure.Mvp.Views;
using Antick.MfbTools.Mvp.Args;

namespace Antick.MfbTools.Mvp.Views
{
    public interface ISettingsView : IView
    {
        event EventHandler<EventArgs> Loaded; 

        event EventHandler<LanguageChangingEventArgs> LanguageChanging;

        event EventHandler<EventArgs> Applying;
        event EventHandler<EventArgs> Saving;

        void Render(Settings settings);

        void ApplyChanges();
    }
}
