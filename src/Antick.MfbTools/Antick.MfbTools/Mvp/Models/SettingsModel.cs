﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.MfbTools.Domain;
using Antick.MfbTools.Domain.Enums;
using Antick.MfbTools.Infractructure.Mvp.Models;

namespace Antick.MfbTools.Mvp.Models
{
    public class SettingsModel : Model
    {
        public Settings Settings { get; set; }
    }
}
