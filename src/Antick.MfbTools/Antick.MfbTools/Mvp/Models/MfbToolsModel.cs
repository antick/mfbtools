﻿using System;
using System.Collections.Generic;
using System.Linq;
using Antick.Contracts;
using Antick.Contracts.Santiment;
using Antick.MfbTools.Components;
using Antick.MfbTools.Components.Data;
using Antick.MfbTools.Domain;
using Antick.MfbTools.Domain.Data;
using Antick.MfbTools.Domain.Enums;
using Antick.MfbTools.Domain.Render;
using Antick.MfbTools.Infractructure.Mvp.Models;
using Antick.MfbTools.Resources;

namespace Antick.MfbTools.Mvp.Models
{
    public class MfbToolsModel : Model
    {
        private readonly IMfbDataAlg m_MfbDataAlg;
        private readonly IMfbDataProvider m_MfbDataProvider;
        private readonly AppConfig m_AppConfig;
        public Instrument Instrument { get; set; }
        public TimeFrame TimeFrame { get; set; }
        public SourceData SourceData { get; set; }
        public CountData CountData { get; set; }
        public IndicatorType Indicator { get; set; }

        public SourceDataProjectionType ProjectionType { get; set; }
        public int ProjectionValue { get; set; }

        public CandleProjectionType CandleProjectionType { get; set; }
        public int CandleProjectionValue { get; set; }

        /// <summary>
        /// Расчитанные данные сантимента за последний период
        /// </summary>
        public List<ViewData> ViewDataLots { get; set; }

        public List<ViewData> ViewDataPositions { get; set; }

        public List<MfbDataItem> DownlaodDataLots { get; set; }
        public List<MfbDataItem> DownlaodDataPositions { get; set; }


        public SourceDataProjectionType[] AvalibleDataProjectionTypes =
        {
            SourceDataProjectionType.None, SourceDataProjectionType.Average
        };

        public CandleProjectionType[] AvalibleCandleProjectionTypes = (CandleProjectionType[])Enum.GetValues(typeof(CandleProjectionType));

        public MfbToolsModel(IMfbDataAlg mfbDataAlg, IMfbDataProvider mfbDataProvider, AppConfig appConfig)
        {
            m_MfbDataAlg = mfbDataAlg;
            m_MfbDataProvider = mfbDataProvider;
            m_AppConfig = appConfig;

            ViewDataLots = new List<ViewData>();
            ViewDataPositions = new List<ViewData>();
            Instrument = Instrument.EUR_USD;
            TimeFrame = TimeFrame.H1;
            SourceData = SourceData.Lots;
            CountData = CountData.C100;
            Indicator = IndicatorType.LongShort;
            ProjectionType = SourceDataProjectionType.None;
        }

        public void DownloadData()
        {
            DownlaodDataLots = m_MfbDataProvider.Do(Instrument, TimeFrame, SourceData.Lots);
            DownlaodDataPositions = m_MfbDataProvider.Do(Instrument, TimeFrame, SourceData.Positions);

            ViewDataLots = m_MfbDataAlg.Do(DownlaodDataLots,Instrument, TimeFrame, SourceData.Lots,
                ProjectionType, ProjectionValue);

            ViewDataPositions = m_MfbDataAlg.Do(DownlaodDataPositions, Instrument, TimeFrame, SourceData.Positions,
                ProjectionType, ProjectionValue);
        }

        public void UpdateViewData()
        {
            ViewDataLots = m_MfbDataAlg.Do(DownlaodDataLots, Instrument, TimeFrame, SourceData.Lots,
                ProjectionType, ProjectionValue);

            ViewDataPositions = m_MfbDataAlg.Do(DownlaodDataPositions, Instrument, TimeFrame, SourceData.Positions,
                ProjectionType, ProjectionValue);
        }


        public RenderData GenerateRenderData()
        {
            RenderData renderData = new RenderData
            {
                Candles = new List<RenderCandle>(),
                IndicatorsData = new List<RenderIndicator>(),
                Spec = Utils.InstrumentSpec.Get(Instrument.ToString()),
                CandleDirections = new List<CandleDirectionType>(),
                CandleGrowTypes = new List<CandleGrowType>(),
                Instrument = Instrument.ToString(),
                TimeFrame = TimeFrame.ToString()
            };

            List<ViewData> data = SourceData == SourceData.Lots ? ViewDataLots : ViewDataPositions;
            
            var count = countDataToInt();

            data = data.ToArray().OrderByDescending(p => p.Candle.Time).Take(count).OrderBy(p => p.Candle.Time).ToList();
            
            foreach (var viewData in data)
            {
                renderData.Candles.Add(new RenderCandle
                {
                    Time = viewData.Candle.Time.ToLocalTime(),
                    Open = viewData.Candle.Open,
                    Close = viewData.Candle.Close,
                    High = viewData.Candle.High,
                    Low = viewData.Candle.Low
                });
            }

            mapIndicatorData(renderData, data);
            mapCandleExtData(renderData, count);

            return renderData;
        }

        public InitData GetInitData()
        {
            return new InitData()
            {
                ExpireTime = m_AppConfig.ExpirationTime,
                Title = "MfbTools",
                Version = m_AppConfig.Version
            };
        }

        private void mapCandleExtData(RenderData renderData, int count)
        {
            List<ViewData> dataForCandleExt = new List<ViewData>();


            List<MfbDataItem> downloaddat = SourceData == SourceData.Lots
                ? DownlaodDataLots
                : DownlaodDataPositions;

            dataForCandleExt = m_MfbDataAlg.Do(downloaddat, Instrument, TimeFrame, SourceData,
                SourceDataProjectionType.Average,
                CandleProjectionValue);

            if (CountData != CountData.All)
                dataForCandleExt = dataForCandleExt.ToArray().OrderByDescending(p => p.Candle.Time).Take(count)
                    .OrderBy(p => p.Candle.Time).ToList();


            renderData.CandleProjectionType = CandleProjectionType;
            if (CandleProjectionType != CandleProjectionType.None)
            {
               if (CandleProjectionType == CandleProjectionType.RectangeGrow)
                {
                    var prevCandle = renderData.Candles[0];
                    var prevType = CandleDirectionType.None;
                    renderData.CandleDirections.Add(prevType);
                    var p = 0;
                    for (var i = 1; i < dataForCandleExt.Count; i++)
                    {
                        var viewData = dataForCandleExt[i];

                        CandleDirectionType type = CandleDirectionType.None;
                        if (viewData.Mfb.DeltaOpened > 0)
                            type = CandleDirectionType.Buy;
                        else if (viewData.Mfb.DeltaOpened < 0)
                            type = CandleDirectionType.Sell;

                        if (type == prevType)
                        {
                            var low = new[] { prevCandle.Low, renderData.Candles[i].Low }.Min();
                            var high = new[] { prevCandle.High, renderData.Candles[i].High }.Max();
                            for (int k = p; k <= i; k++)
                            {
                                renderData.Candles[k].Low = low;
                                renderData.Candles[k].High = high;
                            }

                        }
                        else
                        {
                            p = i;
                        }

                        prevCandle = renderData.Candles[i];
                        prevType = type;

                        renderData.CandleDirections.Add(type);
                    }
                }
                else if (CandleProjectionType == CandleProjectionType.Grow)
                {
                    foreach (var viewData in dataForCandleExt)
                    {
                        CandleDirectionType type = CandleDirectionType.None;
                        if (viewData.Mfb.DeltaOpened > 0)
                            type = CandleDirectionType.Buy;
                        else if (viewData.Mfb.DeltaOpened < 0)
                            type = CandleDirectionType.Sell;

                        renderData.CandleDirections.Add(type);
                    }
                }

                else if (CandleProjectionType == CandleProjectionType.Grow2Type)
                {
                    var avrSells = dataForCandleExt.Where(p => p.Mfb.DeltaOpened < 0).Average(p => p.Mfb.DeltaOpened);
                    var avrBuys = dataForCandleExt.Where(p => p.Mfb.DeltaOpened > 0).Average(p => p.Mfb.DeltaOpened);
                    var avr = new[] {Math.Abs(avrSells), avrBuys}.Max();
                    foreach (var viewData in dataForCandleExt)
                    {
                        CandleGrowType type = CandleGrowType.None;
                        if (viewData.Mfb.DeltaOpened > 0)
                        {
                            type = viewData.Mfb.DeltaOpened > avr
                                ? CandleGrowType.BuyUpperAvr
                                : CandleGrowType.BuyLowerAvr;
                        }
                        else if (viewData.Mfb.DeltaOpened < 0)
                        {
                            type = Math.Abs(viewData.Mfb.DeltaOpened) > avr
                                ? CandleGrowType.SellUpperAvr
                                : CandleGrowType.SellLowerAvr;
                        }
                        renderData.CandleGrowTypes.Add(type);
                    }
                }
                else if (CandleProjectionType == CandleProjectionType.RestangeGrow2Type)
               {
                   var avrSells = dataForCandleExt.Where(x => x.Mfb.DeltaOpened < 0).Average(x => x.Mfb.DeltaOpened);
                   var avrBuys = dataForCandleExt.Where(x => x.Mfb.DeltaOpened > 0).Average(x => x.Mfb.DeltaOpened);
                   var avr = new[] { Math.Abs(avrSells), avrBuys }.Max();

                   var prevType = CandleGrowType.None;
                   var p = 0;
                   var prevCandle = renderData.Candles[0];

                    for (var i = 0; i < dataForCandleExt.Count; i++)
                   {
                       var viewData = dataForCandleExt[i];

                       CandleGrowType type = CandleGrowType.None;
                       if (viewData.Mfb.DeltaOpened > 0)
                       {
                           type = viewData.Mfb.DeltaOpened > avr
                               ? CandleGrowType.BuyUpperAvr
                               : CandleGrowType.BuyLowerAvr;
                       }
                       else if (viewData.Mfb.DeltaOpened < 0)
                       {
                           type = Math.Abs(viewData.Mfb.DeltaOpened) > avr
                               ? CandleGrowType.SellUpperAvr
                               : CandleGrowType.SellLowerAvr;
                       }

                       if (type == prevType || prevType == CandleGrowType.None)
                       {
                           var low = new[] { prevCandle.Low, renderData.Candles[i].Low }.Min();
                           var high = new[] { prevCandle.High, renderData.Candles[i].High }.Max();
                           for (int k = p; k <= i; k++)
                           {
                               renderData.Candles[k].Low = low;
                               renderData.Candles[k].High = high;
                           }
                       }
                       else
                       {
                           p = i;
                       }

                       prevCandle = renderData.Candles[i];
                       prevType = type;

                        renderData.CandleGrowTypes.Add(type);
                   }
               }
            }
        }

        private void mapIndicatorData(RenderData renderData, List<ViewData> data)
        {
            switch (Indicator)
            {
                case IndicatorType.GrowDelta:
                    foreach (var viewData in data)
                    {
                        renderData.IndicatorsData.Add(new RenderIndicator { Buy = viewData.Mfb.DeltaOpened });
                    }
                    renderData.IndicatorType = RenderIndicatorType.Delta;
                    break;

                case IndicatorType.SumGrowDelta:
                    double sum = 0;

                    foreach (var viewData in data)
                    {
                        sum += viewData.Mfb.DeltaOpened;
                        renderData.IndicatorsData.Add(new RenderIndicator { Buy = sum });
                    }
                    renderData.IndicatorType = RenderIndicatorType.Line;
                    break;

                case IndicatorType.LongShort:
                    foreach (var viewData in data)
                    {
                        renderData.IndicatorsData.Add(
                            new RenderIndicator { Buy = viewData.Mfb.Long, Sell = viewData.Mfb.Short });
                    }
                    renderData.IndicatorType = RenderIndicatorType.Line2;
                    break;

                case IndicatorType.OpenInterest:
                    foreach (var viewData in data)
                    {
                        renderData.IndicatorsData.Add(
                            new RenderIndicator { Buy = viewData.Mfb.Oi });
                    }
                    renderData.IndicatorType = RenderIndicatorType.Line;
                    break;

                case IndicatorType.LongDeltaShortDelta:
                    foreach (var viewData in data)
                    {
                        renderData.IndicatorsData.Add(
                            new RenderIndicator { Buy = viewData.Mfb.LongDelta, Sell = viewData.Mfb.ShortDelta });
                    }
                    renderData.IndicatorType = RenderIndicatorType.Line2;
                    break;
            }

            renderData.IndicatorDataFormat = SourceData == SourceData.Lots ? "0.00" : "0";
        }

        public CurrentInstrumentView GenerateMode()
        {
            int? count = null;
            if (CountData != CountData.All)
            {
                var tmp = CountData.ToString().Remove(0, 1);
                count = Convert.ToInt32(tmp);
            }
            return new CurrentInstrumentView {Instrument = Instrument, TimeFrame = TimeFrame, CountCandles = count};
        }

        private int countDataToInt()
        {
            var count = 0;
            switch (CountData)
            {
                case CountData.C100:
                    count = 100;
                    break;
                case CountData.C200:
                    count = 200;
                    break;
                case CountData.C500:
                    count = 500;
                    break;
                case CountData.All:
                    count = int.MaxValue;
                    break;
            }

            return count;
        }
    }
}

