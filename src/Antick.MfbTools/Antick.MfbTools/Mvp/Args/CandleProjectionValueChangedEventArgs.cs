﻿using System;

namespace Antick.MfbTools.Mvp.Args
{
    public class CandleProjectionValueChangedEventArgs : EventArgs
    {
        public int Value { get; set; }
    }
}
