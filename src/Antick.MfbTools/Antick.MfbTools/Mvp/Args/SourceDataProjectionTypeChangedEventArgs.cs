﻿using Antick.MfbTools.Domain.Enums;

namespace Antick.MfbTools.Mvp.Args
{
    public class SourceDataProjectionTypeChangedEventArgs : System.EventArgs
    {
        public SourceDataProjectionType ProjectionType { get; set; }
    }
}
