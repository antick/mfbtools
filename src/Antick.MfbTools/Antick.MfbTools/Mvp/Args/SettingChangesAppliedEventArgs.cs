﻿using System;
using Antick.MfbTools.Domain;

namespace Antick.MfbTools.Mvp.Args
{
    public  class SettingChangesAppliedEventArgs : EventArgs
    {
        public Settings Settings { get; set; }
    }
}
