﻿using Antick.MfbTools.Domain.Enums;

namespace Antick.MfbTools.Mvp.Args
{
    public class SourceDataChangingEventArgs : System.EventArgs
    {
        public SourceData SourceData { get; set; }
    }
}
