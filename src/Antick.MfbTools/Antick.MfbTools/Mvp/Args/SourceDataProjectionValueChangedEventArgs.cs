﻿namespace Antick.MfbTools.Mvp.Args
{
    public class SourceDataProjectionValueChangedEventArgs : System.EventArgs
    {
        public int ProjectionValue { get; set; }
    }
}
