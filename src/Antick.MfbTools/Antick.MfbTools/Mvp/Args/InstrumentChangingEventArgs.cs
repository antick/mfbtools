﻿using Antick.Contracts;

namespace Antick.MfbTools.Mvp.Args
{
    public class InstrumentChangingEventArgs : System.EventArgs
    {
        public Instrument Instrument { get; set; }
    }
}
