﻿using Antick.MfbTools.Domain.Enums;

namespace Antick.MfbTools.Mvp.Args
{
    public class IndicatorChangingEventArgs : System.EventArgs
    {
        public IndicatorType Indicator { get; set; }
    }
}
