﻿using System;
using Antick.MfbTools.Domain.Enums;

namespace Antick.MfbTools.Mvp.Args
{
    public class LanguageChangingEventArgs : EventArgs
    {
        public LanguageType LanguageType { get; set; }
    }
}
