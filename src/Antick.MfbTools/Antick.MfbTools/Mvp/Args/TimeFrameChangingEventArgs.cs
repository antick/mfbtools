﻿using Antick.Contracts;

namespace Antick.MfbTools.Mvp.Args
{
    public class TimeFrameChangingEventArgs : System.EventArgs
    {
        public TimeFrame TimeFrame { get; set; }
    }
}
