﻿using System;
using Antick.MfbTools.Domain.Enums;

namespace Antick.MfbTools.Mvp.Args
{
    public class CandleProjectionTypeChangingEventArgs : EventArgs
    {
        public CandleProjectionType Type { get; set; }
    }
}
