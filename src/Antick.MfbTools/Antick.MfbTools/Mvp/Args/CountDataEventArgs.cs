﻿using Antick.MfbTools.Domain.Enums;

namespace Antick.MfbTools.Mvp.Args
{
    public class CountDataEventArgs : System.EventArgs
    {
        public CountData CountData { get; set; }
    }
}
