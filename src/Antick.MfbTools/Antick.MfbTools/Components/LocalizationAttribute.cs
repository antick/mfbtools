﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Antick.MfbTools.Components
{
    [AttributeUsage(AttributeTargets.All)]
    public class LocalizationAttribute : Attribute
    {
        public string RussiaDesc { get; set; }
        public string EnglishDesc { get; set; }
        public LocalizationAttribute(string russiaDesc, string englishDesc)
        {
            RussiaDesc = russiaDesc;
            EnglishDesc = englishDesc;
        }
    }
}
