﻿using System.IO;
using Newtonsoft.Json;

namespace Antick.MfbTools.Components.Settings
{
    public class SettingsService : ISettingsService
    {
        private string path = "settings.json";

        public Domain.Settings Get()
        {
            if (File.Exists(path))
            {
                try
                {
                    // deserialize JSON directly from a file
                    using (StreamReader file = File.OpenText(path))
                    {
                        JsonSerializer serializer = new JsonSerializer();
                        Domain.Settings settings = (Domain.Settings) serializer.Deserialize(file, typeof(Domain.Settings));
                        return settings;
                    }
                }
                catch
                {
                    return new Domain.Settings();
                }
            }
            return new Domain.Settings();
        }

        public void Save(Domain.Settings settings)
        {
            // serialize JSON directly to a file
            using (StreamWriter file = File.CreateText(path))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(file, settings);
            }
        }
    }
}
