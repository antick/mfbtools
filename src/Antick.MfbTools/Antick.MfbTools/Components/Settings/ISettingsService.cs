﻿namespace Antick.MfbTools.Components.Settings
{
    public interface ISettingsService
    {
        Domain.Settings Get();
        void Save(Domain.Settings settings);
    }
}
