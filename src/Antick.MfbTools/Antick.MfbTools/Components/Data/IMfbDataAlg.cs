﻿using System.Collections.Generic;
using Antick.Contracts;
using Antick.MfbTools.Domain;
using Antick.MfbTools.Domain.Data;
using Antick.MfbTools.Domain.Enums;

namespace Antick.MfbTools.Components.Data
{
    public interface IMfbDataAlg
    {
        List<ViewData> Do(Instrument instrument, TimeFrame tf, SourceData sourceData, SourceDataProjectionType dataProjectionType, int dataProjectionValue);

        List<ViewData> Do(List<MfbDataItem> downloaddata, Instrument instrument, TimeFrame tf, SourceData sourceData,
            SourceDataProjectionType dataProjectionType, int dataProjectionValue);
    }
}
