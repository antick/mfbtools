﻿using System;
using System.Collections.Generic;
using System.Linq;
using Antick.Contracts;
using Antick.MfbTools.Domain.Data;
using Antick.MfbTools.Domain.Enums;

namespace Antick.MfbTools.Components.Data
{
    public class  MfbDataFormatter : IMfbDataFormatter
    {
        public List<MfbDataItem> Do(List<MfbDataItem> inputData, Instrument instrument, TimeFrame tf, SourceData sourceData, SourceDataProjectionType dataProjectionType, int dataProjectionValue)
        {
            var data = DetachNulls(inputData);

            if (dataProjectionType != SourceDataProjectionType.None && dataProjectionValue > 0)
            {
                var extData = GenerateExtData(data);

                var extdatasNoExtremums = Average(extData, dataProjectionType, dataProjectionValue);

                return extdatasNoExtremums
                    .Select(p => new MfbDataItem {Candle = p.Candle, Long = p.Long, Short = p.Short})
                    .ToList();
            }
            else
            {
                return data;
            }
        }

        public List<MfbDataItem> DetachNulls(List<MfbDataItem> inputData)
        {
            var data = inputData.ToArray().ToList();

            var prevLong = data[0].Long;
            var prevShort = data[0].Short;

            for (var i = 1; i < data.Count; i++)
            {
                if (data[i].Long == 0 || data[i].Short == 0)
                {
                    data[i].Long = prevLong;
                    data[i].Short = prevShort;
                }

                prevLong = data[i].Long;
                prevShort = data[i].Short;
            }
            return data;
        }

        public List<MfbDataItemExt> GenerateExtData(List<MfbDataItem> inputData)
        {
            var data = new List<MfbDataItemExt>
            {
                new MfbDataItemExt
                {
                    Candle = inputData[0].Candle,
                    Long = inputData[0].Long,
                    Short = inputData[0].Short
                }
            };
            
            for (var i = 1; i < inputData.Count; i++)
            {
                data.Add(new MfbDataItemExt
                {
                    Candle = inputData[i].Candle,
                    Long = inputData[i].Long,
                    Short = inputData[i].Short,
                    LongDelta = AbsDeltaPercent(inputData[i].Long, inputData[i-1].Long),
                    ShortDelta = AbsDeltaPercent(inputData[i].Short, inputData[i - 1].Short),
                });
            }

            return data;
        }

        public List<MfbDataItemExt> DetachExtremums(List<MfbDataItemExt> datas, double longLim, double shortLim)
        {
            var prevLong = datas[0].Long;
            var prevShort = datas[0].Short;
            var prevLongDelta = 0.0;
            var prevShortDelta = 0.0;
            var alg = false;
            for (int i = 1; i < datas.Count; i++)
            {

                if (alg)
                {
                    datas[i].LongDelta = AbsDeltaPercent(datas[i].Long, prevLong);
                    datas[i].ShortDelta = AbsDeltaPercent(datas[i].Short, prevShort);

                    // выход из впадины
                    if (datas[i].LongDelta <= longLim && datas[i].ShortDelta <= shortLim)
                    {
                        alg = false;
                        prevLongDelta = datas[i].LongDelta;
                        prevShortDelta = datas[i].ShortDelta;
                    }
                    else
                    {
                        datas[i].Long = prevLong;
                        datas[i].Short = prevShort;
                        continue;
                    }
                }

                if (prevLongDelta >= longLim || prevShortDelta >= shortLim)
                {
                    datas[i].Long = prevLong;
                    datas[i].Short = prevShort;
                    alg = true;
                    continue;
                }

                prevLong = datas[i].Long;
                prevShort = datas[i].Short;
                prevLongDelta = datas[i].LongDelta;
                prevShortDelta = datas[i].ShortDelta;
            }

            return datas;
        }

        public List<MfbDataItemExt> Average(List<MfbDataItemExt> datas, SourceDataProjectionType projectionType, int averagePeriod)
        {
            Func<int, string, double , double> average = (index, type, period) =>
            {
                var sum = 0.0;
                for (int k = index; k > index - averagePeriod && k > 0; k--)
                {
                    sum += type == "long" ? datas[k].Long : datas[k].Short;
                }

                return sum / averagePeriod;
            };

            if (projectionType == SourceDataProjectionType.AverageSquare)
            {
                for (int i = 0; i < datas.Count; i++)
                {
                    datas[i].Long = average(i, "long", averagePeriod);
                    datas[i].Short = average(i, "short", averagePeriod);
                }
                return datas;
            }
            else
            {
                List<MfbDataItemExt> datasResult = new List<MfbDataItemExt>();
                for (int i = 0; i < datas.Count; i++)
                {
                    datasResult.Add(new MfbDataItemExt
                    {
                        Candle = datas[i].Candle,
                        Long = average(i, "long", averagePeriod),
                        Short = average(i, "short", averagePeriod)
                    });
                }
                return datasResult;
            }
        }

        private double AbsDeltaPercent(double cur, double prev)
        {
            return Math.Abs((cur - prev) / Math.Max(cur, prev) * 100);
        }
    }
}
