﻿using System.Collections.Generic;
using Antick.Contracts;
using Antick.MfbTools.Domain.Data;
using Antick.MfbTools.Domain.Enums;

namespace Antick.MfbTools.Components.Data
{
    public interface IMfbDataProvider
    {
        List<MfbDataItem> Do(Instrument instrument, TimeFrame tf, SourceData sourceData);
    }
}
