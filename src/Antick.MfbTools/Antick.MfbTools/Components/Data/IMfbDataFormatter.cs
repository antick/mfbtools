﻿using System.Collections.Generic;
using Antick.Contracts;
using Antick.MfbTools.Domain.Data;
using Antick.MfbTools.Domain.Enums;

namespace Antick.MfbTools.Components.Data
{
    public interface IMfbDataFormatter
    {
        List<MfbDataItem> Do(List<MfbDataItem> inputData, Instrument instrument, TimeFrame tf, SourceData sourceData, SourceDataProjectionType dataProjectionType, int dataProjectionValue);
    }
}
