﻿using System;
using System.Collections.Generic;
using Antick.Contracts;
using Antick.MfbTools.Domain;
using Antick.MfbTools.Domain.Data;
using Antick.MfbTools.Domain.Enums;
using Antick.MfbTools.Legacy.Types;

namespace Antick.MfbTools.Components.Data
{
    public class MfbDataAlg : IMfbDataAlg
    {
        private readonly IMfbDataProvider m_DataProvider;
        private readonly IMfbDataFormatter m_DataFormatter;

        public MfbDataAlg(IMfbDataProvider dataProvider, IMfbDataFormatter dataFormatter)
        {
            m_DataProvider = dataProvider;
            m_DataFormatter = dataFormatter;
        }

        public List<ViewData> Do(Instrument instrument, TimeFrame tf, SourceData sourceData,SourceDataProjectionType dataProjectionType, int dataProjectionValue)
        {
            var downloadData = m_DataProvider.Do(instrument, tf, sourceData);
            var formatData = m_DataFormatter.Do(downloadData, instrument, tf, sourceData, dataProjectionType, dataProjectionValue);

            return alg(formatData, tf);
        }

        public List<ViewData> Do(List<MfbDataItem> downloaddata, Instrument instrument, TimeFrame tf, SourceData sourceData,
            SourceDataProjectionType dataProjectionType, int dataProjectionValue)
        {
            var formatData = m_DataFormatter.Do(downloaddata, instrument, tf, sourceData, dataProjectionType, dataProjectionValue);

            return alg(formatData, tf);
        }

        private List<ViewData> alg(List<MfbDataItem> datasInput, TimeFrame tf)
        {
            var tfMin = parceTfMin(tf);

            var datas = new List<ViewData>();

            datas.Add(new ViewData
            {
                Candle = datasInput[0].Candle,
                Mfb = new MfbGroup {Long = datasInput[0].Long, Short = datasInput[0].Short, Oi = datasInput[0].Long  + datasInput[0].Short }
            });


            for (int i = 1; i < datasInput.Count; i++)
            {
                var candle = datasInput[i].Candle;
                
                datas.Add(new ViewData
                {
                    Candle = candle
                });

                var santimentStart = datasInput[i - 1];
                var santimentEnd = datasInput[i];

                CalcSimple(datas[i], santimentStart, santimentEnd);


                datas[i].Mfb.Long = santimentEnd.Long;
                datas[i].Mfb.Short = santimentEnd.Short;
                datas[i].Mfb.Oi = datas[i].Mfb.Long + datas[i].Mfb.Short;

                if (datas[i].Mfb.Oi > 0 && datas[i - 1].Mfb.Oi > 0)
                    datas[i].Mfb.OiDelta = datas[i].Mfb.Oi - datas[i - 1].Mfb.Oi;

                datas[i].Mfb.LongDelta = Math.Abs((datas[i].Mfb.Long - datas[i - 1].Mfb.Long) / Math.Max(
                                                      datas[i].Mfb.Long,
                                                      datas[i - 1].Mfb
                                                          .Long) * 100);

                datas[i].Mfb.ShortDelta = Math.Abs((datas[i].Mfb.Short - datas[i - 1].Mfb.Short) / Math.Max(
                                                       datas[i].Mfb.Short,
                                                       datas[i - 1].Mfb
                                                           .Short) * 100);
            }

            return datas;
        }

        private void CalcSimple(ViewData item, MfbDataItem santimentStart, MfbDataItem santimentEnd)
        {
            var deltaLong = santimentEnd.Long - santimentStart.Long;
            var deltaLongGrow = deltaLong > 0 ? deltaLong : 0;
            var deltaLongClose = deltaLong < 0 ? -deltaLong : 0;

            var deltaShort = santimentEnd.Short - santimentStart.Short;
            var deltaShortGrow = deltaShort > 0 ? deltaShort : 0;
            var deltaShortClose = deltaShort < 0 ? -deltaShort : 0;

            item.Mfb.DeltaOpened = deltaLongGrow - deltaShortGrow;

            item.Mfb.LongClosed = deltaLongClose;
            item.Mfb.ShortClosed = deltaShortClose;
            item.Mfb.LongOpened = deltaLongGrow;
            item.Mfb.ShortOpened = deltaShortGrow;
            item.Mfb.DeltaClosed = deltaLongClose - deltaShortClose;
        }

        private int parceTfMin(TimeFrame tf)
        {
            switch (tf)
            {
                case TimeFrame.M1:
                    return 1;
                case TimeFrame.M2:
                    return 2;
                case TimeFrame.M3:
                    return 3;
                case TimeFrame.M4:
                    return 4;
                case TimeFrame.M5:
                    return 5;
                case TimeFrame.M10:
                    return 10;
                case TimeFrame.M15:
                    return 15;
                case TimeFrame.M20:
                    return 20;
                case TimeFrame.M30:
                    return 30;
                case TimeFrame.H1:
                    return 60;
                case TimeFrame.H4:
                    return 240;
                case TimeFrame.D1:
                    return 1440;
            }

            return 1;
        }
    }
}
