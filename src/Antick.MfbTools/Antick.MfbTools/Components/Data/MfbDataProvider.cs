﻿using System.Collections.Generic;
using System.Linq;
using Antick.Contracts;
using Antick.MfbApi.OutlookDownloader;
using Antick.MfbApi.OutlookDownloader.Contracts;
using Antick.MfbTools.Domain.Data;
using Antick.MfbTools.Domain.Enums;

namespace Antick.MfbTools.Components.Data
{
    public class MfbDataProvider  : IMfbDataProvider
    {
        private readonly IOutlookDownloader m_OutlookDownloader;


        public MfbDataProvider(IOutlookDownloader outlookDownloader)
        {
            m_OutlookDownloader = outlookDownloader;
        }

        public List<MfbDataItem> Do(Instrument instrument, TimeFrame tf, SourceData sourceData)
        {
            var sourceType = sourceData == SourceData.Positions ? OutlookSourceType.Positions : OutlookSourceType.Lots;

            var santimentData = m_OutlookDownloader.Download(instrument, tf, sourceType);

            var mfbStamps = santimentData.Where(p => p.Quote != null).Select(p => new MfbDataItem
            {
                Candle = new Candle
                {
                    Time = p.Quote.Time,
                    Open = p.Quote.Open,
                    Close = p.Quote.Close,
                    High = p.Quote.High,
                    Low = p.Quote.Low,
                    Complete = true,
                    Volume = 0
                },
                Long = p.Long,
                Short = p.Short
            }).ToList();

            return mfbStamps;
        }
    }
}
