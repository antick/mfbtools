﻿namespace Antick.MfbTools.Utils
{
    public class InstrumentStep
    {
        public int MajorDigits { get; set; }

        public double Pips { get; set; }
    }
}
