﻿using Antick.MfbTools.Components;

namespace Antick.MfbTools.Utils
{
    public class InstrumentStepHelper
    {
        public static InstrumentStep Get(string instrument, string timeframe)
        {
            var baseSpec = InstrumentSpec.Get(instrument);

            double mult = 1;

            if (timeframe == "H4")
                mult = 2;
            if (timeframe == "D1")
                mult = 4;
            if (timeframe == "M15")
                mult = 0.5;
            if (timeframe == "M5")
                mult = 0.25;

            return new InstrumentStep {MajorDigits = baseSpec.Digits, Pips = 50 * mult};
        }
    }
}
