﻿using System;
using System.Windows.Forms;
using Antick.MfbApi.OutlookDownloader;
using Antick.MfbTools.Components;
using Antick.MfbTools.Components.Data;
using Antick.MfbTools.Components.Settings;
using Antick.MfbTools.Infractructure.Mvp.Models;
using Antick.MfbTools.Mvp.Models;
using Antick.MfbTools.Mvp.Presenters;
using Antick.MfbTools.Mvp.Views;
using Castle.Facilities.Logging;
using Castle.Facilities.TypedFactory;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace Antick.MfbTools
{
    public class WindsorInstaller  : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.AddFacility<LoggingFacility>();
            container.AddFacility<TypedFactoryFacility>();

            // MfbToolsView
            container.Register(Component.For<ApplicationContext>());
            container.Register(Component.For<IMfbToolsView>().ImplementedBy<MfbToolsForm>());
            container.Register(Component.For<MfbToolsPresenter>());
            container.Register(Component.For<MfbToolsModel>());

            // Setting View
            container.Register(Component.For<ISettingsView>().ImplementedBy<SettingsView>().LifestyleTransient());
            container.Register(Component.For<SettingsPresenter>().LifestyleTransient());
            container.Register(Component.For<SettingsRootPresenter>());
            container.Register(Component.For<ISettingsPresenterFactory>().AsFactory());
            container.Register(Component.For<SettingsModel>().LifestyleTransient());

            container.Register(Component.For<IAboutView>().ImplementedBy<AboutForm>().LifestyleTransient());
            container.Register(Component.For<AboutPresenter>().LifestyleTransient());
            container.Register(Component.For<AboutModel>().LifestyleTransient());
            container.Register(Component.For<IAboutPresenterFactory>().AsFactory());

            container.Register(Component.For<IOutlookDownloader>().ImplementedBy<OutlookDownloader>());
            container.Register(Component.For<IMfbDataProvider>().ImplementedBy<MfbDataProvider>());
            container.Register(Component.For<IMfbDataFormatter>().ImplementedBy<MfbDataFormatter>());
            container.Register(Component.For<IMfbDataAlg>().ImplementedBy<MfbDataAlg>());


            container.Register(Component.For<ISettingsService>().ImplementedBy<SettingsService>());

            container.Register(Component.For<AppConfig>().Instance(new AppConfig
                {Version = "1.0.4.0", ExpirationTime = new DateTime(2100, 1, 1)}));
        }
    }
}
