﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using Antick.Contracts;
using Antick.Lab.Kernel.DataAccess.Workers;
using Antick.Lab.Kernel.Domain.Enums;
using Antick.Lab.Kernel.Infrastructure;
using Antick.Lab.Kernel.Legacy.Logic.Terminal.Logic;
using Antick.Lab.Kernel.Legacy.Logic.Terminal.SubTools;
using Antick.Lab.Kernel.Legacy.Logic.Terminal.Types;
using Antick.Lab.Kernel.Legacy.Ext;
using Antick.MfbTools.Extensions;
using Antick.MfbTools.Utils;
using Autofac;

namespace Antick.Lab.Kernel.Legacy.Logic.Terminal
{
    public partial class ProfitRatioView : Form
    {
        private readonly TerminalCore terminalCore;

        public Instrument Instrument
        {
            get { return (Instrument)comboBoxInstruments.SelectedItem; }
            set { comboBoxInstruments.SelectedItem = value; }
        }

        public TimeFrame TimeFrame
        {
            get
            {   
                return (TimeFrame) comboBoxTimeFrame.SelectedItem;
            }
            set { comboBoxTimeFrame.SelectedItem = value; }
        }

        public int ProfitRatioMaPeriod
        {
            get
            {
                int period = 10;
                //Int32.TryParse(textBoxPeriodMaProfitRatio.Text, out period);
                return period;
            }
        }

        

        public int AppendStartDays
        {
            get
            {
                var days = 0;
                Int32.TryParse(textBoxAddDayHistory.Text, out days);
                return days;
            }
            set { if (value >= 0) textBoxAddDayHistory.Text = value.ToString(); }
        }

        public int AppendHours
        {
            get
            {
                var hours = 0;
                Int32.TryParse(textBoxAppendHours.Text, out hours);
                return hours;
            }
        }

       
        
     

        public ProjOperType ProjOperType
        {
            get { return (ProjOperType) comboBoxProjOperType.SelectedItem; }
        }

        public ProjTimeType ProjTimeType
        {
            get { return (ProjTimeType) comboBoxProjTimeType.SelectedItem; }
        }

        public int ProjAvrPeriod
        {
            get
            {
                var val = 1;
                Int32.TryParse(textBoxProjAvrPeriod.Text, out val);
                return val;
            }
        }

        private int lastPointIndex = 0;

        private int PointStartIndex = 0;
        private int PointEndIndex = 0;
    
        public ProfitRatioView()
        {
            InitializeComponent();

            terminalCore = new TerminalCore(chart1);
            terminalCore.TerminalMode = TerminalMode.Oanda;

            chart1.MouseMove += chart1_MouseMove;
            chart1.MouseDown += chart1_MouseDown;
            chart1.MouseUp += chart1_MouseUp;

          

          

            comboBoxProjOperType.DataSource = ProjOperTypes.List();
            comboBoxProjOperType.SelectedIndex = 0;

            comboBoxProjTimeType.DataSource = ProjTimeTypes.List();
            comboBoxProjTimeType.SelectedIndex = 0;
        }

        void chart1_MouseUp(object sender, MouseEventArgs e)
        {
            PointEndIndex = lastPointIndex;
            this.InvokeAsync(c =>
            {
                var sourceData = terminalCore.sourceData;
                labelEndPointTime.Text = sourceData[PointEndIndex].Candle.Time.ToString();
            });
        }
        

        void chart1_MouseDown(object sender, MouseEventArgs e)
        {
            PointStartIndex = lastPointIndex;
            this.InvokeAsync(c =>
            {
                var sourceData = terminalCore.sourceData;
                labelStartPointTime.Text = sourceData[PointStartIndex].Candle.Time.ToString();
            });
        }

        void chart1_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                var hitTestResult = chart1.HitTest(e.Location.X, e.Location.Y, ChartElementType.DataPoint);
                if (hitTestResult != null && hitTestResult.ChartElementType != ChartElementType.Nothing)
                {
                    var point = hitTestResult.PointIndex;
                    lastPointIndex = point;
                    OnCursorDataCatching(point);
                }

            }
            catch (ArgumentException ex)
            {
                //MessageBox.Show(ex.ToString());
            }
        }

        private void OnCursorDataCatching(int pointIndex)
        {
            this.InvokeAsync(c =>
            {
                try
                {
                    chart1.ChartAreas[0].CursorX.Position = pointIndex + 1;

                    labelTime.Text = terminalCore.Candle(pointIndex).Time.ToString();
                }
                catch{}
            });
        }


        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            terminalCore.Do(this);
        }

        private void radioButtonProfitPartMode_MouseClick(object sender, MouseEventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.OandaProfitPart;
            terminalCore.UpdateSecondArea();
        }

        private void radioButtonProtitTotalMode_MouseClick(object sender, MouseEventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.OandaProfitTotal;
            terminalCore.UpdateSecondArea();
        }

        private void radioButtonProfitLossMode_MouseClick(object sender, MouseEventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.OandaProfitLoss;
            terminalCore.UpdateSecondArea();
        }

        private void radioButtonProfitDeltaMode_MouseClick(object sender, MouseEventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.OandaProfitDelta;
            terminalCore.UpdateSecondArea();
        }


        private void radioButtonPosGrowMode_MouseClick(object sender, MouseEventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.OandaPositionDelta;
            terminalCore.UpdateSecondArea();
        }

        private void radioButtonOrdersGrowMode_MouseClick(object sender, MouseEventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.OandaOrdersDelta;
            terminalCore.UpdateSecondArea();
        }

        private void buttonPerforance_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(string.Format("Загрузка {0:0.0} сек, калькуляция {1:0.0} сек", _statAll, _statCacl));
        }

        private void checkBoxSubArea2Visible_CheckedChanged(object sender, EventArgs e)
        {
            chart1.ChartAreas[1].Visible = checkBoxSubArea2Visible.Checked;
        }

        private void radioButtonPosGrowMode_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void ProfitRatioView_Load(object sender, EventArgs e)
        {
            this.BringToFront();
            this.Focus();
            this.KeyPreview = true;
        }


        private void checkBoxRpMode_CheckedChanged(object sender, EventArgs e)
        {
            chart1.Series["ReturnPoint"].Enabled = checkBoxRpMode.Checked;
            terminalCore.OandaUseReturnPoint = checkBoxRpMode.Checked;
            terminalCore.ChangeVerticalRangeMainArea();
        }

        private void checkBoxPbMode_CheckedChanged(object sender, EventArgs e)
        {
            chart1.Series["ProfitBalance"].Enabled = checkBoxPbMode.Checked;
            terminalCore.OandaUseProfitBalance = checkBoxPbMode.Checked;
            terminalCore.ChangeVerticalRangeMainArea();
        }

        private void checkBoxPlbMode_CheckedChanged(object sender, EventArgs e)
        {
            chart1.Series["PosLossBalance"].Enabled = checkBoxPlbMode.Checked;
            terminalCore.OandaUsePosLossBalance = checkBoxPlbMode.Checked;
            terminalCore.ChangeVerticalRangeMainArea();
        }

        private void radioButtonOiPositionsMode_MouseClick(object sender, MouseEventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.PositionsOi;
            terminalCore.UpdateSecondArea();
        }

        private void radioButtonOiOrdersMode_MouseClick(object sender, MouseEventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.OrdersOi;
            terminalCore.UpdateSecondArea();
        }

        private void checkBoxMfbMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.TerminalMode = checkBoxMfbMode.Checked ? TerminalMode.Mfb : TerminalMode.Oanda;
            if (terminalCore.TerminalMode == TerminalMode.Mfb)
                terminalCore.Do(this);
        }

        private void comboBoxInstruments_SelectedIndexChanged(object sender, EventArgs e)
        {
            terminalCore.Instrument = Instrument;
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            terminalCore.Date = dateTimePicker1.Value;
        }

        private void textBoxPeriodMaProfitRatio_TextChanged(object sender, EventArgs e)
        {
            terminalCore.OandaPrAvrMaPeriod = ProfitRatioMaPeriod;
        }

        private void comboBoxTimeFrame_SelectedIndexChanged(object sender, EventArgs e)
        {
            terminalCore.TimeFrame = TimeFrame;
        }

        private void radioButtonLongShortMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.MfbLongAndShort;
            terminalCore.UpdateSecondArea();
        }

        private void radioButtonOiMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.MfbOi;
            terminalCore.UpdateSecondArea();
        }

        private void checkBoxMfbCaclBeetMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.MfbCalculateBeetweenBars = checkBoxMfbCaclBeetMode.Checked;
            if (terminalCore.TerminalMode == TerminalMode.Mfb) terminalCore.Do(this);
        }

        private void radioButtonPosClosedMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.MfbPositionsClosed;
            terminalCore.UpdateSecondArea();
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.MfbPositionsOpened;
            terminalCore.UpdateSecondArea();
        }

        private void checkBoxMfbUserPosCaclMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.MfbUsePositions = checkBoxMfbUserPosCaclMode.Checked;
            terminalCore.UpdateSecondArea();
        }

        private void radioButtonMfbClosedPosMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.MfbPositionsClosedDelta;
            terminalCore.UpdateSecondArea();
        }

        private void checkBoxOandaRadiusMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.OandaUseRadius100 = checkBoxOandaRadiusMode.Checked;
        }

        private void textBoxAddDayHistory_TextChanged(object sender, EventArgs e)
        {
            terminalCore.AppendStartDays = AppendStartDays;
        }

        private void textBoxAppendHours_TextChanged(object sender, EventArgs e)
        {
            terminalCore.MfbAppendHours = AppendHours;
        }

        private void radioButtonRatioDelta_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.OandaRatioDelta;
            terminalCore.UpdateSecondArea();
        }

        private void radioButtonRatioMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.MfbRatio;
            terminalCore.UpdateSecondArea();
        }

        private void textBoxRadiusValue_TextChanged(object sender, EventArgs e)
        {
            terminalCore.Radius = Radius;
        }

        private void radioButtonRatioChangeMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.OandaRatioChange;
            terminalCore.UpdateSecondArea();
        }

        private void textBoxRatioAbsMinChange_TextChanged(object sender, EventArgs e)
        {
            terminalCore.MinRatioAbsChange = MinRatioAbsChange;
        }

        private void radioButtonRatioDeltaAverageMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.RatioDeltaAverage;
            terminalCore.UpdateSecondArea();
        }

        private void radioButtonGlassValueMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.OandaGlassVolume;
            terminalCore.UpdateSecondArea();
        }

        private void radioButtonTrueDeltaMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.MfbTrueDelta;
            terminalCore.UpdateSecondArea();
        }

        private void radioButtonTrueDdMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.MfbTrueDD;
            terminalCore.UpdateSecondArea();
        }

        private void checkBoxOandaRatioUseOrders_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.OandaRatioUseOrders = checkBoxOandaRatioUseOrders.Checked;
            terminalCore.UpdateSecondArea();
        }

        private void radioButtonOiDeltaMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.MfbOiDelta;
            terminalCore.UpdateSecondArea();
        }

        private void radioButtonRatioStopsMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.RatioStops50;
            terminalCore.UpdateSecondArea();
        }

        private void radioButtonRatioStops100Mode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.RatioStops100;
            terminalCore.UpdateSecondArea();
        }


        private void radioButtonProfitBalanceDelta_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.ProfitBalanceDelta;
            terminalCore.UpdateSecondArea();
        }

      

        private void radioButtonEatenStopsDeltaMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.OandaEatenStopsDelta;
            terminalCore.UpdateSecondArea();
        }

        private void radioButtonSantPosGrowMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.OandaSantPosGrowLines;
            terminalCore.UpdateSecondArea();
        }

        private void radioButtonSantTakesGrowMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.OandaSantStopsGrowLines;
            terminalCore.UpdateSecondArea();
        }

        private void radioButtonTakesGrowMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.OandaSantTakesGrowLines;
            terminalCore.UpdateSecondArea();
        }

        private void radioButtonStopsBalanceMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.OandaStopsBalance;
            terminalCore.UpdateSecondArea();
        }

    

      

        private void radioButtonRatiosStops200pMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.RatioStops200;
            terminalCore.UpdateSecondArea();
        }

        private void radioButtonRatioSuport50Mode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.RatioSupport50;
            terminalCore.UpdateSecondArea();
        }

        private void radioButtonSupport100Mode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.RatioSupport100;
            terminalCore.UpdateSecondArea();
        }

        private void radioButtonRatioSupport200Mode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.RatioSupport200;
            terminalCore.UpdateSecondArea();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void radioButtonOandaGlRatioMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.OandaGlRatio;
            terminalCore.UpdateSecondArea();
        }

        private void radioButtonOandaGlProfitPercentMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.OandaGlProfitPercent;
            terminalCore.UpdateSecondArea();
        }

        private void radioButtonOandaGlProfitPartMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.OandaGlProfitPart;
            terminalCore.UpdateSecondArea();
        }

        private void radioButtonMfbPosGrowAreasMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.PosGrowHistorgramAvrCustom;
            terminalCore.UpdateSecondArea();
        }

        private void radioButtonMfbPosGrowHistorgamAvr20Mode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.PosGrowHistorgramAvr20;
            terminalCore.UpdateSecondArea();
        }

        private void textBoxMfbAvrHistogramPeriod_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                terminalCore.SecondAreaType = TerminalAreaMode.PosGrowHistorgramAvrCustom;
                terminalCore.UpdateSecondArea();
            }
        }

        private void textBoxCountPosPeriod_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                terminalCore.SecondAreaType = TerminalAreaMode.PosGrowHistogramAvrCountCustom;
                terminalCore.UpdateSecondArea();
            }
        }

        private void PosGrowHistogramAvrCountCustomMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.PosGrowHistogramAvrCountCustom;
            terminalCore.UpdateSecondArea();
        }

        private void radioButtonOandaOldPosDelta_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.OandaPositionDeltaClear;
            terminalCore.UpdateSecondArea();
        }

        private void checkBoxBarUpOnlyMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.BarsUpOnlyMode = checkBoxBarUpOnlyMode.Checked;
            terminalCore.UpdateMainArea();
        }

        private void checkBoxDownOnly_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.BarDownOnlyMode = checkBoxDownOnly.Checked;
            terminalCore.UpdateMainArea();
        }

        private void radioButtonAvrPriceStepMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.AvrPriceStep;
            terminalCore.UpdateSecondArea();
        }

        private void radioButtonAvrHlPriceStepMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.AvrPriceStepHl;
            terminalCore.UpdateSecondArea();
        }

        private void checkBoxAvrPriceStepDirMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.AvrPriceStepUseDirection = checkBoxAvrPriceStepDirMode.Checked;
            terminalCore.UpdateSecondArea();
        }

        private void radioButtonAvrPriceStepDirDeltaMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.AvrPriceStepDirDelta;
            terminalCore.UpdateSecondArea();
        }

        private void checkBoxShowOpenPrice_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.ShowOpenPrice = checkBoxShowOpenPrice.Checked;
            terminalCore.UpdateMainArea();
        }

        private void radioButtonPositionsDeltaGrowExtMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.OandaPositionsDeltaExt;
            terminalCore.UpdateSecondArea();
        }

        private void radioButtonAvrPriceStepDirPercMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.AvrStepDirPerc;
            terminalCore.UpdateSecondArea();
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void radioButtonMfbLongAndShortDeltaMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.MfbLongAndShortDelta;
            terminalCore.UpdateSecondArea();
        }

        private void checkBoxFuturesTimeOpen_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.ShowOpenPriceFutures = checkBoxFuturesTimeOpen.Checked;
            terminalCore.UpdateMainArea();
        }

        private void radioButtonMfbOpenPosLineMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.MfbPositionsOpenedLine;
            terminalCore.UpdateSecondArea();
        }

        private void radioButtonMfbClosePosLineMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.MfbPositionsClosedLine;
            terminalCore.UpdateSecondArea();
        }

        private void radioButtonMfbHolderDeltaMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.MfbHolderDelta;
            terminalCore.UpdateSecondArea();
        }

        private void radioButtonMfbAntiHolderDeltaMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.MfbAntiHolderDelta;
            terminalCore.UpdateSecondArea();
        }

        private void radioButtonMfbHolderOiDeltaMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.MfbHolderOiDelta;
            terminalCore.UpdateSecondArea();
        }

        private void buttonDataSetToday_Click(object sender, EventArgs e)
        {
            dateTimePicker1.Value = DateUtils.WorkDayStart();
        }


        private void radioButtonMfbOpenPosAvrMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.MfbPosDeltaAvr;
            terminalCore.UpdateSecondArea();
        }

        private void radioButtonMfbDeltaGrowPercentMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.MfbPositionsDeltaGrowPercent;
            terminalCore.UpdateSecondArea();
        }

     
        private void comboBoxProjOperType_SelectedIndexChanged(object sender, EventArgs e)
        {
            terminalCore.ProjGroup.OperType = ProjOperType;
            terminalCore.UpdateSecondArea();
        }

        private void comboBoxProjTimeType_SelectedIndexChanged(object sender, EventArgs e)
        {
            terminalCore.ProjGroup.TimeType = ProjTimeType;
            terminalCore.UpdateSecondArea();
        }

        private void radioButtonMfbDeltaGrowHistogramMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.MfbPosGrowHistogram;
            terminalCore.UpdateSecondArea();
        }

        private void checkBoxMfbShowDeltaMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.MfbBarsColor = checkBoxMfbShowDeltaMode.Checked;
            terminalCore.UpdateMainArea();
        }

        private void radioButtonMfbHorProfileBalanceMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType =  TerminalAreaMode.MfbHorProfileBalanceExperemental;
            terminalCore.UpdateSecondArea();
        }

        private void radioButtonMfHorProfileBalanceChangeMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.MfbHorProfileBalanceExperementalDeltaChange;
            terminalCore.UpdateSecondArea();
        }

        private void checkBoxMfbBarsDeltaColorsExt_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.MfbBarColorExt1 = checkBoxMfbBarsDeltaColorsExt.Checked;
            terminalCore.UpdateMainArea();
        }

        private void buttoAddWorkDay_Click(object sender, EventArgs e)
        {
            terminalCore.AppendStartDays++;
            textBoxAddDayHistory.Text = terminalCore.AppendStartDays.ToString();
        }

        private void buttonSubWorkDays_Click(object sender, EventArgs e)
        {
            if (terminalCore.AppendStartDays > 0)
            {
                terminalCore.AppendStartDays--;
                textBoxAddDayHistory.Text = terminalCore.AppendStartDays.ToString();
            }
        }

        private void checkBoxMfbBarColorExt2Mode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.MfbBarColorExt2 = checkBoxMfbBarColorExt2Mode.Checked;
            terminalCore.UpdateMainArea();
        }

        private void ProfitRatioView_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Q:
                    checkBoxMfbShowDeltaMode.Checked = !checkBoxMfbShowDeltaMode.Checked;
                    break;
                case Keys.W:
                    checkBoxMfbBarsDeltaColorsExt.Checked = !checkBoxMfbBarsDeltaColorsExt.Checked;
                    break;
                case Keys.E:
                    checkBoxMfbBarColorExt2Mode.Checked = !checkBoxMfbBarColorExt2Mode.Checked;
                    break;
                case Keys.Z:
                    if (!checkBoxMfbMode.Checked)
                    checkBoxMfbMode.Checked = true;
                    else terminalCore.Do(this);
                    break;
                case Keys.H:
                    checkBoxMfbBarColorHlMode.Checked = !checkBoxMfbBarColorHlMode.Checked;
                    break;
                case Keys.A:
                    chart1.ChartAreas[1].Visible = !chart1.ChartAreas[1].Visible;
                    break;

                case Keys.Oemplus:
                    ZoomTfPlus();
                    break;

                case Keys.OemMinus:
                    ZoomTfMinus();
                    break;

                case Keys.PageUp:
                    AppendStartDays++;
                    break;

                case Keys.PageDown:
                    AppendStartDays--;
                    break;

                case Keys.P:
                    RaiseHorizontalProfile();
                    break;
            }
        }

        private void checkBoxMfbBarColorHlMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.BarHlMode = checkBoxMfbBarColorHlMode.Checked;
            terminalCore.UpdateMainArea();
        }

        private void ProfitRatioView_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void buttonGbpSet_Click(object sender, EventArgs e)
        {
            Instrument = Instrument.GBP_USD;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Instrument = Instrument.EUR_USD;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Instrument = Instrument.USD_CAD;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Instrument = Instrument.USD_JPY;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Instrument = Instrument.AUD_USD;
        }

        private void buttonTfM5Set_Click(object sender, EventArgs e)
        {
            TimeFrame = TimeFrame.M5;
        }

        private void buttonTfM10Set_Click(object sender, EventArgs e)
        {
            TimeFrame = TimeFrame.M10;
        }

        private void buttonTfM20Set_Click(object sender, EventArgs e)
        {
            TimeFrame = TimeFrame.M20;
        }

        private void buttonTfM30Set_Click(object sender, EventArgs e)
        {
            TimeFrame = TimeFrame.M30;
        }

        private void buttonTfH1Set_Click(object sender, EventArgs e)
        {
            TimeFrame = TimeFrame.H1;
        }

        private void buttonTfH4Set_Click(object sender, EventArgs e)
        {
            TimeFrame = TimeFrame.H4;
        }

        private void buttonTfD1Set_Click(object sender, EventArgs e)
        {
            TimeFrame = TimeFrame.D1;
        }


        private void ZoomTfPlus()
        {
            var tf = TimeFrame;

            switch (tf)
            {
                    case TimeFrame.M1:
                    TimeFrame = TimeFrame.M5;
                    break;

                    case TimeFrame.M2:
                    TimeFrame = TimeFrame.M5;
                    break;

                    case TimeFrame.M3:
                    TimeFrame = TimeFrame.M5;
                    break;

                    case TimeFrame.M4:
                    TimeFrame = TimeFrame.M5;
                    break;

                    case TimeFrame.M5:
                    TimeFrame = TimeFrame.M10;
                    break;

                    case TimeFrame.M10:
                    TimeFrame = TimeFrame.M20;
                    break;

                    case TimeFrame.M15:
                    TimeFrame = TimeFrame.M30;
                    break;

                    case TimeFrame.M20:
                    TimeFrame = TimeFrame.M30;
                    break;

                    case TimeFrame.M30:
                    TimeFrame = TimeFrame.H1;
                    break;

                    case TimeFrame.H1:
                    TimeFrame = TimeFrame.H4;
                    break;

                    case TimeFrame.H4:
                    TimeFrame = TimeFrame.D1;
                    break;

                default:
                    return;
            }
        }

        private void ZoomTfMinus()
        {
            var tf = TimeFrame;

            switch (tf)
            {
                case TimeFrame.M2:
                    TimeFrame = TimeFrame.M1;
                    break;

                case TimeFrame.M3:
                    TimeFrame = TimeFrame.M1;
                    break;

                case TimeFrame.M4:
                    TimeFrame = TimeFrame.M1;
                    break;

                case TimeFrame.M5:
                    TimeFrame = TimeFrame.M1;
                    break;

                case TimeFrame.M10:
                    TimeFrame = TimeFrame.M5;
                    break;

                case TimeFrame.M15:
                    TimeFrame = TimeFrame.M10;
                    break;

                case TimeFrame.M20:
                    TimeFrame = TimeFrame.M10;
                    break;

                case TimeFrame.M30:
                    TimeFrame = TimeFrame.M20;
                    break;

                case TimeFrame.H1:
                    TimeFrame = TimeFrame.M30;
                    break;

                case TimeFrame.H4:
                    TimeFrame = TimeFrame.H1;
                    break;

                case TimeFrame.D1:
                    TimeFrame = TimeFrame.H4;
                    break;

                default:
                    return;
            }
        }

        private void RaiseHorizontalProfile()
        {
            this.InvokeAsync(c =>
            {
                var sourceDate = terminalCore.sourceData;

                var startTime = sourceDate[PointStartIndex].Candle.Time;
                var endTime = sourceDate[PointEndIndex].Candle.Time;

                var profile = new HorizontalProfile(Instrument, startTime, endTime);
                profile.Show();
            });
        }

        private void textBoxProjAvrPeriod_TextChanged(object sender, EventArgs e)
        {
            terminalCore.ProjGroup.AveragePeriod = ProjAvrPeriod;
        }

        private void textBoxProjAvrPeriod_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                terminalCore.UpdateSecondArea();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double growLong = 0.0, growShort = 0.0;
            double closeLong = 0.0,closeShort = 0;

            var path = @"Analize\TestData\Grow\grow.txt";
            if (!Directory.Exists(@"Analize\TestData\Grow\"))
                Directory.CreateDirectory(@"Analize\TestData\Grow\");
            
            using (
                StreamWriter sw =
                    new StreamWriter(path, false))
            {
                foreach (var d in terminalCore.oandaData)
                {
                    growLong = d.Oanda.Sant.Mush.PosGrowLong;
                    growShort = d.Oanda.Sant.Mush.PosGrowShort;
                    closeLong = d.Oanda.Sant.Mush.PosСloseLong;
                    closeShort = d.Oanda.Sant.Mush.PosСloseShort;
                    
                    sw.WriteLine("{0:0.00}\t{1:0.00}\t{2:0.00}\t{3:0.00}", growLong, growShort, closeLong, closeShort);
                }
            }
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.MfbDeltaRsi;
            terminalCore.UpdateSecondArea();
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.OandaRatioStopsAndPos;
            terminalCore.UpdateSecondArea();
        }

        private void radioButton5_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.OandaRatioStopsAndPosTotal;
            terminalCore.UpdateSecondArea();
        }

        private void radioButtonOandaRatioPos50BalanceMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.OandaRatioPos50Balance;
            terminalCore.UpdateSecondArea();
        }

        private void radioButton6_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.OandaRatioStop50Balance;
            terminalCore.UpdateSecondArea();
        }

        private void radioButton7_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.MfbOiAndGrowDelta;
            terminalCore.UpdateSecondArea();
        }

        private void checkBoxMfbTrueColorMode_CheckedChanged(object sender, EventArgs e)
        {
            terminalCore.MfbBarTrueColor = checkBoxMfbTrueColorMode.Checked;
            terminalCore.UpdateMainArea();
        }

        private void checkBoxUseStopBalance_CheckedChanged(object sender, EventArgs e)
        {
            chart1.Series["StopBalance"].Enabled = checkBoxUseStopBalance.Checked;
            terminalCore.OandaUserStopBalance = checkBoxUseStopBalance.Checked;
            terminalCore.ChangeVerticalRangeMainArea();
        }

        private void checkBoxTakeBalance_CheckedChanged(object sender, EventArgs e)
        {
            chart1.Series["TakeBalance"].Enabled = checkBoxTakeBalance.Checked;
            terminalCore.OandaUseTakeBalance = checkBoxTakeBalance.Checked;
            terminalCore.ChangeVerticalRangeMainArea();
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            chart1.Series["StopBalance100"].Enabled = checkBoxStopBalance100.Checked;
            terminalCore.OandaUseStopBalance100 = checkBoxStopBalance100.Checked;
            terminalCore.ChangeVerticalRangeMainArea();
        }

        private void checkBoxStopBalance50_CheckedChanged(object sender, EventArgs e)
        {
            chart1.Series["StopBalance50"].Enabled = checkBoxStopBalance50.Checked;
            terminalCore.OandaUseStopBalance50 = checkBoxStopBalance50.Checked;
            terminalCore.ChangeVerticalRangeMainArea();
        }

        private void checkBoxUsePosBalance_CheckedChanged(object sender, EventArgs e)
        {
            chart1.Series["PosBalance"].Enabled = checkBoxUsePosBalance.Checked;
            terminalCore.OandaUserPosBalance = checkBoxUsePosBalance.Checked;
            terminalCore.ChangeVerticalRangeMainArea();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void radioButton1_MouseClick(object sender, MouseEventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.MfbPositionDelta;
            terminalCore.UpdateSecondArea();
        }

        private void radioButtonOandaSantSmartMode_MouseClick(object sender, MouseEventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.OandaSantSmartGrow;
            terminalCore.UpdateSecondArea();
        }

        private void radioButton8_Click(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.MfbStupidGrowDelta;
            terminalCore.UpdateSecondArea();
        }

        private void radioButton9_Click(object sender, EventArgs e)
        {
            terminalCore.SecondAreaType = TerminalAreaMode.MfbRatioDelta;
            terminalCore.UpdateSecondArea();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            terminalCore.TerminalMode = TerminalMode.Rates;
            terminalCore.StartTimeRange = dateTimePickerStartRange.Value;
            terminalCore.EndTimeRange = dateTimePickerEndRange.Value;
            terminalCore.RangeStep = textBoxRangeStep.Text;
            terminalCore.Do(this);

        }
    }
}
