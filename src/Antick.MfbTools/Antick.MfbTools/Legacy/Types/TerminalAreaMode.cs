﻿namespace Antick.MfbTools.Legacy.Types
{
    public enum TerminalAreaMode
    {
        //Блок Оанды, расчет отношений профита.

        /// <summary>
        /// График отношения внутри всего профита, показывает кто больше в плюсе, лонг или шорт.
        /// </summary>
        OandaProfitPart,

        /// <summary>
        /// График отношения профита лонга и шорта, ко всему стакану
        /// </summary>
        OandaProfitTotal,

        /// <summary>
        /// График отношения профита лонга к убытку шорта(на тех же ценах), аналогично для шорта.
        /// </summary>
        OandaProfitLoss,
        
        /// <summary>
        /// График прироста или спада профит-ратио, ввиде дельты
        /// </summary>
        OandaProfitDelta,

        //-----------------------------------
        
        // Блок оанды, расчет роста позиций/орденов, дельт, ОИ по данным стакана
        
        /// <summary>
        /// График роста орденов ввиде дельты в стакане Оанды
        /// </summary>
        OandaOrdersDelta,
        
        /// <summary>
        /// ОИ роста однево в стакане Оанды
        /// </summary>
        OrdersOi,
        
        /// <summary>
        /// ОИ по росту орденов в стакане Оанды, так и по данным МБФ
        /// </summary>
        PositionsOi,


        //------------------------------------------------


        // Блок общих данных между оандой и МФБ

        /// <summary>
        /// График роста орденов ввиде дельты в стакане Оанды, так же и по данным МФБ
        /// </summary>
        //PositionsDelta,
        OandaPositionDelta,

        MfbPositionDelta,

        // -------------------------------------

        OandaEatenStopsDelta,


        OandaSantSmartGrow,

        /// <summary>
        /// График изменения ратио стакана Оанды ввиде графика
        /// </summary>
        MfbRatio,

        OandaRatio,
        
        OandaRatioDelta,
        RatioDeltaAverage,

        OandaRatioChange,

        RatioStops50,
        RatioStops100,
        RatioStops200,

        RatioSupport50,
        RatioSupport100,
        RatioSupport200,

        
        OandaGlassVolume,

        ProfitBalanceDelta,

        MfbLongAndShort,
        MfbLongAndShortDelta,

        MfbOi,
        MfbOiDelta,


        MfbPositionsClosed,
        MfbPositionsClosedLine,

        MfbPositionsOpened,
        MfbPositionsOpenedLine,

        MfbPositionsClosedDelta,



        MfbTrueDelta,
        MfbTrueDD,

        MfbDeltaRsi,

        /// <summary>
        /// Усредненная дельта.
        /// </summary>
        DeltaAverage,


        OandaSantPosGrowLines,
        OandaSantStopsGrowLines,
        OandaSantTakesGrowLines,

        OandaStopsBalance,


        OandaRatioStopsAndPos,
        OandaRatioStopsAndPosTotal,

        OandaGlRatio,
        OandaGlProfitPercent,
        OandaGlProfitPart,


        /// <summary>
        /// Рост позиций ввиде ареа
        /// </summary>
        PosGrowHistorgramAvrCustom,

        PosGrowHistorgramAvr20,

        PosGrowHistogramAvrCountCustom,


        OandaPositionDeltaClear,


        AvrPriceStep,
        AvrPriceStepHl,
        AvrPriceStepDirDelta,

        /// <summary>
        /// Отношения направленного хода к средней по этому направлению.
        /// </summary>
        AvrStepDirPerc,


        OandaPositionsDeltaExt,


        MfbHolderDelta,

        MfbAntiHolderDelta,

        MfbHolderOiDelta,

        MfbPosDeltaAvr,

        MfbPositionsDeltaGrowPercent,

        MfbPosGrowHistogram,

        MfbHorProfileBalanceExperemental,
        MfbHorProfileBalanceExperementalDeltaChange,

        OandaRatioPos50Balance,
        OandaRatioStop50Balance,

        MfbOiAndGrowDelta,


        MfbStupidGrowDelta,

        MfbRatioDelta,

        MfbParetoDeleta,
    }
}
