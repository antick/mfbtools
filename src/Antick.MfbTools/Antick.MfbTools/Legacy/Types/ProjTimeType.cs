﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Antick.MfbTools.Legacy.Types
{
    public enum ProjTimeType
    {
        None,
        All,
        Dayli,
        Session,
        H4,
        H3,
        H1,
        M30,
        M20
    }

    public static class ProjTimeTypes
    {
        public static List<ProjTimeType> List()
        {
            return Enum.GetValues(typeof(ProjTimeType)).Cast<ProjTimeType>().ToList();
        }
    }
}
