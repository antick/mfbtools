﻿namespace Antick.MfbTools.Legacy.Types
{
    public enum IndicatorViewType
    {
        /// <summary>
        /// В случае если типизации нет или стд поведение
        /// </summary>
        Default,
        
        /// <summary>
        /// Все данные, до вычисления дельты, но выводятся графически как дельта 2двух значний
        /// </summary>
        DeltaAll,

        /// <summary>
        /// Обычная дельта
        /// </summary>
        Delta,

        /// <summary>
        /// Тоже дельта, но ввиде гистограммы положительных значений.
        /// </summary>
        Histogram,


        Line,
    }
}
