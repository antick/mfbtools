﻿namespace Antick.MfbTools.Legacy.Types
{
    public class ProjGroup
    {
        public ProjOperType OperType { get; set; }
        public ProjTimeType TimeType { get; set; }
        public int AveragePeriod { get; set; }

        public ProjGroup()
        {
            OperType = ProjOperType.None;
            TimeType = ProjTimeType.None;
            AveragePeriod = 1;
        }
    }
}
