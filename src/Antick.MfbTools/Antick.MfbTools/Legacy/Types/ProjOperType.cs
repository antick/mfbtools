﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Antick.MfbTools.Legacy.Types
{
    public enum ProjOperType
    {
        None,
        Sum,
        Count,
        SumAbs,
        CountAbs,

        Average
    }

    public static class ProjOperTypes
    {
        public static List<ProjOperType> List()
        {
            return Enum.GetValues(typeof (ProjOperType)).Cast<ProjOperType>().ToList();
        } 
    }
}
