﻿namespace Antick.MfbTools.Legacy
{
    public class TerminalConfig
    {
        public static OandaConfig Oanda = new OandaConfig();
        public static MfbConfig Mfb = new MfbConfig();
    }


    public class OandaConfig
    {
        public bool ReturnPoint = false;

        public bool ProfitDataExt = true;

        /// <summary>
        /// Использование данныех по сантименту, рост позиций и орденов и связанные расчеты
        /// </summary>
        public bool Santiment = true;

        public bool SantimentEatenStops = false;
        
        /// <summary>
        /// Вычисление объема стакана
        /// </summary>
        public bool GlassVolume = true;

        public bool RatiosPosAndOrders = true;

        /// <summary>
        /// Набор вычислений по стопам и сопротивлениям с разной глубиной
        /// </summary>
        public bool RatiosStopsSupport = true;

        /// <summary>
        /// Набов вычислений по чистому стакану.
        /// </summary>
        public bool ClearGlassRatiosExt = true;
    }

    public class MfbConfig
    {
        
    }
}
