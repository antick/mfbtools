﻿using System;
using System.Collections.Generic;
using System.Linq;
using Antick.MfbTools.Legacy.Types;

namespace Antick.MfbTools.Legacy
{
    public class TerminalProj
    {
        /// <summary>
        /// Тип операции
        /// </summary>
        private readonly ProjOperType _operType;

        /// <summary>
        /// Период проведения операции по времени
        /// </summary>
        private readonly ProjTimeType _timeType;

        /// <summary>
        /// Время текущей последней даты-итема
        /// </summary>
        private  DateTime _curTime;

        /// <summary>
        /// Время предыдущей даты-итема
        /// </summary>
        private  DateTime _pTime;

        /// <summary>
        /// Первичное значение даты-итема
        /// </summary>
        private readonly double _val;

        /// <summary>
        /// Предыдущее значение проекций
        /// </summary>
        private readonly double _prevProj;

        /// <summary>
        /// Окончательное значение в текущей итерации
        /// </summary>
        private double _result;

        public  bool _isBreaked;

        public TerminalProj(ProjOperType operType, ProjTimeType timeType, DateTime curTime, DateTime pTime, double val, double prevProj)
        {
            _operType = operType;
            _timeType = timeType;
            _curTime = curTime;
            _pTime = pTime;
            _val = val;
            _prevProj = prevProj;
        }

        /// <summary>
        /// Операция на абслолютных значениях
        /// </summary>
        /// <returns></returns>
        private bool Abs()
        {
            return _operType == ProjOperType.SumAbs || _operType == ProjOperType.CountAbs;
        }

        /// <summary>
        /// Операция суммирования
        /// </summary>
        /// <returns></returns>
        private bool Sum()
        {
            return _operType == ProjOperType.Sum || _operType == ProjOperType.SumAbs;
        }

        /// <summary>
        /// Операция подсчета итераций
        /// </summary>
        /// <returns></returns>
        private bool Count()
        {
            return _operType == ProjOperType.Count || _operType == ProjOperType.CountAbs;
        }

        /// <summary>
        /// Подсчет за все время
        /// </summary>
        /// <returns></returns>
        private bool All()
        {
            return _timeType == ProjTimeType.All;
        }

        /// <summary>
        /// Подсчет за каждый день
        /// </summary>
        /// <returns></returns>
        private bool Dayli()
        {
            return _timeType == ProjTimeType.Dayli;
        }

        /// <summary>
        /// Подсчет за каждые 4 часа
        /// </summary>
        /// <returns></returns>
        private bool H4()
        {
            return _timeType == ProjTimeType.H4;
        }

        /// <summary>
        /// Подсчет за каждые 3 часа
        /// </summary>
        /// <returns></returns>
        private bool H3()
        {
            return _timeType == ProjTimeType.H3;
        }

        /// <summary>
        /// Подсчет за каждый час
        /// </summary>
        /// <returns></returns>
        private bool Hourly()
        {
            return _timeType == ProjTimeType.H1;
        }

        /// <summary>
        /// Подсчет за каждые 30 минут
        /// </summary>
        /// <returns></returns>
        private bool M30()
        {
            return _timeType == ProjTimeType.M30;
        }

        /// <summary>
        /// Подсчет за каждые 20 минут
        /// </summary>
        /// <returns></returns>
        private bool M20()
        {
            return _timeType == ProjTimeType.M20;
        }

        /// <summary>
        /// Подсчет за сессии(по моему виду)
        /// </summary>
        /// <returns></returns>
        private bool Session()
        {
            return _timeType == ProjTimeType.Session;
        }

        /// <summary>
        /// Процедура добавления (примениется при суммировании
        /// </summary>
        /// <param name="vv"></param>
        public void Append(double vv)
        {
            var ap = Abs() ? Math.Abs(vv) : vv;
            _result = _prevProj + ap;
        }

        /// <summary>
        /// Процедура добавления (примениется при суммировании)
        /// </summary>
        public void Append()
        {
            Append(_val);
        }

        /// <summary>
        /// Процедура получения данные для итераций
        /// </summary>
        public int Counter(double vv)
        {
            if (!Abs())
                return vv > 0 ? 1 : vv < 0 ? -1 : 0;
            else return 1;
        }

        /// <summary>
        /// Процедура получения данные для итераций
        /// </summary>
        public int Counter()
        {
            return Counter(_val);
        }

        /// <summary>
        /// Логика увеличения
        /// </summary>
        public void IncreaseLogic()
        {
            if (Sum()) Append();
            else if (Count()) Append(Counter());
            else _result = _val;
        }

        /// <summary>
        /// Логика сброса
        /// </summary>
        public void BreakLogic()
        {
            if (Sum()) _result = _val;
            else if (Count()) _result = Counter();
            else _result = _val;

            _isBreaked = true;
        }

        /// <summary>
        /// Общая логика подсчет по сессиям
        /// </summary>
        /// <param name="breaks"></param>
        private void SessionsFunc(List<int[]> breaks)
        {
            var search = false;
            if (breaks.Any(@break => _pTime.Hour ==  @break[0] && _curTime.Hour == @break[1]))
            {
                BreakLogic();
                search = true;
            }
            if (!search)
            {
                IncreaseLogic();
            }
        }

        /// <summary>
        /// Логика подсчета за все время
        /// </summary>
        /// <returns></returns>
        private bool AllLogic()
        {
            if (All())
            {
                IncreaseLogic();
                return true;
            }
            return false;
        }

        /// <summary>
        /// Логика подсчета за каждый день
        /// </summary>
        /// <returns></returns>
        private bool DayliLogic()
        {
            if (Dayli())
            {
                if (_curTime.Day == _pTime.Day)
                {
                    IncreaseLogic();
                }
                else
                {
                    BreakLogic();
                }
                return true;
            }
            return false;
        }

        /// <summary>
        /// Логика подсчета за сессию
        /// </summary>
        /// <returns></returns>
        private bool SessionLogic()
        {
            if (Session())
            {
                List<int[]> breaks = new List<int[]> { new[] { 8, 9 }, new[] { 14, 15 }, new[] { 20, 21 } };
                SessionsFunc(breaks);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Логика подсчета за каждые 4 часа
        /// </summary>
        /// <returns></returns>
        private bool H4Logic()
        {
            if (H4())
            {
                List<int[]> breaks = new List<int[]>
                {
                    new[] {3, 4},
                    new[] {7, 8},
                    new[] {11, 12},
                    new[] {15, 16},
                    new[] {20, 21},
                    new[] {23, 0}
                };
                SessionsFunc(breaks);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Логика подсчета за каждые 3 часа
        /// </summary>
        /// <returns></returns>
        private bool H3Logic()
        {
            if (H3())
            {
                List<int[]> breaks = new List<int[]>
                {
                    new[] {2, 3},
                    new[] {5, 6},
                    new[] {8, 9},
                    new[] {11, 12},
                    new[] {14, 15},
                    new[] {17, 18},
                    new[] {20, 21},
                    new[] {23, 0}
                };
                SessionsFunc(breaks);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Логика подсчета за каждый час
        /// </summary>
        /// <returns></returns>
        private bool H1Logic()
        {
            if (Hourly())
            {
                if (_curTime.Hour == _pTime.Hour)
                {
                    IncreaseLogic();
                }
                else
                {
                    BreakLogic();
                }
                return true;
            }
            return false;
        }

        /// <summary>
        /// Логика подсчета по минутно(по 20 или 30 минут сейчас)
        /// </summary>
        /// <returns></returns>
        private bool MLogic()
        {
            if (M20() || M30())
            {
                var min = M20() ? 20 : 30;
                if (_curTime.Minute%min != 0)
                {
                    IncreaseLogic();
                }
                else
                {
                    BreakLogic();
                }
                return true;
            }
            return false;
        }

        public static double Do(ProjGroup proj, DateTime curTime, DateTime pTime,
            double val, double prevProj)
        {
            return Do(proj.OperType, proj.TimeType, curTime, pTime, val, prevProj);
        }

        /// <summary>
        /// Логика проведения итерации
        /// </summary>
        /// <param name="operType"></param>
        /// <param name="timeType"></param>
        /// <param name="curTime"></param>
        /// <param name="pTime"></param>
        /// <param name="val"></param>
        /// <param name="prevProj"></param>
        /// <returns></returns>
        public static double Do(ProjOperType operType, ProjTimeType timeType,  DateTime curTime, DateTime pTime, double val, double prevProj)
        {
            var p = new TerminalProj(operType, timeType, curTime, pTime, val, prevProj);

            var status = p.AllLogic();
            if (status) return p._result;
            status = p.DayliLogic();
            if (status) return p._result;
            status = p.SessionLogic();
            if (status) return p._result;
            status = p.H4Logic();
            if (status) return p._result;
            status = p.H3Logic();
            if (status) return p._result;
            status = p.H1Logic();
            if (status) return p._result;
            status = p.MLogic();
            if (status) return p._result;

            p.BreakLogic();
            return p._result;
        }

        public static double Do(ProjOperType operType, ProjTimeType timeType, DateTime curTime, DateTime pTime,
            double val, double prevProj, out bool breaked)
        {
            var p = new TerminalProj(operType, timeType, curTime, pTime, val, prevProj);

            var status = p.AllLogic();
            if (status) { breaked = p._isBreaked; return p._result; }
            status = p.DayliLogic();
            if (status) { breaked = p._isBreaked; return p._result; }
            status = p.SessionLogic();
            if (status) { breaked = p._isBreaked; return p._result; }
            status = p.H4Logic();
            if (status) { breaked = p._isBreaked; return p._result; }
            status = p.H3Logic();
            if (status) { breaked = p._isBreaked; return p._result; }
            status = p.H1Logic();
            if (status) { breaked = p._isBreaked; return p._result; }
            status = p.MLogic();
            if (status) { breaked = p._isBreaked; return p._result; }

            breaked = p._isBreaked;
            p.BreakLogic();
            return p._result;
        }
    }
}
