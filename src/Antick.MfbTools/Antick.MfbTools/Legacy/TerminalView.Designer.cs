﻿namespace Antick.Lab.Kernel.Legacy.Logic.Terminal
{
    partial class ProfitRatioView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series7 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series8 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series9 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series10 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series11 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series12 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series13 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series14 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series15 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series16 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series17 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series18 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series19 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.comboBoxInstruments = new System.Windows.Forms.ComboBox();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.labelTime = new System.Windows.Forms.Label();
            this.textBoxAddDayHistory = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBoxTimeFrame = new System.Windows.Forms.ComboBox();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButtonLongShortMode = new System.Windows.Forms.RadioButton();
            this.radioButtonOiMode = new System.Windows.Forms.RadioButton();
            this.radioButtonPosClosedMode = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButtonMfbClosedPosMode = new System.Windows.Forms.RadioButton();
            this.textBoxAppendHours = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.textBoxRangeStep = new System.Windows.Forms.TextBox();
            this.dateTimePickerEndRange = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerStartRange = new System.Windows.Forms.DateTimePicker();
            this.radioButton9 = new System.Windows.Forms.RadioButton();
            this.radioButton8 = new System.Windows.Forms.RadioButton();
            this.radioButton7 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.radioButtonMfbDeltaGrowHistogramMode = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBoxProjAvrPeriod = new System.Windows.Forms.TextBox();
            this.comboBoxProjOperType = new System.Windows.Forms.ComboBox();
            this.comboBoxProjTimeType = new System.Windows.Forms.ComboBox();
            this.radioButtonMfbAntiHolderDeltaMode = new System.Windows.Forms.RadioButton();
            this.radioButtonMfbHolderDeltaMode = new System.Windows.Forms.RadioButton();
            this.radioButtonMfbLongAndShortDeltaMode = new System.Windows.Forms.RadioButton();
            this.radioButtonOiDeltaMode = new System.Windows.Forms.RadioButton();
            this.radioButtonTrueDdMode = new System.Windows.Forms.RadioButton();
            this.radioButtonTrueDeltaMode = new System.Windows.Forms.RadioButton();
            this.checkBoxMfbUserPosCaclMode = new System.Windows.Forms.CheckBox();
            this.checkBoxMfbCaclBeetMode = new System.Windows.Forms.CheckBox();
            this.checkBoxMfbMode = new System.Windows.Forms.CheckBox();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.buttonDataSetToday = new System.Windows.Forms.Button();
            this.checkBoxMfbShowDeltaMode = new System.Windows.Forms.CheckBox();
            this.checkBoxMfbBarsDeltaColorsExt = new System.Windows.Forms.CheckBox();
            this.buttoAddWorkDay = new System.Windows.Forms.Button();
            this.buttonSubWorkDays = new System.Windows.Forms.Button();
            this.checkBoxMfbBarColorExt2Mode = new System.Windows.Forms.CheckBox();
            this.checkBoxMfbBarColorHlMode = new System.Windows.Forms.CheckBox();
            this.buttonGbpSet = new System.Windows.Forms.Button();
            this.buttonEurSet = new System.Windows.Forms.Button();
            this.buttonCadSet = new System.Windows.Forms.Button();
            this.buttonJpySet = new System.Windows.Forms.Button();
            this.buttonAudSet = new System.Windows.Forms.Button();
            this.buttonTfM5Set = new System.Windows.Forms.Button();
            this.buttonTfM10Set = new System.Windows.Forms.Button();
            this.buttonTfM20Set = new System.Windows.Forms.Button();
            this.buttonTfM30Set = new System.Windows.Forms.Button();
            this.buttonTfH1Set = new System.Windows.Forms.Button();
            this.buttonTfH4Set = new System.Windows.Forms.Button();
            this.buttonTfD1Set = new System.Windows.Forms.Button();
            this.labelStartPointTime = new System.Windows.Forms.Label();
            this.labelEndPointTime = new System.Windows.Forms.Label();
            this.checkBoxMfbTrueColorMode = new System.Windows.Forms.CheckBox();
            this.checkBoxSubArea2Visible = new System.Windows.Forms.CheckBox();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBoxInstruments
            // 
            this.comboBoxInstruments.FormattingEnabled = true;
            this.comboBoxInstruments.Location = new System.Drawing.Point(1775, 21);
            this.comboBoxInstruments.Name = "comboBoxInstruments";
            this.comboBoxInstruments.Size = new System.Drawing.Size(121, 24);
            this.comboBoxInstruments.TabIndex = 5;
            this.comboBoxInstruments.SelectedIndexChanged += new System.EventHandler(this.comboBoxInstruments_SelectedIndexChanged);
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Location = new System.Drawing.Point(1658, 257);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(108, 41);
            this.buttonUpdate.TabIndex = 6;
            this.buttonUpdate.Text = "Обновить";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(1353, 125);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(180, 22);
            this.dateTimePicker1.TabIndex = 7;
            this.dateTimePicker1.Value = new System.DateTime(2016, 1, 27, 0, 0, 0, 0);
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // labelTime
            // 
            this.labelTime.AutoSize = true;
            this.labelTime.Location = new System.Drawing.Point(47, 907);
            this.labelTime.Name = "labelTime";
            this.labelTime.Size = new System.Drawing.Size(46, 17);
            this.labelTime.TabIndex = 10;
            this.labelTime.Text = "label1";
            // 
            // textBoxAddDayHistory
            // 
            this.textBoxAddDayHistory.Location = new System.Drawing.Point(1727, 125);
            this.textBoxAddDayHistory.Name = "textBoxAddDayHistory";
            this.textBoxAddDayHistory.Size = new System.Drawing.Size(45, 22);
            this.textBoxAddDayHistory.TabIndex = 17;
            this.textBoxAddDayHistory.TextChanged += new System.EventHandler(this.textBoxAddDayHistory_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(1620, 125);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(99, 17);
            this.label3.TabIndex = 15;
            this.label3.Text = "Доп. раб. дни";
            // 
            // comboBoxTimeFrame
            // 
            this.comboBoxTimeFrame.FormattingEnabled = true;
            this.comboBoxTimeFrame.Location = new System.Drawing.Point(1824, 51);
            this.comboBoxTimeFrame.Name = "comboBoxTimeFrame";
            this.comboBoxTimeFrame.Size = new System.Drawing.Size(72, 24);
            this.comboBoxTimeFrame.TabIndex = 18;
            this.comboBoxTimeFrame.SelectedIndexChanged += new System.EventHandler(this.comboBoxTimeFrame_SelectedIndexChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(58, 14);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(39, 21);
            this.radioButton1.TabIndex = 9;
            this.radioButton1.Text = "D";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            this.radioButton1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.radioButton1_MouseClick);
            // 
            // radioButtonLongShortMode
            // 
            this.radioButtonLongShortMode.AutoSize = true;
            this.radioButtonLongShortMode.Location = new System.Drawing.Point(8, 238);
            this.radioButtonLongShortMode.Name = "radioButtonLongShortMode";
            this.radioButtonLongShortMode.Size = new System.Drawing.Size(99, 21);
            this.radioButtonLongShortMode.TabIndex = 19;
            this.radioButtonLongShortMode.TabStop = true;
            this.radioButtonLongShortMode.Text = "Long/Short";
            this.radioButtonLongShortMode.UseVisualStyleBackColor = true;
            this.radioButtonLongShortMode.CheckedChanged += new System.EventHandler(this.radioButtonLongShortMode_CheckedChanged);
            // 
            // radioButtonOiMode
            // 
            this.radioButtonOiMode.AutoSize = true;
            this.radioButtonOiMode.Location = new System.Drawing.Point(8, 265);
            this.radioButtonOiMode.Name = "radioButtonOiMode";
            this.radioButtonOiMode.Size = new System.Drawing.Size(50, 21);
            this.radioButtonOiMode.TabIndex = 19;
            this.radioButtonOiMode.TabStop = true;
            this.radioButtonOiMode.Text = "ОИ";
            this.radioButtonOiMode.UseVisualStyleBackColor = true;
            this.radioButtonOiMode.CheckedChanged += new System.EventHandler(this.radioButtonOiMode_CheckedChanged);
            // 
            // radioButtonPosClosedMode
            // 
            this.radioButtonPosClosedMode.AutoSize = true;
            this.radioButtonPosClosedMode.Location = new System.Drawing.Point(8, 214);
            this.radioButtonPosClosedMode.Name = "radioButtonPosClosedMode";
            this.radioButtonPosClosedMode.Size = new System.Drawing.Size(91, 21);
            this.radioButtonPosClosedMode.TabIndex = 21;
            this.radioButtonPosClosedMode.TabStop = true;
            this.radioButtonPosClosedMode.Text = "Close pos";
            this.radioButtonPosClosedMode.UseVisualStyleBackColor = true;
            this.radioButtonPosClosedMode.CheckedChanged += new System.EventHandler(this.radioButtonPosClosedMode_CheckedChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(6, 14);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(44, 21);
            this.radioButton2.TabIndex = 21;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "All";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButtonMfbClosedPosMode
            // 
            this.radioButtonMfbClosedPosMode.AutoSize = true;
            this.radioButtonMfbClosedPosMode.Location = new System.Drawing.Point(103, 214);
            this.radioButtonMfbClosedPosMode.Name = "radioButtonMfbClosedPosMode";
            this.radioButtonMfbClosedPosMode.Size = new System.Drawing.Size(62, 21);
            this.radioButtonMfbClosedPosMode.TabIndex = 9;
            this.radioButtonMfbClosedPosMode.Text = "Delta";
            this.radioButtonMfbClosedPosMode.UseVisualStyleBackColor = true;
            this.radioButtonMfbClosedPosMode.CheckedChanged += new System.EventHandler(this.radioButtonMfbClosedPosMode_CheckedChanged);
            // 
            // textBoxAppendHours
            // 
            this.textBoxAppendHours.Location = new System.Drawing.Point(1727, 153);
            this.textBoxAppendHours.Name = "textBoxAppendHours";
            this.textBoxAppendHours.Size = new System.Drawing.Size(45, 22);
            this.textBoxAppendHours.TabIndex = 23;
            this.textBoxAppendHours.TextChanged += new System.EventHandler(this.textBoxAppendHours_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(1624, 158);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(90, 17);
            this.label5.TabIndex = 15;
            this.label5.Text = "Часов слева";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button2);
            this.groupBox2.Controls.Add(this.textBoxRangeStep);
            this.groupBox2.Controls.Add(this.dateTimePickerEndRange);
            this.groupBox2.Controls.Add(this.dateTimePickerStartRange);
            this.groupBox2.Controls.Add(this.radioButton9);
            this.groupBox2.Controls.Add(this.radioButton8);
            this.groupBox2.Controls.Add(this.radioButton7);
            this.groupBox2.Controls.Add(this.radioButton3);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Controls.Add(this.groupBox1);
            this.groupBox2.Controls.Add(this.radioButtonMfbAntiHolderDeltaMode);
            this.groupBox2.Controls.Add(this.radioButtonMfbHolderDeltaMode);
            this.groupBox2.Controls.Add(this.radioButtonMfbLongAndShortDeltaMode);
            this.groupBox2.Controls.Add(this.radioButtonOiDeltaMode);
            this.groupBox2.Controls.Add(this.radioButtonTrueDdMode);
            this.groupBox2.Controls.Add(this.radioButtonTrueDeltaMode);
            this.groupBox2.Controls.Add(this.checkBoxMfbUserPosCaclMode);
            this.groupBox2.Controls.Add(this.checkBoxMfbCaclBeetMode);
            this.groupBox2.Controls.Add(this.checkBoxMfbMode);
            this.groupBox2.Controls.Add(this.radioButtonPosClosedMode);
            this.groupBox2.Controls.Add(this.radioButtonOiMode);
            this.groupBox2.Controls.Add(this.radioButtonMfbClosedPosMode);
            this.groupBox2.Controls.Add(this.radioButtonLongShortMode);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox2.Location = new System.Drawing.Point(1624, 304);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(293, 681);
            this.groupBox2.TabIndex = 30;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "MyFxBook";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(132, 562);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 65;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // textBoxRangeStep
            // 
            this.textBoxRangeStep.Location = new System.Drawing.Point(13, 545);
            this.textBoxRangeStep.Name = "textBoxRangeStep";
            this.textBoxRangeStep.Size = new System.Drawing.Size(100, 22);
            this.textBoxRangeStep.TabIndex = 64;
            // 
            // dateTimePickerEndRange
            // 
            this.dateTimePickerEndRange.Location = new System.Drawing.Point(13, 517);
            this.dateTimePickerEndRange.Name = "dateTimePickerEndRange";
            this.dateTimePickerEndRange.Size = new System.Drawing.Size(200, 22);
            this.dateTimePickerEndRange.TabIndex = 63;
            // 
            // dateTimePickerStartRange
            // 
            this.dateTimePickerStartRange.Location = new System.Drawing.Point(12, 471);
            this.dateTimePickerStartRange.Name = "dateTimePickerStartRange";
            this.dateTimePickerStartRange.Size = new System.Drawing.Size(200, 22);
            this.dateTimePickerStartRange.TabIndex = 63;
            // 
            // radioButton9
            // 
            this.radioButton9.AutoSize = true;
            this.radioButton9.Location = new System.Drawing.Point(12, 429);
            this.radioButton9.Name = "radioButton9";
            this.radioButton9.Size = new System.Drawing.Size(95, 21);
            this.radioButton9.TabIndex = 62;
            this.radioButton9.TabStop = true;
            this.radioButton9.Text = "RatioDelta";
            this.radioButton9.UseVisualStyleBackColor = true;
            this.radioButton9.Click += new System.EventHandler(this.radioButton9_Click);
            // 
            // radioButton8
            // 
            this.radioButton8.AutoSize = true;
            this.radioButton8.Location = new System.Drawing.Point(8, 402);
            this.radioButton8.Name = "radioButton8";
            this.radioButton8.Size = new System.Drawing.Size(102, 21);
            this.radioButton8.TabIndex = 61;
            this.radioButton8.TabStop = true;
            this.radioButton8.Text = "GrowStupid";
            this.radioButton8.UseVisualStyleBackColor = true;
            this.radioButton8.Click += new System.EventHandler(this.radioButton8_Click);
            // 
            // radioButton7
            // 
            this.radioButton7.AutoSize = true;
            this.radioButton7.Location = new System.Drawing.Point(7, 375);
            this.radioButton7.Name = "radioButton7";
            this.radioButton7.Size = new System.Drawing.Size(136, 21);
            this.radioButton7.TabIndex = 60;
            this.radioButton7.TabStop = true;
            this.radioButton7.Text = "Grow + OI Deltas";
            this.radioButton7.UseVisualStyleBackColor = true;
            this.radioButton7.CheckedChanged += new System.EventHandler(this.radioButton7_CheckedChanged);
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(8, 348);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(82, 21);
            this.radioButton3.TabIndex = 59;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "RsiDelta";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.radioButtonMfbDeltaGrowHistogramMode);
            this.groupBox3.Controls.Add(this.radioButton2);
            this.groupBox3.Controls.Add(this.radioButton1);
            this.groupBox3.Location = new System.Drawing.Point(7, 161);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(277, 48);
            this.groupBox3.TabIndex = 58;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Grow";
            // 
            // radioButtonMfbDeltaGrowHistogramMode
            // 
            this.radioButtonMfbDeltaGrowHistogramMode.AutoSize = true;
            this.radioButtonMfbDeltaGrowHistogramMode.Location = new System.Drawing.Point(105, 14);
            this.radioButtonMfbDeltaGrowHistogramMode.Name = "radioButtonMfbDeltaGrowHistogramMode";
            this.radioButtonMfbDeltaGrowHistogramMode.Size = new System.Drawing.Size(39, 21);
            this.radioButtonMfbDeltaGrowHistogramMode.TabIndex = 59;
            this.radioButtonMfbDeltaGrowHistogramMode.TabStop = true;
            this.radioButtonMfbDeltaGrowHistogramMode.Text = "H";
            this.radioButtonMfbDeltaGrowHistogramMode.UseVisualStyleBackColor = true;
            this.radioButtonMfbDeltaGrowHistogramMode.CheckedChanged += new System.EventHandler(this.radioButtonMfbDeltaGrowHistogramMode_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBoxProjAvrPeriod);
            this.groupBox1.Controls.Add(this.comboBoxProjOperType);
            this.groupBox1.Controls.Add(this.comboBoxProjTimeType);
            this.groupBox1.Location = new System.Drawing.Point(6, 97);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(281, 63);
            this.groupBox1.TabIndex = 57;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Projections";
            // 
            // textBoxProjAvrPeriod
            // 
            this.textBoxProjAvrPeriod.Location = new System.Drawing.Point(194, 26);
            this.textBoxProjAvrPeriod.Name = "textBoxProjAvrPeriod";
            this.textBoxProjAvrPeriod.Size = new System.Drawing.Size(66, 22);
            this.textBoxProjAvrPeriod.TabIndex = 57;
            this.textBoxProjAvrPeriod.TextChanged += new System.EventHandler(this.textBoxProjAvrPeriod_TextChanged);
            this.textBoxProjAvrPeriod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxProjAvrPeriod_KeyDown);
            // 
            // comboBoxProjOperType
            // 
            this.comboBoxProjOperType.FormattingEnabled = true;
            this.comboBoxProjOperType.Location = new System.Drawing.Point(11, 26);
            this.comboBoxProjOperType.Name = "comboBoxProjOperType";
            this.comboBoxProjOperType.Size = new System.Drawing.Size(88, 24);
            this.comboBoxProjOperType.TabIndex = 56;
            this.comboBoxProjOperType.SelectedIndexChanged += new System.EventHandler(this.comboBoxProjOperType_SelectedIndexChanged);
            // 
            // comboBoxProjTimeType
            // 
            this.comboBoxProjTimeType.FormattingEnabled = true;
            this.comboBoxProjTimeType.Location = new System.Drawing.Point(100, 26);
            this.comboBoxProjTimeType.Name = "comboBoxProjTimeType";
            this.comboBoxProjTimeType.Size = new System.Drawing.Size(88, 24);
            this.comboBoxProjTimeType.TabIndex = 56;
            this.comboBoxProjTimeType.SelectedIndexChanged += new System.EventHandler(this.comboBoxProjTimeType_SelectedIndexChanged);
            // 
            // radioButtonMfbAntiHolderDeltaMode
            // 
            this.radioButtonMfbAntiHolderDeltaMode.AutoSize = true;
            this.radioButtonMfbAntiHolderDeltaMode.Location = new System.Drawing.Point(112, 320);
            this.radioButtonMfbAntiHolderDeltaMode.Name = "radioButtonMfbAntiHolderDeltaMode";
            this.radioButtonMfbAntiHolderDeltaMode.Size = new System.Drawing.Size(128, 21);
            this.radioButtonMfbAntiHolderDeltaMode.TabIndex = 50;
            this.radioButtonMfbAntiHolderDeltaMode.TabStop = true;
            this.radioButtonMfbAntiHolderDeltaMode.Text = "AntiHolderDelta";
            this.radioButtonMfbAntiHolderDeltaMode.UseVisualStyleBackColor = true;
            this.radioButtonMfbAntiHolderDeltaMode.CheckedChanged += new System.EventHandler(this.radioButtonMfbAntiHolderDeltaMode_CheckedChanged);
            // 
            // radioButtonMfbHolderDeltaMode
            // 
            this.radioButtonMfbHolderDeltaMode.AutoSize = true;
            this.radioButtonMfbHolderDeltaMode.Location = new System.Drawing.Point(7, 320);
            this.radioButtonMfbHolderDeltaMode.Name = "radioButtonMfbHolderDeltaMode";
            this.radioButtonMfbHolderDeltaMode.Size = new System.Drawing.Size(104, 21);
            this.radioButtonMfbHolderDeltaMode.TabIndex = 49;
            this.radioButtonMfbHolderDeltaMode.TabStop = true;
            this.radioButtonMfbHolderDeltaMode.Text = "HolderDelta";
            this.radioButtonMfbHolderDeltaMode.UseVisualStyleBackColor = true;
            this.radioButtonMfbHolderDeltaMode.CheckedChanged += new System.EventHandler(this.radioButtonMfbHolderDeltaMode_CheckedChanged);
            // 
            // radioButtonMfbLongAndShortDeltaMode
            // 
            this.radioButtonMfbLongAndShortDeltaMode.AutoSize = true;
            this.radioButtonMfbLongAndShortDeltaMode.Location = new System.Drawing.Point(103, 241);
            this.radioButtonMfbLongAndShortDeltaMode.Name = "radioButtonMfbLongAndShortDeltaMode";
            this.radioButtonMfbLongAndShortDeltaMode.Size = new System.Drawing.Size(62, 21);
            this.radioButtonMfbLongAndShortDeltaMode.TabIndex = 46;
            this.radioButtonMfbLongAndShortDeltaMode.TabStop = true;
            this.radioButtonMfbLongAndShortDeltaMode.Text = "Delta";
            this.radioButtonMfbLongAndShortDeltaMode.UseVisualStyleBackColor = true;
            this.radioButtonMfbLongAndShortDeltaMode.CheckedChanged += new System.EventHandler(this.radioButtonMfbLongAndShortDeltaMode_CheckedChanged);
            // 
            // radioButtonOiDeltaMode
            // 
            this.radioButtonOiDeltaMode.AutoSize = true;
            this.radioButtonOiDeltaMode.Location = new System.Drawing.Point(103, 265);
            this.radioButtonOiDeltaMode.Name = "radioButtonOiDeltaMode";
            this.radioButtonOiDeltaMode.Size = new System.Drawing.Size(78, 21);
            this.radioButtonOiDeltaMode.TabIndex = 34;
            this.radioButtonOiDeltaMode.TabStop = true;
            this.radioButtonOiDeltaMode.Text = "Дельта";
            this.radioButtonOiDeltaMode.UseVisualStyleBackColor = true;
            this.radioButtonOiDeltaMode.CheckedChanged += new System.EventHandler(this.radioButtonOiDeltaMode_CheckedChanged);
            // 
            // radioButtonTrueDdMode
            // 
            this.radioButtonTrueDdMode.AutoSize = true;
            this.radioButtonTrueDdMode.Location = new System.Drawing.Point(103, 293);
            this.radioButtonTrueDdMode.Name = "radioButtonTrueDdMode";
            this.radioButtonTrueDdMode.Size = new System.Drawing.Size(49, 21);
            this.radioButtonTrueDdMode.TabIndex = 31;
            this.radioButtonTrueDdMode.TabStop = true;
            this.radioButtonTrueDdMode.Text = "DD";
            this.radioButtonTrueDdMode.UseVisualStyleBackColor = true;
            this.radioButtonTrueDdMode.CheckedChanged += new System.EventHandler(this.radioButtonTrueDdMode_CheckedChanged);
            // 
            // radioButtonTrueDeltaMode
            // 
            this.radioButtonTrueDeltaMode.AutoSize = true;
            this.radioButtonTrueDeltaMode.Location = new System.Drawing.Point(8, 293);
            this.radioButtonTrueDeltaMode.Name = "radioButtonTrueDeltaMode";
            this.radioButtonTrueDeltaMode.Size = new System.Drawing.Size(96, 21);
            this.radioButtonTrueDeltaMode.TabIndex = 30;
            this.radioButtonTrueDeltaMode.TabStop = true;
            this.radioButtonTrueDeltaMode.Text = "True Delta";
            this.radioButtonTrueDeltaMode.UseVisualStyleBackColor = true;
            this.radioButtonTrueDeltaMode.CheckedChanged += new System.EventHandler(this.radioButtonTrueDeltaMode_CheckedChanged);
            // 
            // checkBoxMfbUserPosCaclMode
            // 
            this.checkBoxMfbUserPosCaclMode.AutoSize = true;
            this.checkBoxMfbUserPosCaclMode.Location = new System.Drawing.Point(8, 70);
            this.checkBoxMfbUserPosCaclMode.Name = "checkBoxMfbUserPosCaclMode";
            this.checkBoxMfbUserPosCaclMode.Size = new System.Drawing.Size(181, 21);
            this.checkBoxMfbUserPosCaclMode.TabIndex = 25;
            this.checkBoxMfbUserPosCaclMode.Text = "Использовать позиции";
            this.checkBoxMfbUserPosCaclMode.UseVisualStyleBackColor = true;
            this.checkBoxMfbUserPosCaclMode.CheckedChanged += new System.EventHandler(this.checkBoxMfbUserPosCaclMode_CheckedChanged);
            // 
            // checkBoxMfbCaclBeetMode
            // 
            this.checkBoxMfbCaclBeetMode.AutoSize = true;
            this.checkBoxMfbCaclBeetMode.Location = new System.Drawing.Point(8, 43);
            this.checkBoxMfbCaclBeetMode.Name = "checkBoxMfbCaclBeetMode";
            this.checkBoxMfbCaclBeetMode.Size = new System.Drawing.Size(202, 21);
            this.checkBoxMfbCaclBeetMode.TabIndex = 24;
            this.checkBoxMfbCaclBeetMode.Text = "калькуляция баров между";
            this.checkBoxMfbCaclBeetMode.UseVisualStyleBackColor = true;
            this.checkBoxMfbCaclBeetMode.CheckedChanged += new System.EventHandler(this.checkBoxMfbCaclBeetMode_CheckedChanged);
            // 
            // checkBoxMfbMode
            // 
            this.checkBoxMfbMode.AutoSize = true;
            this.checkBoxMfbMode.Location = new System.Drawing.Point(8, 21);
            this.checkBoxMfbMode.Name = "checkBoxMfbMode";
            this.checkBoxMfbMode.Size = new System.Drawing.Size(186, 21);
            this.checkBoxMfbMode.TabIndex = 23;
            this.checkBoxMfbMode.Text = "Использовать MyfxBook";
            this.checkBoxMfbMode.UseVisualStyleBackColor = true;
            this.checkBoxMfbMode.CheckedChanged += new System.EventHandler(this.checkBoxMfbMode_CheckedChanged);
            // 
            // chart1
            // 
            chartArea1.CursorX.LineColor = System.Drawing.Color.Black;
            chartArea1.CursorX.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea1.CursorX.LineWidth = 2;
            chartArea1.Name = "MainArea";
            chartArea2.AlignWithChartArea = "MainArea";
            chartArea2.CursorX.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            chartArea2.CursorX.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dot;
            chartArea2.CursorX.LineWidth = 2;
            chartArea2.Name = "AreaThree";
            this.chart1.ChartAreas.Add(chartArea1);
            this.chart1.ChartAreas.Add(chartArea2);
            this.chart1.Location = new System.Drawing.Point(24, 12);
            this.chart1.Name = "chart1";
            series1.ChartArea = "MainArea";
            series1.Color = System.Drawing.Color.Transparent;
            series1.IsXValueIndexed = true;
            series1.Name = "BarsStub";
            series1.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Time;
            series2.ChartArea = "MainArea";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Candlestick;
            series2.CustomProperties = "PriceDownColor=Red, PointWidth=1, PriceUpColor=Green";
            series2.IsXValueIndexed = true;
            series2.Name = "Bars";
            series2.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Time;
            series2.YValuesPerPoint = 4;
            series3.BorderWidth = 2;
            series3.ChartArea = "MainArea";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series3.Color = System.Drawing.Color.Black;
            series3.IsXValueIndexed = true;
            series3.Name = "ReturnPoint";
            series3.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Time;
            series4.BorderWidth = 2;
            series4.ChartArea = "AreaThree";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series4.Color = System.Drawing.Color.Blue;
            series4.IsXValueIndexed = true;
            series4.Name = "Buys";
            series4.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Time;
            series5.BorderWidth = 2;
            series5.ChartArea = "AreaThree";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series5.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            series5.IsXValueIndexed = true;
            series5.Name = "Sells";
            series5.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Time;
            series6.ChartArea = "AreaThree";
            series6.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series6.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            series6.IsXValueIndexed = true;
            series6.Name = "ProfitRatioAvr";
            series6.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Time;
            series7.BorderWidth = 3;
            series7.ChartArea = "AreaThree";
            series7.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series7.Color = System.Drawing.Color.Black;
            series7.IsXValueIndexed = true;
            series7.Name = "ProfitRatio";
            series7.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Time;
            series8.ChartArea = "AreaThree";
            series8.IsXValueIndexed = true;
            series8.Name = "Delta";
            series8.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Time;
            series9.ChartArea = "AreaThree";
            series9.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StackedColumn;
            series9.IsXValueIndexed = true;
            series9.Name = "Delta2";
            series9.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Time;
            series10.BorderWidth = 2;
            series10.ChartArea = "MainArea";
            series10.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series10.Color = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            series10.IsXValueIndexed = true;
            series10.Name = "ProfitBalance";
            series10.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Time;
            series11.BorderWidth = 3;
            series11.ChartArea = "MainArea";
            series11.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series11.Color = System.Drawing.Color.Magenta;
            series11.IsXValueIndexed = true;
            series11.Name = "PosLossBalance";
            series11.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Time;
            series12.BorderWidth = 5;
            series12.ChartArea = "MainArea";
            series12.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series12.Color = System.Drawing.Color.Purple;
            series12.IsXValueIndexed = true;
            series12.Name = "PosBalance";
            series12.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Time;
            series13.BorderWidth = 4;
            series13.ChartArea = "MainArea";
            series13.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series13.Color = System.Drawing.Color.Blue;
            series13.IsXValueIndexed = true;
            series13.Name = "StopBalance";
            series13.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Time;
            series14.BorderWidth = 2;
            series14.ChartArea = "MainArea";
            series14.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series14.Color = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            series14.IsXValueIndexed = true;
            series14.Name = "StopBalance50";
            series14.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Time;
            series15.BorderWidth = 2;
            series15.ChartArea = "MainArea";
            series15.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series15.Color = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            series15.IsXValueIndexed = true;
            series15.Name = "StopBalance100";
            series15.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Time;
            series16.BorderWidth = 2;
            series16.ChartArea = "MainArea";
            series16.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series16.Color = System.Drawing.Color.Red;
            series16.IsXValueIndexed = true;
            series16.Name = "TakeBalance";
            series16.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Time;
            series17.ChartArea = "AreaThree";
            series17.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.SplineArea;
            series17.Color = System.Drawing.Color.Blue;
            series17.CustomProperties = "EmptyPointValue=Zero, LineTension=1";
            series17.IsXValueIndexed = true;
            series17.Name = "BuysArea";
            series17.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Time;
            series18.ChartArea = "AreaThree";
            series18.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.SplineArea;
            series18.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            series18.CustomProperties = "EmptyPointValue=Zero, LineTension=1";
            series18.IsXValueIndexed = true;
            series18.Name = "SellsArea";
            series18.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Time;
            series19.BorderWidth = 2;
            series19.ChartArea = "AreaThree";
            series19.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series19.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            series19.IsXValueIndexed = true;
            series19.Name = "Sells2";
            series19.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Time;
            this.chart1.Series.Add(series1);
            this.chart1.Series.Add(series2);
            this.chart1.Series.Add(series3);
            this.chart1.Series.Add(series4);
            this.chart1.Series.Add(series5);
            this.chart1.Series.Add(series6);
            this.chart1.Series.Add(series7);
            this.chart1.Series.Add(series8);
            this.chart1.Series.Add(series9);
            this.chart1.Series.Add(series10);
            this.chart1.Series.Add(series11);
            this.chart1.Series.Add(series12);
            this.chart1.Series.Add(series13);
            this.chart1.Series.Add(series14);
            this.chart1.Series.Add(series15);
            this.chart1.Series.Add(series16);
            this.chart1.Series.Add(series17);
            this.chart1.Series.Add(series18);
            this.chart1.Series.Add(series19);
            this.chart1.Size = new System.Drawing.Size(1320, 877);
            this.chart1.TabIndex = 1;
            this.chart1.Text = "chart1";
            // 
            // buttonDataSetToday
            // 
            this.buttonDataSetToday.Location = new System.Drawing.Point(1539, 119);
            this.buttonDataSetToday.Name = "buttonDataSetToday";
            this.buttonDataSetToday.Size = new System.Drawing.Size(75, 36);
            this.buttonDataSetToday.TabIndex = 52;
            this.buttonDataSetToday.Text = "Today";
            this.buttonDataSetToday.UseVisualStyleBackColor = true;
            this.buttonDataSetToday.Click += new System.EventHandler(this.buttonDataSetToday_Click);
            // 
            // checkBoxMfbShowDeltaMode
            // 
            this.checkBoxMfbShowDeltaMode.AutoSize = true;
            this.checkBoxMfbShowDeltaMode.Location = new System.Drawing.Point(1363, 231);
            this.checkBoxMfbShowDeltaMode.Name = "checkBoxMfbShowDeltaMode";
            this.checkBoxMfbShowDeltaMode.Size = new System.Drawing.Size(148, 21);
            this.checkBoxMfbShowDeltaMode.TabIndex = 53;
            this.checkBoxMfbShowDeltaMode.Text = "Mfb Дельта цвета";
            this.checkBoxMfbShowDeltaMode.UseVisualStyleBackColor = true;
            this.checkBoxMfbShowDeltaMode.CheckedChanged += new System.EventHandler(this.checkBoxMfbShowDeltaMode_CheckedChanged);
            // 
            // checkBoxMfbBarsDeltaColorsExt
            // 
            this.checkBoxMfbBarsDeltaColorsExt.AutoSize = true;
            this.checkBoxMfbBarsDeltaColorsExt.Location = new System.Drawing.Point(1509, 231);
            this.checkBoxMfbBarsDeltaColorsExt.Name = "checkBoxMfbBarsDeltaColorsExt";
            this.checkBoxMfbBarsDeltaColorsExt.Size = new System.Drawing.Size(57, 21);
            this.checkBoxMfbBarsDeltaColorsExt.TabIndex = 54;
            this.checkBoxMfbBarsDeltaColorsExt.Text = "Ext1";
            this.checkBoxMfbBarsDeltaColorsExt.UseVisualStyleBackColor = true;
            this.checkBoxMfbBarsDeltaColorsExt.CheckedChanged += new System.EventHandler(this.checkBoxMfbBarsDeltaColorsExt_CheckedChanged);
            // 
            // buttoAddWorkDay
            // 
            this.buttoAddWorkDay.Location = new System.Drawing.Point(1778, 81);
            this.buttoAddWorkDay.Name = "buttoAddWorkDay";
            this.buttoAddWorkDay.Size = new System.Drawing.Size(53, 40);
            this.buttoAddWorkDay.TabIndex = 55;
            this.buttoAddWorkDay.Text = "+1";
            this.buttoAddWorkDay.UseVisualStyleBackColor = true;
            this.buttoAddWorkDay.Click += new System.EventHandler(this.buttoAddWorkDay_Click);
            // 
            // buttonSubWorkDays
            // 
            this.buttonSubWorkDays.Location = new System.Drawing.Point(1837, 81);
            this.buttonSubWorkDays.Name = "buttonSubWorkDays";
            this.buttonSubWorkDays.Size = new System.Drawing.Size(53, 40);
            this.buttonSubWorkDays.TabIndex = 55;
            this.buttonSubWorkDays.Text = "-1";
            this.buttonSubWorkDays.UseVisualStyleBackColor = true;
            this.buttonSubWorkDays.Click += new System.EventHandler(this.buttonSubWorkDays_Click);
            // 
            // checkBoxMfbBarColorExt2Mode
            // 
            this.checkBoxMfbBarColorExt2Mode.AutoSize = true;
            this.checkBoxMfbBarColorExt2Mode.Location = new System.Drawing.Point(1572, 230);
            this.checkBoxMfbBarColorExt2Mode.Name = "checkBoxMfbBarColorExt2Mode";
            this.checkBoxMfbBarColorExt2Mode.Size = new System.Drawing.Size(57, 21);
            this.checkBoxMfbBarColorExt2Mode.TabIndex = 56;
            this.checkBoxMfbBarColorExt2Mode.Text = "Ext2";
            this.checkBoxMfbBarColorExt2Mode.UseVisualStyleBackColor = true;
            this.checkBoxMfbBarColorExt2Mode.CheckedChanged += new System.EventHandler(this.checkBoxMfbBarColorExt2Mode_CheckedChanged);
            // 
            // checkBoxMfbBarColorHlMode
            // 
            this.checkBoxMfbBarColorHlMode.AutoSize = true;
            this.checkBoxMfbBarColorHlMode.Location = new System.Drawing.Point(1636, 230);
            this.checkBoxMfbBarColorHlMode.Name = "checkBoxMfbBarColorHlMode";
            this.checkBoxMfbBarColorHlMode.Size = new System.Drawing.Size(48, 21);
            this.checkBoxMfbBarColorHlMode.TabIndex = 57;
            this.checkBoxMfbBarColorHlMode.Text = "HL";
            this.checkBoxMfbBarColorHlMode.UseVisualStyleBackColor = true;
            this.checkBoxMfbBarColorHlMode.CheckedChanged += new System.EventHandler(this.checkBoxMfbBarColorHlMode_CheckedChanged);
            // 
            // buttonGbpSet
            // 
            this.buttonGbpSet.Location = new System.Drawing.Point(1350, 12);
            this.buttonGbpSet.Name = "buttonGbpSet";
            this.buttonGbpSet.Size = new System.Drawing.Size(56, 42);
            this.buttonGbpSet.TabIndex = 58;
            this.buttonGbpSet.Text = "GBP";
            this.buttonGbpSet.UseVisualStyleBackColor = true;
            this.buttonGbpSet.Click += new System.EventHandler(this.buttonGbpSet_Click);
            // 
            // buttonEurSet
            // 
            this.buttonEurSet.Location = new System.Drawing.Point(1413, 12);
            this.buttonEurSet.Name = "buttonEurSet";
            this.buttonEurSet.Size = new System.Drawing.Size(56, 41);
            this.buttonEurSet.TabIndex = 59;
            this.buttonEurSet.Text = "EUR";
            this.buttonEurSet.UseVisualStyleBackColor = true;
            this.buttonEurSet.Click += new System.EventHandler(this.button2_Click);
            // 
            // buttonCadSet
            // 
            this.buttonCadSet.Location = new System.Drawing.Point(1475, 12);
            this.buttonCadSet.Name = "buttonCadSet";
            this.buttonCadSet.Size = new System.Drawing.Size(56, 41);
            this.buttonCadSet.TabIndex = 59;
            this.buttonCadSet.Text = "CAD";
            this.buttonCadSet.UseVisualStyleBackColor = true;
            this.buttonCadSet.Click += new System.EventHandler(this.button3_Click);
            // 
            // buttonJpySet
            // 
            this.buttonJpySet.Location = new System.Drawing.Point(1537, 12);
            this.buttonJpySet.Name = "buttonJpySet";
            this.buttonJpySet.Size = new System.Drawing.Size(56, 41);
            this.buttonJpySet.TabIndex = 59;
            this.buttonJpySet.Text = "JPY";
            this.buttonJpySet.UseVisualStyleBackColor = true;
            this.buttonJpySet.Click += new System.EventHandler(this.button4_Click);
            // 
            // buttonAudSet
            // 
            this.buttonAudSet.Location = new System.Drawing.Point(1599, 12);
            this.buttonAudSet.Name = "buttonAudSet";
            this.buttonAudSet.Size = new System.Drawing.Size(56, 41);
            this.buttonAudSet.TabIndex = 59;
            this.buttonAudSet.Text = "AUD";
            this.buttonAudSet.UseVisualStyleBackColor = true;
            this.buttonAudSet.Click += new System.EventHandler(this.button5_Click);
            // 
            // buttonTfM5Set
            // 
            this.buttonTfM5Set.Location = new System.Drawing.Point(1350, 60);
            this.buttonTfM5Set.Name = "buttonTfM5Set";
            this.buttonTfM5Set.Size = new System.Drawing.Size(56, 39);
            this.buttonTfM5Set.TabIndex = 60;
            this.buttonTfM5Set.Text = "M5";
            this.buttonTfM5Set.UseVisualStyleBackColor = true;
            this.buttonTfM5Set.Click += new System.EventHandler(this.buttonTfM5Set_Click);
            // 
            // buttonTfM10Set
            // 
            this.buttonTfM10Set.Location = new System.Drawing.Point(1413, 59);
            this.buttonTfM10Set.Name = "buttonTfM10Set";
            this.buttonTfM10Set.Size = new System.Drawing.Size(56, 39);
            this.buttonTfM10Set.TabIndex = 60;
            this.buttonTfM10Set.Text = "M10";
            this.buttonTfM10Set.UseVisualStyleBackColor = true;
            this.buttonTfM10Set.Click += new System.EventHandler(this.buttonTfM10Set_Click);
            // 
            // buttonTfM20Set
            // 
            this.buttonTfM20Set.Location = new System.Drawing.Point(1475, 59);
            this.buttonTfM20Set.Name = "buttonTfM20Set";
            this.buttonTfM20Set.Size = new System.Drawing.Size(56, 39);
            this.buttonTfM20Set.TabIndex = 60;
            this.buttonTfM20Set.Text = "M20";
            this.buttonTfM20Set.UseVisualStyleBackColor = true;
            this.buttonTfM20Set.Click += new System.EventHandler(this.buttonTfM20Set_Click);
            // 
            // buttonTfM30Set
            // 
            this.buttonTfM30Set.Location = new System.Drawing.Point(1537, 59);
            this.buttonTfM30Set.Name = "buttonTfM30Set";
            this.buttonTfM30Set.Size = new System.Drawing.Size(56, 39);
            this.buttonTfM30Set.TabIndex = 60;
            this.buttonTfM30Set.Text = "M30";
            this.buttonTfM30Set.UseVisualStyleBackColor = true;
            this.buttonTfM30Set.Click += new System.EventHandler(this.buttonTfM30Set_Click);
            // 
            // buttonTfH1Set
            // 
            this.buttonTfH1Set.Location = new System.Drawing.Point(1599, 59);
            this.buttonTfH1Set.Name = "buttonTfH1Set";
            this.buttonTfH1Set.Size = new System.Drawing.Size(56, 39);
            this.buttonTfH1Set.TabIndex = 60;
            this.buttonTfH1Set.Text = "H1";
            this.buttonTfH1Set.UseVisualStyleBackColor = true;
            this.buttonTfH1Set.Click += new System.EventHandler(this.buttonTfH1Set_Click);
            // 
            // buttonTfH4Set
            // 
            this.buttonTfH4Set.Location = new System.Drawing.Point(1658, 60);
            this.buttonTfH4Set.Name = "buttonTfH4Set";
            this.buttonTfH4Set.Size = new System.Drawing.Size(56, 39);
            this.buttonTfH4Set.TabIndex = 60;
            this.buttonTfH4Set.Text = "H4";
            this.buttonTfH4Set.UseVisualStyleBackColor = true;
            this.buttonTfH4Set.Click += new System.EventHandler(this.buttonTfH4Set_Click);
            // 
            // buttonTfD1Set
            // 
            this.buttonTfD1Set.Location = new System.Drawing.Point(1720, 60);
            this.buttonTfD1Set.Name = "buttonTfD1Set";
            this.buttonTfD1Set.Size = new System.Drawing.Size(56, 39);
            this.buttonTfD1Set.TabIndex = 60;
            this.buttonTfD1Set.Text = "D1";
            this.buttonTfD1Set.UseVisualStyleBackColor = true;
            this.buttonTfD1Set.Click += new System.EventHandler(this.buttonTfD1Set_Click);
            // 
            // labelStartPointTime
            // 
            this.labelStartPointTime.AutoSize = true;
            this.labelStartPointTime.Location = new System.Drawing.Point(1143, 903);
            this.labelStartPointTime.Name = "labelStartPointTime";
            this.labelStartPointTime.Size = new System.Drawing.Size(46, 17);
            this.labelStartPointTime.TabIndex = 61;
            this.labelStartPointTime.Text = "label1";
            // 
            // labelEndPointTime
            // 
            this.labelEndPointTime.AutoSize = true;
            this.labelEndPointTime.Location = new System.Drawing.Point(1143, 930);
            this.labelEndPointTime.Name = "labelEndPointTime";
            this.labelEndPointTime.Size = new System.Drawing.Size(46, 17);
            this.labelEndPointTime.TabIndex = 61;
            this.labelEndPointTime.Text = "label1";
            // 
            // checkBoxMfbTrueColorMode
            // 
            this.checkBoxMfbTrueColorMode.AutoSize = true;
            this.checkBoxMfbTrueColorMode.Location = new System.Drawing.Point(1689, 231);
            this.checkBoxMfbTrueColorMode.Name = "checkBoxMfbTrueColorMode";
            this.checkBoxMfbTrueColorMode.Size = new System.Drawing.Size(60, 21);
            this.checkBoxMfbTrueColorMode.TabIndex = 64;
            this.checkBoxMfbTrueColorMode.Text = "True";
            this.checkBoxMfbTrueColorMode.UseVisualStyleBackColor = true;
            this.checkBoxMfbTrueColorMode.CheckedChanged += new System.EventHandler(this.checkBoxMfbTrueColorMode_CheckedChanged);
            // 
            // checkBoxSubArea2Visible
            // 
            this.checkBoxSubArea2Visible.AutoSize = true;
            this.checkBoxSubArea2Visible.Checked = true;
            this.checkBoxSubArea2Visible.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxSubArea2Visible.Location = new System.Drawing.Point(1363, 258);
            this.checkBoxSubArea2Visible.Name = "checkBoxSubArea2Visible";
            this.checkBoxSubArea2Visible.Size = new System.Drawing.Size(101, 21);
            this.checkBoxSubArea2Visible.TabIndex = 11;
            this.checkBoxSubArea2Visible.Text = "Видимость";
            this.checkBoxSubArea2Visible.UseVisualStyleBackColor = true;
            this.checkBoxSubArea2Visible.CheckedChanged += new System.EventHandler(this.checkBoxSubArea2Visible_CheckedChanged);
            // 
            // ProfitRatioView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1914, 1009);
            this.Controls.Add(this.checkBoxMfbTrueColorMode);
            this.Controls.Add(this.labelEndPointTime);
            this.Controls.Add(this.labelStartPointTime);
            this.Controls.Add(this.buttonTfD1Set);
            this.Controls.Add(this.buttonTfH4Set);
            this.Controls.Add(this.buttonTfH1Set);
            this.Controls.Add(this.buttonTfM30Set);
            this.Controls.Add(this.buttonTfM20Set);
            this.Controls.Add(this.buttonTfM10Set);
            this.Controls.Add(this.buttonTfM5Set);
            this.Controls.Add(this.buttonAudSet);
            this.Controls.Add(this.buttonJpySet);
            this.Controls.Add(this.buttonCadSet);
            this.Controls.Add(this.buttonEurSet);
            this.Controls.Add(this.buttonGbpSet);
            this.Controls.Add(this.checkBoxMfbBarColorHlMode);
            this.Controls.Add(this.checkBoxMfbBarColorExt2Mode);
            this.Controls.Add(this.buttonSubWorkDays);
            this.Controls.Add(this.buttoAddWorkDay);
            this.Controls.Add(this.checkBoxMfbBarsDeltaColorsExt);
            this.Controls.Add(this.checkBoxMfbShowDeltaMode);
            this.Controls.Add(this.buttonDataSetToday);
            this.Controls.Add(this.checkBoxSubArea2Visible);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.textBoxAppendHours);
            this.Controls.Add(this.comboBoxTimeFrame);
            this.Controls.Add(this.textBoxAddDayHistory);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.labelTime);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.buttonUpdate);
            this.Controls.Add(this.comboBoxInstruments);
            this.Controls.Add(this.chart1);
            this.MaximizeBox = false;
            this.Name = "ProfitRatioView";
            this.Text = "ProfitRatioView";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ProfitRatioView_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ProfitRatioView_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ProfitRatioView_KeyPress);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxInstruments;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label labelTime;
        private System.Windows.Forms.TextBox textBoxAddDayHistory;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBoxTimeFrame;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButtonLongShortMode;
        private System.Windows.Forms.RadioButton radioButtonOiMode;
        private System.Windows.Forms.RadioButton radioButtonPosClosedMode;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButtonMfbClosedPosMode;
        private System.Windows.Forms.TextBox textBoxAppendHours;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox checkBoxMfbUserPosCaclMode;
        private System.Windows.Forms.CheckBox checkBoxMfbCaclBeetMode;
        private System.Windows.Forms.CheckBox checkBoxMfbMode;
        private System.Windows.Forms.RadioButton radioButtonTrueDeltaMode;
        private System.Windows.Forms.RadioButton radioButtonTrueDdMode;
        private System.Windows.Forms.RadioButton radioButtonOiDeltaMode;
        private System.Windows.Forms.RadioButton radioButtonMfbLongAndShortDeltaMode;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.RadioButton radioButtonMfbHolderDeltaMode;
        private System.Windows.Forms.RadioButton radioButtonMfbAntiHolderDeltaMode;
        private System.Windows.Forms.Button buttonDataSetToday;
        private System.Windows.Forms.ComboBox comboBoxProjTimeType;
        private System.Windows.Forms.ComboBox comboBoxProjOperType;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton radioButtonMfbDeltaGrowHistogramMode;
        private System.Windows.Forms.CheckBox checkBoxMfbShowDeltaMode;
        private System.Windows.Forms.CheckBox checkBoxMfbBarsDeltaColorsExt;
        private System.Windows.Forms.Button buttoAddWorkDay;
        private System.Windows.Forms.Button buttonSubWorkDays;
        private System.Windows.Forms.CheckBox checkBoxMfbBarColorExt2Mode;
        private System.Windows.Forms.CheckBox checkBoxMfbBarColorHlMode;
        private System.Windows.Forms.Button buttonGbpSet;
        private System.Windows.Forms.Button buttonEurSet;
        private System.Windows.Forms.Button buttonCadSet;
        private System.Windows.Forms.Button buttonJpySet;
        private System.Windows.Forms.Button buttonAudSet;
        private System.Windows.Forms.Button buttonTfM5Set;
        private System.Windows.Forms.Button buttonTfM10Set;
        private System.Windows.Forms.Button buttonTfM20Set;
        private System.Windows.Forms.Button buttonTfM30Set;
        private System.Windows.Forms.Button buttonTfH1Set;
        private System.Windows.Forms.Button buttonTfH4Set;
        private System.Windows.Forms.Button buttonTfD1Set;
        private System.Windows.Forms.Label labelStartPointTime;
        private System.Windows.Forms.Label labelEndPointTime;
        private System.Windows.Forms.TextBox textBoxProjAvrPeriod;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton7;
        private System.Windows.Forms.CheckBox checkBoxMfbTrueColorMode;
        private System.Windows.Forms.RadioButton radioButton8;
        private System.Windows.Forms.RadioButton radioButton9;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBoxRangeStep;
        private System.Windows.Forms.DateTimePicker dateTimePickerEndRange;
        private System.Windows.Forms.DateTimePicker dateTimePickerStartRange;
        private System.Windows.Forms.CheckBox checkBoxSubArea2Visible;
    }
}