﻿namespace Antick.MfbTools.Infractructure.Mvp.Views
{
    public interface IView
    {
        void Show();
        void Close();
    }
}
