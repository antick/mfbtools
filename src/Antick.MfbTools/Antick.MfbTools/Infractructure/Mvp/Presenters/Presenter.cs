﻿using Antick.MfbTools.Infractructure.Mvp.Models;
using Antick.MfbTools.Infractructure.Mvp.Views;
using Castle.MicroKernel;

namespace Antick.MfbTools.Infractructure.Mvp.Presenters
{
    /// <summary>
    /// Классический презентрер модели MVP
    /// </summary>
    public class Presenter<TView, TModel> : ViewPresenter<TView>
        where TView : IView
        where TModel : Model
    {
        protected readonly TModel Model;

        public Presenter(IKernel kernel, TView view, TModel model)
            : base(kernel, view)
        {
            Model = model;
        }
    }
}
