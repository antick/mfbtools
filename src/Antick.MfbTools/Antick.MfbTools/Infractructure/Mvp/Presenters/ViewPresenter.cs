﻿using Antick.MfbTools.Infractructure.Mvp.Views;
using Castle.MicroKernel;

namespace Antick.MfbTools.Infractructure.Mvp.Presenters
{
    /// <summary>
    /// Первичный презентер, имеет еще View
    /// </summary>
    /// <typeparam name="TView"></typeparam>
    public abstract class ViewPresenter<TView> : RootPresenter
        where TView : IView
    {
        protected TView View { get; private set; }

        protected ViewPresenter(IKernel kernel, TView view)
            : base(kernel)
        {
            View = view;
        }

        public override void Run()
        {
            View.Show();
        }
    }
}
