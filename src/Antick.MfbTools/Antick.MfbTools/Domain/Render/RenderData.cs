﻿using System.Collections.Generic;
using Antick.MfbTools.Domain.Enums;

namespace Antick.MfbTools.Domain.Render
{
    public class RenderData
    {
        public List<RenderCandle> Candles { get; set; }

        public List<RenderIndicator> IndicatorsData { get; set; }

        public RenderIndicatorType IndicatorType { get; set; }

        public Utils.InstrumentSpec Spec { get; set; }

        public string IndicatorDataFormat { get; set; }

        public CandleProjectionType CandleProjectionType { get; set; }

        public List<CandleDirectionType> CandleDirections { get; set; }
        public List<CandleGrowType> CandleGrowTypes { get; set; }

        public string Instrument { get; set; }

        public string TimeFrame { get; set; }
    }
}
