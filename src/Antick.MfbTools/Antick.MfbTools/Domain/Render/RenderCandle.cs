﻿using System;

namespace Antick.MfbTools.Domain.Render
{
    public class RenderCandle
    {
        public DateTime Time { get; set; }

        public double Open { get; set; }

        public double Close { get; set; }

        public double High { get; set; }

        public double Low { get; set; }


        public RenderCandle Clone()
        {
            return new RenderCandle
            {
                Time = Time,
                Open = Open,
                Close = Close,
                High = High,
                Low = Low,
            };
        }
    }
}
