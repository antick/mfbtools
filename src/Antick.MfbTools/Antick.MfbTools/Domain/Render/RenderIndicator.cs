﻿namespace Antick.MfbTools.Domain.Render
{
    public class RenderIndicator
    {
        public double Buy { get; set; }

        public double Sell { get; set; }
    }
}
