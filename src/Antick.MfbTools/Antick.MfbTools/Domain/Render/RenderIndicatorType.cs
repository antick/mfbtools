﻿namespace Antick.MfbTools.Domain.Render
{
    public enum RenderIndicatorType
    {
        Line,
        Line2,
        Delta,
        Delta2,
    }
}
