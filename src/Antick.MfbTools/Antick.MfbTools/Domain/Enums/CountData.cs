﻿using System.ComponentModel;

namespace Antick.MfbTools.Domain.Enums
{
    public enum CountData
    {
        All,
        C100,
        C200,
        C500
    }
}
