﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Antick.MfbTools.Domain.Enums
{
    public enum CandleGrowType
    {
       None,
       SellLowerAvr,
       BuyLowerAvr,
       BuyUpperAvr,
       SellUpperAvr
    }
}
