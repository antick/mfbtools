﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.MfbTools.Components;

namespace Antick.MfbTools.Domain.Enums
{
    public enum SourceDataProjectionType
    {
        [Localization("Нет", "None")]
        None,
        [Localization("Усреднение", "Average")]
        Average,
        [Localization("Усреднение с накоплением", "Average with accumulation")]
        AverageSquare
    }
}
