﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.MfbTools.Components;

namespace Antick.MfbTools.Domain.Enums
{
    public enum CandleProjectionType
    {
        [Localization("Нет", "None")]
        None,
      
        [Localization("Прирост", "Grow")]
        Grow,
        
        [Localization("Прирост[2 типа]", "Grow[2 types]")]
        Grow2Type,
        
        [Localization("Прирост[Зоны]", "Grow[Zones]")]
        RectangeGrow,

        [Localization("Прирост[Зоны, 2 типа]", "Grow[Zones, 2 types]")]
        RestangeGrow2Type,
    }
}
