﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Antick.MfbTools.Domain.Enums
{
    public enum LanguageType
    {
        English,
        Russian
    }
}
