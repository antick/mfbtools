﻿namespace Antick.MfbTools.Domain.Enums
{
    public enum IndicatorType
    {
        LongShort,

        SumGrowDelta,
        GrowDelta,
        OpenInterest,

        LongDeltaShortDelta,
    }
}
