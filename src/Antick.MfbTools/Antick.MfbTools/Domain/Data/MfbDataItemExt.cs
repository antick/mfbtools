﻿namespace Antick.MfbTools.Domain.Data
{
    public class MfbDataItemExt : MfbDataItem
    {
        public double LongDelta { get; set; }
        public double ShortDelta { get; set; }
    }
}
