﻿using Antick.Contracts;

namespace Antick.MfbTools.Domain.Data
{
    public class MfbDataItem
    {
        public Candle Candle { get; set; }

        public double Long { get; set; }

        public double Short { get; set; }
    }
}
