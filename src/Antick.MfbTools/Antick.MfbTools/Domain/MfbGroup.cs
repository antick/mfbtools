﻿namespace Antick.MfbTools.Domain
{
    /// <summary>
    /// Освновной срез данных по сантименту МФБ. 
    /// Может описывать как данные по объемам так и по позициям.
    /// </summary>
    public class MfbGroup
    {
        /// <summary>
        /// Объем позиций лонга
        /// </summary>
        public double Long { get; set; }

        /// <summary>
        /// Объем позиций шорта
        /// </summary>
        public double Short { get; set; }

        /// <summary>
        /// Объем открытых позиций лонга
        /// </summary>
        public double LongOpened { get; set; }

        /// <summary>
        /// Объем открытых позиций шорта
        /// </summary>
        public double ShortOpened { get; set; }

        /// <summary>
        /// Объем закрытых позиций лонга
        /// </summary>
        public double LongClosed { get; set; }

        /// <summary>
        /// Объем закрытых позиций шорта
        /// </summary>
        public double ShortClosed { get; set; }

        // Расчетные величины

        /// <summary>
        /// Обычный ОИ, сумма лонга и шорта
        /// </summary>
        public double Oi { get; set; }

        /// <summary>
        /// Изменение Ои ввиде дельты
        /// </summary>
        public double OiDelta { get; set; }

        /// <summary>
        /// Дельта прироста позиций, больше нуля значит лонга
        /// </summary>
        public double DeltaOpened { get; set; }

        public double DeltaClosed { get; set; }

        /// <summary>
        /// Убрать, расчетное.
        /// </summary>
        public double HorBalance { get; set; }

        public double LongDelta { get; set; }
        public double ShortDelta { get; set; }

        public void Null()
        {
            LongOpened = 0;
            ShortOpened = 0;
            LongClosed = 0;
            ShortClosed = 0;
            Oi = 0;
            OiDelta = 0;
            DeltaOpened = 0;
            DeltaClosed = 0;
            HorBalance = 0;
            LongDelta = 0;
            ShortDelta = 0;
        }
    }
}
