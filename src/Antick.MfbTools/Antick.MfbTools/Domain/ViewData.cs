﻿using Antick.Contracts;
using Antick.MfbTools.Legacy.Types;

namespace Antick.MfbTools.Domain
{
    /// <summary>
    /// Базовый класс, описывающий первичный набор даты для отрисовки, 
    /// </summary>
    public class ViewData
    {
        public Candle Candle { get; set; }

        public MfbGroup Mfb = new MfbGroup();
    }
}
