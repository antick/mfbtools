﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Antick.MfbTools.Domain
{
    public class InitData
    {
        public DateTime ExpireTime { get; set; }

        public string Title { get; set; }

        public string Version { get; set; }
    }
}
