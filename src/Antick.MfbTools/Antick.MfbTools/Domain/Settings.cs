﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.MfbTools.Domain.Enums;

namespace Antick.MfbTools.Domain
{
    public class Settings
    {
        public LanguageType Language { get; set; }
    }
}
