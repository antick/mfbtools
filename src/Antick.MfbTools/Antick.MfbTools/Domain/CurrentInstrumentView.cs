﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Contracts;

namespace Antick.MfbTools.Domain
{
    public class CurrentInstrumentView
    {
        public Instrument Instrument { get; set; }

        public TimeFrame TimeFrame { get; set; }
        
        public int? CountCandles { get; set; }
    }
}
