﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Antick.MfbTools.Domain.Enums;

namespace Antick.MfbTools.Extensions
{
    public static class LocalicationAttributeExtensions
    {
        public static string GetLocalizationEnum(Enum t)
        {
            MemberInfo memberInfo = t.GetType().GetMember(t.ToString())
                .FirstOrDefault();

            var culture = Thread.CurrentThread.CurrentUICulture;

            if (memberInfo != null)
            {
                var attribute = (Components.LocalizationAttribute)memberInfo.GetCustomAttributes(typeof(Components.LocalizationAttribute), false).FirstOrDefault();
                if (attribute != null)
                {
                    if (culture.Name == "ru-RU")
                        return attribute.RussiaDesc;
                    else
                    {
                        return attribute.EnglishDesc;
                    }
                }
                
            }

            return "No Localization";
        }
    }
}
